$(document).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    // Pop Up
    $('.kipop.costum')
        .popup({
            hoverable: true,
        });
    // Dropdowns
    $('.ui.dropdown:not(.costum)').dropdown();

    // Messages
    $('.message.closeable')
        .on('click', function() {
            $(this)
                .transition('fade');
        });

    // Sidebar
    $('.ui.sidebar.menu')
        .sidebar('setting', 'transition', 'overlay')
        .sidebar('attach events', '.toc.item');

    // Header 1
    $('.static.menu').visibility({
        type: 'fixed',
        onFixed: function() {
            $('.static.menu').find('.logo').transition('fade in');
        },
        onUnfixed: function() {
            $('.static.menu').find('.logo').transition('fade out');
        }
    });

    if ($(window).width() > 600) {
        var logoImage = $('.following.bar .item .image').attr('src');
        var logoImageInverted = $('.following.bar .item .image').attr('inverted-src');
        var sidebarIcon = $('.following.bar .icon');
        $('body')
            .visibility({
                offset: -10,
                observeChanges: false,
                once: false,
                continuous: false,
                onTopPassed: function() {
                    requestAnimationFrame(function() {

                        //Header 2
                        $('.following.bar')
                            .addClass('light fixed')
                            .find('.menu')
                            .removeClass('inverted');
                        $('.following.bar .item .image')
                            .attr('src', logoImageInverted);

                        // Header 3
                        $('.header3.bar')
                            .removeClass('light');

                        $('.header3.bar .hiddenable')
                            .transition('slide down');

                    });
                },
                onTopPassedReverse: function() {
                    requestAnimationFrame(function() {
                        //Header 2
                        $('.following.bar')
                            .removeClass('light fixed')
                            .find('.menu')
                            .addClass('inverted');
                        $('.following.bar .item .image')
                            .attr('src', logoImage);

                        //Header 3
                        $('.header3.bar')
                            .addClass('light');
                        $('.header3.bar .hiddenable')
                            .transition('slide down');
                    });
                }
            });
    };
});

function toggleFullscreen(elem) {
    elem = elem || document.documentElement;
    if (!document.fullscreenElement && !document.mozFullScreenElement &&
        !document.webkitFullscreenElement && !document.msFullscreenElement) {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
}

function showConfirmModal(caller) {
    var curModal = $('#' + $(caller).attr('data-modalid'));

    curModal.modal({
        closable: true,
        onDeny: function() {
            return true;
        },
        onShow: function() {
            if (caller != null) {
                var action = $(caller).attr('data-action');
                var method = $(caller).attr('data-method');
                var m_header = $(caller).attr('data-m-header');
                var m_sub_header = $(caller).attr('data-m-sub-header');

                curModal.find('form').attr('action', action);
                curModal.find('form').attr('method', method);

                curModal.find('#confirm_header').html(m_header);
                curModal.find('#confirm_sub_header').html(m_sub_header);
            }
        }
    });
    curModal.modal('show');
}