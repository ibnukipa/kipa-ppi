(function(g, j, c, f, d, k, i) { /*! Jssor */
    new(function() {});
    var e = { db: function(a) { return a }, Xe: function(a) { return -a * (a - 2) }, Sc: function(a) { return a * a * a } };
    var b = new function() {
        var h = this,
            zb = /\S+/g,
            H = 1,
            bb = 2,
            eb = 3,
            db = 4,
            hb = 5,
            I, s = 0,
            l = 0,
            t = 0,
            Z = 0,
            B = 0,
            K = navigator,
            mb = K.appName,
            o = K.userAgent,
            p = parseFloat;

        function Ib() { if (!I) { I = { ef: "ontouchstart" in g || "createTouch" in j }; var a; if (K.pointerEnabled || (a = K.msPointerEnabled)) I.Qc = a ? "msTouchAction" : "touchAction" } return I }

        function w(h) {
            if (!s) {
                s = -1;
                if (mb == "Microsoft Internet Explorer" && !!g.attachEvent && !!g.ActiveXObject) {
                    var e = o.indexOf("MSIE");
                    s = H;
                    t = p(o.substring(e + 5, o.indexOf(";", e))); /*@cc_on Z=@_jscript_version@*/ ;
                    l = j.documentMode || t
                } else if (mb == "Netscape" && !!g.addEventListener) {
                    var d = o.indexOf("Firefox"),
                        b = o.indexOf("Safari"),
                        f = o.indexOf("Chrome"),
                        c = o.indexOf("AppleWebKit");
                    if (d >= 0) {
                        s = bb;
                        l = p(o.substring(d + 8))
                    } else if (b >= 0) {
                        var i = o.substring(0, b).lastIndexOf("/");
                        s = f >= 0 ? db : eb;
                        l = p(o.substring(i + 1, b))
                    } else {
                        var a = /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/i.exec(o);
                        if (a) {
                            s = H;
                            l = t = p(a[1])
                        }
                    }
                    if (c >= 0) B = p(o.substring(c + 12))
                } else {
                    var a = /(opera)(?:.*version|)[ \/]([\w.]+)/i.exec(o);
                    if (a) {
                        s = hb;
                        l = p(a[2])
                    }
                }
            }
            return h == s
        }

        function q() { return w(H) }

        function xb() { return q() && (l < 6 || j.compatMode == "BackCompat") }

        function cb() { return w(eb) }

        function gb() { return w(hb) }

        function tb() { return cb() && B > 534 && B < 535 }

        function L() { w(); return B > 537 || l > 42 || s == H && l >= 11 }

        function vb() { return q() && l < 9 }

        function ub(a) {
            var b, c;
            return function(f) {
                if (!b) {
                    b = d;
                    var e = a.substr(0, 1).toUpperCase() + a.substr(1);
                    n([a].concat(["WebKit", "ms", "Moz", "O", "webkit"]), function(g, d) { var b = a; if (d) b = g + e; if (f.style[b] != i) return c = b })
                }
                return c
            }
        }

        function sb(b) { var a; return function(c) { a = a || ub(b)(c) || b; return a } }
        var M = sb("transform");

        function lb(a) { return {}.toString.call(a) }
        var ib = {};
        n(["Boolean", "Number", "String", "Function", "Array", "Date", "RegExp", "Object"], function(a) { ib["[object " + a + "]"] = a.toLowerCase() });

        function n(b, d) {
            var a, c;
            if (lb(b) == "[object Array]") {
                for (a = 0; a < b.length; a++)
                    if (c = d(b[a], a, b)) return c
            } else
                for (a in b)
                    if (c = d(b[a], a, b)) return c
        }

        function E(a) { return a == f ? String(a) : ib[lb(a)] || "object" }

        function jb(a) { for (var b in a) return d }

        function C(a) { try { return E(a) == "object" && !a.nodeType && a != a.window && (!a.constructor || {}.hasOwnProperty.call(a.constructor.prototype, "isPrototypeOf")) } catch (b) {} }

        function v(a, b) { return { x: a, y: b } }

        function pb(b, a) { setTimeout(b, a || 0) }

        function J(b, d, c) {
            var a = !b || b == "inherit" ? "" : b;
            n(d, function(c) {
                var b = c.exec(a);
                if (b) {
                    var d = a.substr(0, b.index),
                        e = a.substr(b.index + b[0].length + 1, a.length - 1);
                    a = d + e
                }
            });
            a = c + (!a.indexOf(" ") ? "" : " ") + a;
            return a
        }

        function rb(b, a) { if (l < 9) b.style.filter = a }

        function Hb(a, b) { if (a === i) a = b; return a }
        h.lf = Ib;
        h.yd = q;
        h.gf = xb;
        h.Af = cb;
        h.Bf = L;
        ub("transform");
        h.qd = function() { return l };
        h.pd = function() { return t || l };
        h.Cf = function() { w(); return B };
        h.Hb = pb;
        h.Df = function(a, b) { b.call(a); return D({}, a) };

        function W(a) { a.constructor === W.caller && a.Kb && a.Kb.apply(a, W.caller.arguments) }
        h.Kb = W;
        h.Ab = function(a) { if (h.Gf(a)) a = j.getElementById(a); return a };

        function u(a) { return a || g.event }
        h.ld = u;
        h.rc = function(b) { b = u(b); var a = b.target || b.srcElement || j; if (a.nodeType == 3) a = h.kc(a); return a };
        h.jd = function(a) { a = u(a); return { x: a.pageX || a.clientX || 0, y: a.pageY || a.clientY || 0 } };

        function x(c, d, a) {
            if (a !== i) c.style[d] = a == i ? "" : a;
            else {
                var b = c.currentStyle || c.style;
                a = b[d];
                if (a == "" && g.getComputedStyle) {
                    b = c.ownerDocument.defaultView.getComputedStyle(c, f);
                    b && (a = b.getPropertyValue(d) || b[d])
                }
                return a
            }
        }

        function Y(b, c, a, d) {
            if (a === i) {
                a = p(x(b, c));
                isNaN(a) && (a = f);
                return a
            }
            if (a == f) a = "";
            else d && (a += "px");
            x(b, c, a)
        }

        function m(c, a) {
            var d = a ? Y : x,
                b;
            if (a & 4) b = sb(c);
            return function(e, f) { return d(e, b ? b(e) : c, f, a & 2) }
        }

        function Cb(b) { if (q() && t < 9) { var a = /opacity=([^)]*)/.exec(b.style.filter || ""); return a ? p(a[1]) / 100 : 1 } else return p(b.style.opacity || "1") }

        function Eb(b, a, f) {
            if (q() && t < 9) {
                var h = b.style.filter || "",
                    i = new RegExp(/[\s]*alpha\([^\)]*\)/g),
                    e = c.round(100 * a),
                    d = "";
                if (e < 100 || f) d = "alpha(opacity=" + e + ") ";
                var g = J(h, [i], d);
                rb(b, g)
            } else b.style.opacity = a == 1 ? "" : c.round(a * 100) / 100
        }
        var N = { C: ["rotate"], Y: ["rotateX"], U: ["rotateY"], Gb: ["skewX"], Ib: ["skewY"] };
        if (!L()) N = D(N, { B: ["scaleX", 2], A: ["scaleY", 2], X: ["translateZ", 1] });

        function O(d, a) {
            var c = "";
            if (a) {
                if (q() && l && l < 10) {
                    delete a.Y;
                    delete a.U;
                    delete a.X
                }
                b.f(a, function(d, b) { var a = N[b]; if (a) { var e = a[1] || 0; if (P[b] != d) c += " " + a[0] + "(" + d + (["deg", "px", ""])[e] + ")" } });
                if (L()) { if (a.lb || a.ib || a.X != i) c += " translate3d(" + (a.lb || 0) + "px," + (a.ib || 0) + "px," + (a.X || 0) + "px)"; if (a.B == i) a.B = 1; if (a.A == i) a.A = 1; if (a.B != 1 || a.A != 1) c += " scale3d(" + a.B + ", " + a.A + ", 1)" }
            }
            d.style[M(d)] = c
        }
        h.Hf = m("transformOrigin", 4);
        h.If = m("backfaceVisibility", 4);
        h.Kf = m("transformStyle", 4);
        h.Lf = m("perspective", 6);
        h.zf = m("perspectiveOrigin", 4);
        h.Ef = function(a, b) {
            if (q() && t < 9 || t < 10 && xb()) a.style.zoom = b == 1 ? "" : b;
            else {
                var c = M(a),
                    f = "scale(" + b + ")",
                    e = a.style[c],
                    g = new RegExp(/[\s]*scale\(.*?\)/g),
                    d = J(e, [g], f);
                a.style[c] = d
            }
        };
        h.c = function(a, d, b, c) {
            a = h.Ab(a);
            if (a.addEventListener) {
                d == "mousewheel" && a.addEventListener("DOMMouseScroll", b, c);
                a.addEventListener(d, b, c)
            } else if (a.attachEvent) {
                a.attachEvent("on" + d, b);
                c && a.setCapture && a.setCapture()
            }
        };
        h.P = function(a, c, d, b) {
            a = h.Ab(a);
            if (a.removeEventListener) {
                c == "mousewheel" && a.removeEventListener("DOMMouseScroll", d, b);
                a.removeEventListener(c, d, b)
            } else if (a.detachEvent) {
                a.detachEvent("on" + c, d);
                b && a.releaseCapture && a.releaseCapture()
            }
        };
        h.Lb = function(a) {
            a = u(a);
            a.preventDefault && a.preventDefault();
            a.cancel = d;
            a.returnValue = k
        };
        h.jf = function(a) {
            a = u(a);
            a.stopPropagation && a.stopPropagation();
            a.cancelBubble = d
        };
        h.O = function(d, c) {
            var a = [].slice.call(arguments, 2),
                b = function() { var b = a.concat([].slice.call(arguments, 0)); return c.apply(d, b) };
            return b
        };
        h.Nb = function(d, c) { for (var b = [], a = d.firstChild; a; a = a.nextSibling)(c || a.nodeType == 1) && b.push(a); return b };

        function kb(a, c, e, b) {
            b = b || "u";
            for (a = a ? a.firstChild : f; a; a = a.nextSibling)
                if (a.nodeType == 1) { if (U(a, b) == c) return a; if (!e) { var d = kb(a, c, e, b); if (d) return d } }
        }
        h.Mb = kb;

        function T(a, d, g, b) {
            b = b || "u";
            var c = [];
            for (a = a ? a.firstChild : f; a; a = a.nextSibling)
                if (a.nodeType == 1) { U(a, b) == d && c.push(a); if (!g) { var e = T(a, d, g, b); if (e.length) c = c.concat(e) } }
            return c
        }

        function fb(a, c, d) {
            for (a = a ? a.firstChild : f; a; a = a.nextSibling)
                if (a.nodeType == 1) { if (a.tagName == c) return a; if (!d) { var b = fb(a, c, d); if (b) return b } }
        }
        h.of = fb;
        h.pf = function(b, a) { return b.getElementsByTagName(a) };
        h.wb = function(a, f, d) {
            d = d || "u";
            var e;
            do {
                if (a.nodeType == 1) { var c = b.m(a, d); if (c && c == Hb(f, c)) { e = a; break } }
                a = b.kc(a)
            } while (a && a != j.body);
            return e
        };

        function D() {
            var e = arguments,
                d, c, b, a, g = 1 & e[0],
                f = 1 + g;
            d = e[f - 1] || {};
            for (; f < e.length; f++)
                if (c = e[f])
                    for (b in c) {
                        a = c[b];
                        if (a !== i) {
                            a = c[b];
                            var h = d[b];
                            d[b] = g && (C(h) || C(a)) ? D(g, {}, h, a) : a
                        }
                    }
            return d
        }
        h.G = D;

        function X(f, g) {
            var d = {},
                c, a, b;
            for (c in f) {
                a = f[c];
                b = g[c];
                if (a !== b) {
                    var e;
                    if (C(a) && C(b)) {
                        a = X(a, b);
                        e = !jb(a)
                    }!e && (d[c] = a)
                }
            }
            return d
        }
        h.Gc = function(a) { return E(a) == "function" };
        h.Gf = function(a) { return E(a) == "string" };
        h.Hc = function(a) { return !isNaN(p(a)) && isFinite(a) };
        h.f = n;
        h.Fe = C;

        function R(a) { return j.createElement(a) }
        h.zb = function() { return R("DIV") };
        h.Zc = function() {};

        function y(b, c, a) {
            if (a == i) return b.getAttribute(c);
            b.setAttribute(c, a)
        }

        function U(a, b) { return y(a, b) || y(a, "data-" + b) }
        h.F = y;
        h.m = U;
        h.Rb = function(d, b, c) { var a = h.Oc(y(d, b)); if (isNaN(a)) a = c; return a };

        function z(b, a) { return y(b, "class", a) || "" }

        function ob(b) {
            var a = {};
            n(b, function(b) { if (b != i) a[b] = b });
            return a
        }

        function qb(b, a) { return b.match(a || zb) }

        function Q(b, a) { return ob(qb(b || "", a)) }
        h.qf = ob;
        h.sf = qb;

        function ab(b, c) {
            var a = "";
            n(c, function(c) {
                a && (a += b);
                a += c
            });
            return a
        }

        function F(a, c, b) { z(a, ab(" ", D(X(Q(z(a)), Q(c)), Q(b)))) }
        h.kc = function(a) { return a.parentNode };
        h.Q = function(a) { h.qb(a, "none") };
        h.H = function(a, b) { h.qb(a, b ? "none" : "") };
        h.xf = function(b, a) { b.removeAttribute(a) };
        h.yf = function() { return q() && l < 10 };
        h.tf = function(d, a) {
            if (a) d.style.clip = "rect(" + c.round(a.i || a.z || 0) + "px " + c.round(a.l) + "px " + c.round(a.k) + "px " + c.round(a.g || a.v || 0) + "px)";
            else if (a !== i) {
                var g = d.style.cssText,
                    f = [new RegExp(/[\s]*clip: rect\(.*?\)[;]?/i), new RegExp(/[\s]*cliptop: .*?[;]?/i), new RegExp(/[\s]*clipright: .*?[;]?/i), new RegExp(/[\s]*clipbottom: .*?[;]?/i), new RegExp(/[\s]*clipleft: .*?[;]?/i)],
                    e = J(g, f, "");
                b.Uc(d, e)
            }
        };
        h.Z = function() { return +new Date };
        h.J = function(b, a) { b.appendChild(a) };
        h.Db = function(b, a, c) {
            (c || a.parentNode).insertBefore(b, a)
        };
        h.zc = function(b, a) {
            a = a || b.parentNode;
            a && a.removeChild(b)
        };
        h.mf = function(a, b) { n(a, function(a) { h.zc(a, b) }) };
        h.Fd = function(a) { h.mf(h.Nb(a, d), a) };
        h.Wc = function(a, b) {
            var c = h.kc(a);
            b & 1 && h.M(a, (h.n(c) - h.n(a)) / 2);
            b & 2 && h.K(a, (h.o(c) - h.o(a)) / 2)
        };
        var S = { i: f, l: f, k: f, g: f, q: f, s: f };
        h.te = function(a) {
            var b = h.zb();
            r(b, { ve: "block", rb: h.I(a), i: 0, g: 0, q: 0, s: 0 });
            var d = h.Yc(a, S);
            h.Db(b, a);
            h.J(b, a);
            var e = h.Yc(a, S),
                c = {};
            n(d, function(b, a) { if (b == e[a]) c[a] = b });
            r(b, S);
            r(b, c);
            r(a, { i: 0, g: 0 });
            return c
        };
        h.Oc = p;

        function V(d, c, b) { var a = d.cloneNode(!c);!b && h.xf(a, "id"); return a }
        h.V = V;
        h.Bb = function(e, f) {
            var a = new Image;

            function b(e, d) {
                h.P(a, "load", b);
                h.P(a, "abort", c);
                h.P(a, "error", c);
                f && f(a, d)
            }

            function c(a) { b(a, d) }
            if (gb() && l < 11.6 || !e) b(!e);
            else {
                h.c(a, "load", b);
                h.c(a, "abort", c);
                h.c(a, "error", c);
                a.src = e
            }
        };
        h.le = function(d, a, e) {
            var c = d.length + 1;

            function b(b) { c--; if (a && b && b.src == a.src) a = b;!c && e && e(a) }
            n(d, function(a) { h.Bb(a.src, b) });
            b()
        };
        h.he = function(a, g, i, h) {
            if (h) a = V(a);
            var c = T(a, g);
            if (!c.length) c = b.pf(a, g);
            for (var f = c.length - 1; f > -1; f--) {
                var d = c[f],
                    e = V(i);
                z(e, z(d));
                b.Uc(e, d.style.cssText);
                b.Db(e, d);
                b.zc(d)
            }
            return a
        };

        function Fb(a) {
            var l = this,
                p = "",
                r = ["av", "pv", "ds", "dn"],
                d = [],
                q, k = 0,
                f = 0,
                e = 0;

            function g() {
                F(a, q, (d[e || f & 2 || f] || "") + " " + (d[k] || ""));
                b.mb(a, "pointer-events", e ? "none" : "")
            }

            function c() {
                k = 0;
                g();
                h.P(j, "mouseup", c);
                h.P(j, "touchend", c);
                h.P(j, "touchcancel", c)
            }

            function o(a) {
                if (e) h.Lb(a);
                else {
                    k = 4;
                    g();
                    h.c(j, "mouseup", c);
                    h.c(j, "touchend", c);
                    h.c(j, "touchcancel", c)
                }
            }
            l.ee = function(a) {
                if (a === i) return f;
                f = a & 2 || a & 1;
                g()
            };
            l.Sb = function(a) {
                if (a === i) return !e;
                e = a ? 0 : 3;
                g()
            };
            l.fb = a = h.Ab(a);
            y(a, "data-jssor-button", "1");
            var m = b.sf(z(a));
            if (m) p = m.shift();
            n(r, function(a) { d.push(p + a) });
            q = ab(" ", d);
            d.unshift("");
            h.c(a, "mousedown", o);
            h.c(a, "touchstart", o)
        }
        h.mc = function(a) { return new Fb(a) };
        h.mb = x;
        h.vb = m("overflow");
        h.K = m("top", 2);
        h.Td = m("right", 2);
        h.Qd = m("bottom", 2);
        h.M = m("left", 2);
        h.n = m("width", 2);
        h.o = m("height", 2);
        h.Yd = m("marginLeft", 2);
        h.Ud = m("marginTop", 2);
        h.I = m("position");
        h.qb = m("display");
        h.E = m("zIndex", 1);
        h.tc = function(b, a, c) {
            if (a != i) Eb(b, a, c);
            else return Cb(b)
        };
        h.Uc = function(a, b) {
            if (b != i) a.style.cssText = b;
            else return a.style.cssText
        };
        h.ke = function(b, a) {
            if (a === i) { a = x(b, "backgroundImage") || ""; var c = /\burl\s*\(\s*["']?([^"'\r\n,]+)["']?\s*\)/gi.exec(a) || []; return c[1] }
            x(b, "backgroundImage", a ? "url('" + a + "')" : "")
        };
        var G;
        h.ie = G = { u: h.tc, i: h.K, l: h.Td, k: h.Qd, g: h.M, q: h.n, s: h.o, rb: h.I, ve: h.qb, R: h.E };
        h.Yc = function(c, b) {
            var a = {};
            n(b, function(d, b) { if (G[b]) a[b] = G[b](c) });
            return a
        };

        function r(g, l) {
            var e = vb(),
                b = L(),
                d = tb(),
                j = M(g);

            function k(b, d, a) {
                var e = b.nb(v(-d / 2, -a / 2)),
                    f = b.nb(v(d / 2, -a / 2)),
                    g = b.nb(v(d / 2, a / 2)),
                    h = b.nb(v(-d / 2, a / 2));
                b.nb(v(300, 300));
                return v(c.min(e.x, f.x, g.x, h.x) + d / 2, c.min(e.y, f.y, g.y, h.y) + a / 2)
            }

            function a(d, a) {
                a = a || {};
                var n = a.X || 0,
                    p = (a.Y || 0) % 360,
                    q = (a.U || 0) % 360,
                    u = (a.C || 0) % 360,
                    l = a.B,
                    m = a.A,
                    f = a.Tf;
                if (l == i) l = 1;
                if (m == i) m = 1;
                if (f == i) f = 1;
                if (e) {
                    n = 0;
                    p = 0;
                    q = 0;
                    f = 0
                }
                var c = new Bb(a.lb, a.ib, n);
                c.Y(p);
                c.U(q);
                c.fe(u);
                c.se(a.Gb, a.Ib);
                c.yb(l, m, f);
                if (b) {
                    c.pb(a.v, a.z);
                    d.style[j] = c.ne()
                } else if (!Z || Z < 9) {
                    var o = "",
                        g = { x: 0, y: 0 };
                    if (a.gb) g = k(c, a.gb, a.hb);
                    h.Ud(d, g.y);
                    h.Yd(d, g.x);
                    o = c.Sd();
                    var s = d.style.filter,
                        t = new RegExp(/[\s]*progid:DXImageTransform\.Microsoft\.Matrix\([^\)]*\)/g),
                        r = J(s, [t], o);
                    rb(d, r)
                }
            }
            r = function(e, c) {
                c = c || {};
                var j = c.v,
                    k = c.z,
                    g;
                n(G, function(a, b) {
                    g = c[b];
                    g !== i && a(e, g)
                });
                h.tf(e, c.a);
                if (!b) {
                    j != i && h.M(e, (c.xd || 0) + j);
                    k != i && h.K(e, (c.bd || 0) + k)
                }
                if (c.Pd)
                    if (d) pb(h.O(f, O, e, c));
                    else a(e, c)
            };
            h.dc = O;
            if (d) h.dc = r;
            if (e) h.dc = a;
            else if (!b) a = O;
            h.D = r;
            r(g, l)
        }
        h.dc = r;
        h.D = r;

        function Bb(j, k, o) {
            var d = this,
                b = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, j || 0, k || 0, o || 0, 1],
                i = c.sin,
                h = c.cos,
                l = c.tan;

            function g(a) { return a * c.PI / 180 }

            function n(a, b) { return { x: a, y: b } }

            function m(c, e, l, m, o, r, t, u, w, z, A, C, E, b, f, k, a, g, i, n, p, q, s, v, x, y, B, D, F, d, h, j) { return [c * a + e * p + l * x + m * F, c * g + e * q + l * y + m * d, c * i + e * s + l * B + m * h, c * n + e * v + l * D + m * j, o * a + r * p + t * x + u * F, o * g + r * q + t * y + u * d, o * i + r * s + t * B + u * h, o * n + r * v + t * D + u * j, w * a + z * p + A * x + C * F, w * g + z * q + A * y + C * d, w * i + z * s + A * B + C * h, w * n + z * v + A * D + C * j, E * a + b * p + f * x + k * F, E * g + b * q + f * y + k * d, E * i + b * s + f * B + k * h, E * n + b * v + f * D + k * j] }

            function e(c, a) { return m.apply(f, (a || b).concat(c)) }
            d.yb = function(a, c, d) { if (a != 1 || c != 1 || d != 1) b = e([a, 0, 0, 0, 0, c, 0, 0, 0, 0, d, 0, 0, 0, 0, 1]) };
            d.pb = function(a, c, d) {
                b[12] += a || 0;
                b[13] += c || 0;
                b[14] += d || 0
            };
            d.Y = function(c) {
                if (c) {
                    a = g(c);
                    var d = h(a),
                        f = i(a);
                    b = e([1, 0, 0, 0, 0, d, f, 0, 0, -f, d, 0, 0, 0, 0, 1])
                }
            };
            d.U = function(c) {
                if (c) {
                    a = g(c);
                    var d = h(a),
                        f = i(a);
                    b = e([d, 0, -f, 0, 0, 1, 0, 0, f, 0, d, 0, 0, 0, 0, 1])
                }
            };
            d.fe = function(c) {
                if (c) {
                    a = g(c);
                    var d = h(a),
                        f = i(a);
                    b = e([d, f, 0, 0, -f, d, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1])
                }
            };
            d.se = function(a, c) {
                if (a || c) {
                    j = g(a);
                    k = g(c);
                    b = e([1, l(k), 0, 0, l(j), 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1])
                }
            };
            d.nb = function(c) { var a = e(b, [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, c.x, c.y, 0, 1]); return n(a[12], a[13]) };
            d.ne = function() { return "matrix3d(" + b.join(",") + ")" };
            d.Sd = function() { return "progid:DXImageTransform.Microsoft.Matrix(M11=" + b[0] + ", M12=" + b[4] + ", M21=" + b[1] + ", M22=" + b[5] + ", SizingMethod='auto expand')" }
        }
        new function() {
            var a = this;

            function b(d, g) {
                for (var j = d[0].length, i = d.length, h = g[0].length, f = [], c = 0; c < i; c++)
                    for (var k = f[c] = [], b = 0; b < h; b++) {
                        for (var e = 0, a = 0; a < j; a++) e += d[c][a] * g[a][b];
                        k[b] = e
                    }
                return f
            }
            a.B = function(b, c) { return a.dd(b, c, 0) };
            a.A = function(b, c) { return a.dd(b, 0, c) };
            a.dd = function(a, c, d) {
                return b(a, [
                    [c, 0],
                    [0, d]
                ])
            };
            a.nb = function(d, c) {
                var a = b(d, [
                    [c.x],
                    [c.y]
                ]);
                return v(a[0][0], a[1][0])
            }
        };
        var P = { xd: 0, bd: 0, v: 0, z: 0, N: 1, B: 1, A: 1, C: 0, Y: 0, U: 0, lb: 0, ib: 0, X: 0, Gb: 0, Ib: 0 };
        h.Nd = function(c, d) {
            var a = c || {};
            if (c)
                if (b.Gc(c)) a = { cb: a };
                else if (b.Gc(c.a)) a.a = { cb: c.a };
            a.cb = a.cb || d;
            if (a.a) a.a.cb = a.a.cb || d;
            return a
        };
        h.ad = function(n, j, s, t, B, C, o) {
            var a = j;
            if (n) {
                a = {};
                for (var h in j) {
                    var D = C[h] || 1,
                        z = B[h] || [0, 1],
                        g = (s - z[0]) / z[1];
                    g = c.min(c.max(g, 0), 1);
                    g = g * D;
                    var x = c.floor(g);
                    if (g != x) g -= x;
                    var k = t.cb || e.db,
                        m, E = n[h],
                        p = j[h];
                    if (b.Hc(p)) {
                        k = t[h] || k;
                        var A = k(g);
                        m = E + p * A
                    } else {
                        m = b.G({ Zb: {} }, n[h]);
                        var y = t[h] || {};
                        b.f(p.Zb || p, function(d, a) {
                            k = y[a] || y.cb || k;
                            var c = k(g),
                                b = d * c;
                            m.Zb[a] = b;
                            m[a] += b
                        })
                    }
                    a[h] = m
                }
                var w = b.f(j, function(b, a) { return P[a] != i });
                w && b.f(P, function(c, b) { if (a[b] == i && n[b] !== i) a[b] = n[b] });
                if (w) {
                    if (a.N) a.B = a.A = a.N;
                    a.gb = o.gb;
                    a.hb = o.hb;
                    if (q() && l >= 11 && (j.v || j.z) && s != 0 && s != 1) a.C = a.C || 1e-8;
                    a.Pd = d
                }
            }
            if (j.a && o.pb) {
                var r = a.a.Zb,
                    v = (r.i || 0) + (r.k || 0),
                    u = (r.g || 0) + (r.l || 0);
                a.g = (a.g || 0) + u;
                a.i = (a.i || 0) + v;
                a.a.g -= u;
                a.a.l -= u;
                a.a.i -= v;
                a.a.k -= v
            }
            if (a.a && b.yf() && !a.a.i && !a.a.g && !a.a.z && !a.a.v && a.a.l == o.gb && a.a.k == o.hb) a.a = f;
            return a
        }
    };

    function o() {
        var a = this,
            d = [];

        function i(a, b) { d.push({ xc: a, Fc: b }) }

        function h(a, c) { b.f(d, function(b, e) { b.xc == a && b.Fc === c && d.splice(e, 1) }) }
        a.Cb = a.addEventListener = i;
        a.removeEventListener = h;
        a.j = function(a) {
            var c = [].slice.call(arguments, 1);
            b.f(d, function(b) { b.xc == a && b.Fc.apply(g, c) })
        }
    }
    var l = function(A, E, h, J, M, L) {
        A = A || 0;
        var a = this,
            p, m, n, s, C = 0,
            G, H, F, D, z = 0,
            i = 0,
            l = 0,
            y, j, e, f, o, x, u = [],
            w;

        function O(a) {
            e += a;
            f += a;
            j += a;
            i += a;
            l += a;
            z += a
        }

        function r(p) {
            var g = p;
            if (o)
                if (!x && (g >= f || g < e) || x && g >= e) g = ((g - e) % o + o) % o + e;
            if (!y || s || i != g) {
                var k = c.min(g, f);
                k = c.max(k, e);
                if (!y || s || k != l) {
                    if (L) {
                        var m = (k - j) / (E || 1);
                        if (h.gd) m = 1 - m;
                        var n = b.ad(M, L, m, G, F, H, h);
                        if (w) b.f(n, function(b, a) { w[a] && w[a](J, b) });
                        else b.D(J, n)
                    }
                    a.nc(l - j, k - j);
                    var r = l,
                        q = l = k;
                    b.f(u, function(b, c) {
                        var a = !y && x || g <= i ? u[u.length - c - 1] : b;
                        a.T(l - z)
                    });
                    i = g;
                    y = d;
                    a.Wb(r, q)
                }
            }
        }

        function B(a, b, d) {
            b && a.Ob(f);
            if (!d) {
                e = c.min(e, a.od() + z);
                f = c.max(f, a.jc() + z)
            }
            u.push(a)
        }
        var v = g.requestAnimationFrame || g.webkitRequestAnimationFrame || g.mozRequestAnimationFrame || g.msRequestAnimationFrame;
        if (b.Af() && b.qd() < 7 || !v) v = function(a) { b.Hb(a, h.W) };

        function I() {
            if (p) {
                var d = b.Z(),
                    e = c.min(d - C, h.fd),
                    a = i + e * n;
                C = d;
                if (a * n >= m * n) a = m;
                r(a);
                if (!s && a * n >= m * n) K(D);
                else v(I)
            }
        }

        function q(g, h, j) {
            if (!p) {
                p = d;
                s = j;
                D = h;
                g = c.max(g, e);
                g = c.min(g, f);
                m = g;
                n = m < i ? -1 : 1;
                a.Ad();
                C = b.Z();
                v(I)
            }
        }

        function K(b) {
            if (p) {
                s = p = D = k;
                a.Cd();
                b && b()
            }
        }
        a.Ld = function(a, b, c) { q(a ? i + a : f, b, c) };
        a.Jd = q;
        a.sb = K;
        a.ae = function(a) { q(a) };
        a.ab = function() { return i };
        a.Md = function() { return m };
        a.ub = function() { return l };
        a.T = r;
        a.pb = function(a) { r(i + a) };
        a.Mc = function() { return p };
        a.ce = function(a) { o = a };
        a.Ob = O;
        a.Lc = function(a, b) { B(a, 0, b) };
        a.ic = function(a) { B(a, 1) };
        a.od = function() { return e };
        a.jc = function() { return f };
        a.Wb = a.Ad = a.Cd = a.nc = b.Zc;
        a.hc = b.Z();
        h = b.G({ W: 16, fd: 50 }, h);
        o = h.fc;
        x = h.oe;
        w = h.pe;
        e = j = A;
        f = A + E;
        H = h.Tc || {};
        F = h.Ec || {};
        G = b.Nd(h.L)
    };
    var m = { Ub: "data-scale", vc: "data-scale-ratio", Fb: "data-autocenter" },
        n = new function() {
            var a = this;
            a.tb = function(c, a, e, d) {
                (d || !b.F(c, a)) && b.F(c, a, e)
            };
            a.uc = function(a) {
                var c = b.Rb(a, m.Fb);
                b.Wc(a, c)
            }
        },
        q = new function() {
            var h = this;

            function g(b, a, c) {
                c.push(a);
                b[a] = b[a] || [];
                b[a].push(c)
            }
            h.Xd = function(d) {
                for (var e = [], a, b = 0; b < d.eb; b++)
                    for (a = 0; a < d.p; a++) g(e, c.ceil(1e5 * c.random()) % 13, [b, a]);
                return e
            }
        },
        t = function(m, s, p, u, z, A) {
            var a = this,
                v, h, g, y = 0,
                x = u.je,
                r, i = 8;

            function t(a) {
                if (a.i) a.z = a.i;
                if (a.g) a.v = a.g;
                b.f(a, function(a) { b.Fe(a) && t(a) })
            }

            function j(h, f, g) {
                var a = { W: f, Qb: 1, Hb: 0, p: 1, eb: 1, u: 0, N: 0, a: 0, pb: k, Pb: k, gd: k, Rd: q.Xd, Id: { ge: 0, Od: 0 }, L: e.db, Tc: {}, ac: [], Ec: {} };
                b.G(a, h);
                if (a.eb == 0) a.eb = c.round(a.p * g);
                t(a);
                a.L = b.Nd(a.L, e.db);
                a.Wd = c.ceil(a.Qb / a.W);
                a.me = function(c, b) {
                    c /= a.p;
                    b /= a.eb;
                    var f = c + "x" + b;
                    if (!a.ac[f]) {
                        a.ac[f] = { q: c, s: b };
                        for (var d = 0; d < a.p; d++)
                            for (var e = 0; e < a.eb; e++) a.ac[f][e + "," + d] = { i: e * b, l: d * c + c, k: e * b + b, g: d * c }
                    }
                    return a.ac[f]
                };
                if (a.gc) {
                    a.gc = j(a.gc, f, g);
                    a.Pb = d
                }
                return a
            }

            function n(z, i, a, v, n, l) {
                var y = this,
                    t, u = {},
                    h = {},
                    m = [],
                    f, e, r, p = a.Id.ge || 0,
                    q = a.Id.Od || 0,
                    g = a.me(n, l),
                    o = B(a),
                    C = o.length - 1,
                    s = a.Qb + a.Hb * C,
                    w = v + s,
                    j = a.Pb,
                    x;
                w += 50;

                function B(a) { var b = a.Rd(a); return a.gd ? b.reverse() : b }
                y.Jc = w;
                y.bc = function(d) {
                    d -= v;
                    var e = d < s;
                    if (e || x) {
                        x = e;
                        if (!j) d = s - d;
                        var f = c.ceil(d / a.W);
                        b.f(h, function(a, e) {
                            var d = c.max(f, a.re);
                            d = c.min(d, a.length - 1);
                            if (a.ed != d) {
                                if (!a.ed && !j) b.H(m[e]);
                                else d == a.Zd && j && b.Q(m[e]);
                                a.ed = d;
                                b.D(m[e], a[d])
                            }
                        })
                    }
                };
                i = b.V(i);
                A(i, 0, 0);
                b.f(o, function(i, m) {
                    b.f(i, function(G) {
                        var I = G[0],
                            H = G[1],
                            v = I + "," + H,
                            o = k,
                            s = k,
                            x = k;
                        if (p && H % 2) { if (p & 3) o = !o; if (p & 12) s = !s; if (p & 16) x = !x }
                        if (q && I % 2) { if (q & 3) o = !o; if (q & 12) s = !s; if (q & 16) x = !x }
                        a.i = a.i || a.a & 4;
                        a.k = a.k || a.a & 8;
                        a.g = a.g || a.a & 1;
                        a.l = a.l || a.a & 2;
                        var E = s ? a.k : a.i,
                            B = s ? a.i : a.k,
                            D = o ? a.l : a.g,
                            C = o ? a.g : a.l;
                        a.a = E || B || D || C;
                        r = {};
                        e = { z: 0, v: 0, u: 1, q: n, s: l };
                        f = b.G({}, e);
                        t = b.G({}, g[v]);
                        if (a.u) e.u = 2 - a.u;
                        if (a.R) {
                            e.R = a.R;
                            f.R = 0
                        }
                        var K = a.p * a.eb > 1 || a.a;
                        if (a.N || a.C) {
                            var J = d;
                            if (J) {
                                e.N = a.N ? a.N - 1 : 1;
                                f.N = 1;
                                var N = a.C || 0;
                                e.C = N * 360 * (x ? -1 : 1);
                                f.C = 0
                            }
                        }
                        if (K) {
                            var i = t.Zb = {};
                            if (a.a) {
                                var w = a.Pf || 1;
                                if (E && B) {
                                    i.i = g.s / 2 * w;
                                    i.k = -i.i
                                } else if (E) i.k = -g.s * w;
                                else if (B) i.i = g.s * w;
                                if (D && C) {
                                    i.g = g.q / 2 * w;
                                    i.l = -i.g
                                } else if (D) i.l = -g.q * w;
                                else if (C) i.g = g.q * w
                            }
                            r.a = t;
                            f.a = g[v]
                        }
                        var L = o ? 1 : -1,
                            M = s ? 1 : -1;
                        if (a.x) e.v += n * a.x * L;
                        if (a.y) e.z += l * a.y * M;
                        b.f(e, function(a, c) {
                            if (b.Hc(a))
                                if (a != f[c]) r[c] = a - f[c]
                        });
                        u[v] = j ? f : e;
                        var F = a.Wd,
                            A = c.round(m * a.Hb / a.W);
                        h[v] = new Array(A);
                        h[v].re = A;
                        h[v].Zd = A + F - 1;
                        for (var z = 0; z <= F; z++) {
                            var y = b.ad(f, r, z / F, a.L, a.Ec, a.Tc, { pb: a.pb, gb: n, hb: l });
                            y.R = y.R || 1;
                            h[v].push(y)
                        }
                    })
                });
                o.reverse();
                b.f(o, function(a) {
                    b.f(a, function(c) {
                        var f = c[0],
                            e = c[1],
                            d = f + "," + e,
                            a = i;
                        if (e || f) a = b.V(i);
                        b.D(a, u[d]);
                        b.vb(a, "hidden");
                        b.I(a, "absolute");
                        z.qe(a);
                        m[d] = a;
                        b.H(a, !j)
                    })
                })
            }

            function w() {
                var a = this,
                    b = 0;
                l.call(a, 0, v);
                a.Wb = function(c, a) {
                    if (a - b > i) {
                        b = a;
                        g && g.bc(a);
                        h && h.bc(a)
                    }
                };
                a.Pc = r
            }
            a.be = function() {
                var a = 0,
                    b = u.Ac,
                    d = b.length;
                if (x) a = y++ % d;
                else a = c.floor(c.random() * d);
                b[a] && (b[a].jb = a);
                return b[a]
            };
            a.rf = function(x, y, k, l, b, t) {
                a.kb();
                r = b;
                b = j(b, i, t);
                var f = l.Rc,
                    e = k.Rc;
                f["no-image"] = !l.Tb;
                e["no-image"] = !k.Tb;
                var o = f,
                    q = e,
                    w = b,
                    d = b.gc || j({}, i, t);
                if (!b.Pb) {
                    o = e;
                    q = f
                }
                var u = d.Ob || 0;
                h = new n(m, q, d, c.max(u - d.W, 0), s, p);
                g = new n(m, o, w, c.max(d.W - u, 0), s, p);
                h.bc(0);
                g.bc(0);
                v = c.max(h.Jc, g.Jc);
                a.jb = x
            };
            a.kb = function() {
                m.kb();
                h = f;
                g = f
            };
            a.vf = function() { var a = f; if (g) a = new w; return a };
            if (z && b.Cf() < 537) i = 16;
            o.call(a);
            l.call(a, -1e7, 1e7)
        },
        p = { lc: 1 };
    var u = function(a, e, i) {
            var c = this;
            o.call(c);
            var t, g, h, j;
            b.n(a);
            b.o(a);
            var r, q;

            function l(a) { c.j(p.lc, a, d) }

            function v(c) {
                b.H(a, c);
                b.H(e, c)
            }

            function u() {
                r.Sb(i.Vb || g > 0);
                q.Sb(i.Vb || g < t - i.p)
            }
            c.sc = function(b, a, c) {
                if (c) g = a;
                else {
                    g = b;
                    u()
                }
            };
            c.yc = v;
            var s;
            c.pc = function(c) {
                t = c;
                g = 0;
                if (!s) {
                    b.c(a, "click", b.O(f, l, -j));
                    b.c(e, "click", b.O(f, l, j));
                    r = b.mc(a);
                    q = b.mc(e);
                    s = d
                }
            };
            c.Xb = h = b.G({ nf: 1 }, i);
            j = h.nf;
            if (h.yb == k) {
                n.tb(a, m.Ub, 1);
                n.tb(e, m.Ub, 1)
            }
            if (h.bb) {
                n.tb(a, m.Fb, h.bb);
                n.tb(e, m.Fb, h.bb)
            }
            n.uc(a);
            n.uc(e)
        },
        r = function(i, D) {
            var j = this,
                B, s, a, x = [],
                z, y, e, t, u, w, v, r, l, g, q;
            o.call(j);
            i = b.Ab(i);

            function C(o, g) {
                var h = this,
                    c, n, m;

                function r() { n.ee(s == g) }

                function i(f) {
                    if (f || !l.kf()) {
                        var a = e - g % e,
                            b = l.Kd((g + a) / e - 1),
                            c = b * e + e - a;
                        j.j(p.lc, c, k, d)
                    }
                }
                h.jb = g;
                h.Ed = r;
                m = o.hf || o.Tb || b.zb();
                h.Jb = c = b.he(q, "thumbnailtemplate", m, d);
                n = b.mc(c);
                a.oc & 1 && b.c(c, "click", b.O(f, i, 0));
                a.oc & 2 && b.c(c, "mouseenter", b.O(f, i, 1))
            }
            j.sc = function(b, d, f) {
                var a = s;
                s = b;
                a != -1 && x[a].Ed();
                x[b].Ed();
                !f && l.wf(l.Kd(c.floor(d / e)))
            };
            j.yc = function(a) { b.H(i, a) };
            var A;
            j.pc = function(F, D) {
                if (!A) {
                    B = F;
                    c.ceil(B / e);
                    s = -1;
                    r = c.min(r, D.length);
                    var f = a.ob & 1,
                        o = w + (w + t) * (e - 1) * (1 - f),
                        n = v + (v + u) * (e - 1) * f,
                        q = o + (o + t) * (r - 1) * f,
                        p = n + (n + u) * (r - 1) * (1 - f);
                    b.I(g, "absolute");
                    b.vb(g, "hidden");
                    b.M(g, (z - q) / 2);
                    b.K(g, (y - p) / 2);
                    b.n(g, q);
                    b.o(g, p);
                    var m = [];
                    b.f(D, function(k, h) {
                        var i = new C(k, h),
                            d = i.Jb,
                            a = c.floor(h / e),
                            j = h % e;
                        b.M(d, (w + t) * j * (1 - f));
                        b.K(d, (v + u) * j * f);
                        if (!m[a]) {
                            m[a] = b.zb();
                            b.J(g, m[a])
                        }
                        b.J(m[a], d);
                        x.push(i)
                    });
                    var E = b.G({ xb: 0, id: k, Mf: o, Ff: n, rd: t * f + u * (1 - f), sd: 12, td: 200, ud: 1, vd: a.ob, wd: a.Ue || a.Qf ? 0 : a.ob }, a);
                    l = new h(i, E);
                    j.He = l.He;
                    A = d
                }
            };
            j.Xb = a = b.G({ Kc: 0, Ic: 0, p: 1, ob: 1, oc: 1 }, D);
            z = b.n(i);
            y = b.o(i);
            g = b.Mb(i, "slides", d);
            q = b.Mb(g, "prototype");
            w = b.n(q);
            v = b.o(q);
            b.zc(q, g);
            e = a.eb || 1;
            t = a.Kc;
            u = a.Ic;
            r = a.p;
            a.yb == k && n.tb(i, m.Ub, 1);
            a.bb &= a.ob;
            a.bb && n.tb(i, m.Fb, a.bb);
            n.uc(i)
        };

    function s(e, d, c) {
        var a = this;
        l.call(a, 0, c);
        a.md = b.Zc;
        a.kd = 0;
        a.hd = c
    }
    var h = function() {
        var a = this;
        b.Df(a, o);
        var Qb = "data-jssor-slider",
            Tb = "data-jssor-thumb",
            v, n, U, eb, V, ob, db, hb, H, G, Kb, hc = 1,
            cc = 1,
            Ub = 1,
            Vb = {},
            z, T, Pb, vb, ub, pb, Ob, Nb, cb, r = -1,
            N, Ab, q, K, I, Hb, lb, mb, nb, t, R, x, P, Jb, Y = [],
            Zb, ac, Wb, ic, Bc, u, fb, F, Yb, kb, yb, Bb, jb, Cb, L, gb, Q, J = 1,
            S, D, X, Db = 0,
            Eb = 0,
            M, qb, ib, Mb, y, ab, A, tb, Z = [],
            rb = b.lf(),
            Gb = rb.ef,
            B = [],
            C, O, E, xb, Sb, W;

        function qc(e, n, o) {
            var k = this,
                h = { i: 2, l: 1, k: 2, g: 1 },
                l = { i: "top", l: "right", k: "bottom", g: "left" },
                g, a, f, i, j = {};
            k.fb = e;
            k.Yb = function(q, k) {
                var p, s = q,
                    r = k;
                if (!f) {
                    f = b.te(e);
                    g = e.parentNode;
                    i = { yb: b.Rb(e, m.Ub, 1), bb: b.Rb(e, m.Fb) };
                    b.f(l, function(c, a) { j[a] = b.Rb(e, "data-scale-" + c, 1) });
                    a = e;
                    if (n) {
                        a = b.V(g, d);
                        b.D(a, { i: 0, g: 0 });
                        b.J(a, e);
                        b.J(g, a)
                    }
                }
                if (o) p = q > k ? q : k;
                else s = r = p = c.pow(H < G ? k : q, i.yb);
                b.Ef(a, p);
                b.F(a, m.vc, p);
                b.n(g, f.q * s);
                b.o(g, f.s * r);
                var t = b.yd() && b.pd() < 9 || b.pd() < 10 && b.gf() ? p : 1,
                    u = (s - t) * f.q / 2,
                    v = (r - t) * f.s / 2;
                b.M(a, u);
                b.K(a, v);
                b.f(f, function(d, a) {
                    if (h[a] && d) {
                        var e = (h[a] & 1) * c.pow(q, j[a]) * d + (h[a] & 2) * c.pow(k, j[a]) * d / 2;
                        b.ie[a](g, e)
                    }
                });
                b.Wc(g, i.bb)
            }
        }

        function Ac() {
            var b = this;
            l.call(b, -1e8, 2e8);
            b.Ce = function() {
                var a = b.ub(),
                    d = c.floor(a),
                    f = w(d),
                    e = a - c.floor(a);
                return { jb: f, Be: d, rb: e }
            };
            b.Wb = function(e, b) {
                var f = c.floor(b);
                if (f != b && b > e) f++;
                bc(f, d);
                a.j(h.Ae, w(b), w(e), b, e)
            }
        }

        function zc() {
            var a = this;
            l.call(a, 0, 0, { fc: q });
            b.f(B, function(b) {
                L & 1 && b.ce(q);
                a.ic(b);
                b.Ob(jb / nb)
            })
        }

        function yc() {
            var a = this,
                b = tb.fb;
            l.call(a, -1, 2, { L: e.db, pe: { rb: gc }, fc: q }, b, { rb: 1 }, { rb: -2 });
            a.Jb = b
        }

        function rc(o, m) {
            var b = this,
                e, g, i, j, c;
            l.call(b, -1e8, 2e8, { fd: 100 });
            b.Ad = function() {
                S = d;
                X = f;
                a.j(h.ze, w(y.ab()), y.ab())
            };
            b.Cd = function() {
                S = k;
                j = k;
                var b = y.Ce();
                a.j(h.ye, w(y.ab()), y.ab());
                !b.rb && Cc(b.Be, r)
            };
            b.Wb = function(f, d) {
                var a;
                if (j) a = c;
                else {
                    a = g;
                    if (i) {
                        var b = d / i;
                        a = n.Nc(b) * (g - e) + e
                    }
                }
                y.T(a)
            };
            b.cc = function(a, d, c, f) {
                e = a;
                g = d;
                i = c;
                y.T(a);
                b.T(0);
                b.Jd(c, f)
            };
            b.xe = function(a) {
                j = d;
                c = a;
                b.Ld(a, f, d)
            };
            b.we = function(a) { c = a };
            y = new Ac;
            y.Lc(o);
            y.Lc(m)
        }

        function sc() {
            var c = this,
                a = ec();
            b.E(a, 0);
            b.mb(a, "pointerEvents", "none");
            c.fb = a;
            c.qe = function(c) {
                b.J(a, c);
                b.H(a)
            };
            c.kb = function() {
                b.Q(a);
                b.Fd(a)
            }
        }

        function xc(m, g) {
            var e = this,
                s, M, v, j, z = [],
                y, D, S, H, P, F, J, i, x, p;
            l.call(e, -t, t + 1, {});

            function E(a) {
                s && s.md();
                R(m, a, 0);
                F = d;
                s = new V.S(m, V, b.Oc(b.m(m, "idle")) || Yb, !u);
                s.T(0)
            }

            function Y() { s.hc < V.hc && E() }

            function N(p, r, o) {
                if (!H) {
                    H = d;
                    if (j && o) {
                        var f = o.width,
                            c = o.height,
                            m = f,
                            l = c;
                        if (f && c && n.Eb) {
                            if (n.Eb & 3 && (!(n.Eb & 4) || f > K || c > I)) {
                                var i = k,
                                    q = K / I * c / f;
                                if (n.Eb & 1) i = q > 1;
                                else if (n.Eb & 2) i = q < 1;
                                m = i ? f * I / c : K;
                                l = i ? I : c * K / f
                            }
                            b.n(j, m);
                            b.o(j, l);
                            b.K(j, (I - l) / 2);
                            b.M(j, (K - m) / 2)
                        }
                        b.I(j, "absolute");
                        a.j(h.ue, g)
                    }
                }
                b.Q(r);
                p && p(e)
            }

            function W(f, b, c, d) {
                if (d == X && r == g && u)
                    if (!Bc) {
                        var a = w(f);
                        C.rf(a, g, b, e, c, I / K);
                        b.Je();
                        ab.Ob(a - ab.od() - 1);
                        ab.T(a);
                        A.cc(a, a, 0)
                    }
            }

            function bb(b) {
                if (b == X && r == g) {
                    if (!i) {
                        var a = f;
                        if (C)
                            if (C.jb == g) a = C.vf();
                            else C.kb();
                        Y();
                        i = new wc(m, g, a, s);
                        i.Bd(p)
                    }!i.Mc() && i.qc()
                }
            }

            function G(a, d, k) {
                if (a == g) {
                    if (a != d) B[d] && B[d].Xc();
                    else !k && i && i.Ke();
                    p && p.Sb();
                    var l = X = b.Z();
                    e.Bb(b.O(f, bb, l))
                } else {
                    var j = c.min(g, a),
                        h = c.max(g, a),
                        o = c.min(h - j, j + q - h),
                        m = t + n.Le - 1;
                    (!P || o <= m) && e.Bb()
                }
            }

            function db() {
                if (r == g && i) {
                    i.sb();
                    p && p.Me();
                    p && p.cf();
                    i.zd()
                }
            }

            function eb() { r == g && i && i.sb() }

            function Z(b) {!Q && a.j(h.bf, g, b) }

            function O() {
                p = x.pInstance;
                i && i.Bd(p)
            }
            e.Bb = function(e, c) {
                c = c || v;
                if (z.length && !H) {
                    b.H(c);
                    if (!S) {
                        S = d;
                        a.j(h.af, g);
                        b.f(z, function(a) {
                            if (!b.F(a, "src")) {
                                a.src = b.m(a, "src2") || "";
                                b.qb(a, a["display-origin"])
                            }
                        })
                    }
                    b.le(z, j, b.O(f, N, e, c))
                } else N(e, c)
            };
            e.Ze = function() {
                if (q == 1) {
                    e.Xc();
                    G(g, g)
                } else if (C) {
                    var a = C.be(q);
                    if (a) {
                        var h = X = b.Z(),
                            c = g + fb,
                            d = B[w(c)];
                        return d.Bb(b.O(f, W, c, d, a, h), v)
                    }
                } else Ib(fb)
            };
            e.ec = function() { G(g, g, d) };
            e.Xc = function() {
                p && p.Me();
                p && p.cf();
                e.cd();
                i && i.Ye();
                i = f;
                E()
            };
            e.Je = function() { b.Q(m) };
            e.cd = function() { b.H(m) };
            e.We = function() { p && p.Sb() };

            function R(a, f, c, h) {
                if (b.F(a, Qb)) return;
                if (!F) {
                    if (a.tagName == "IMG") {
                        z.push(a);
                        if (!b.F(a, "src")) {
                            P = d;
                            a["display-origin"] = b.qb(a);
                            b.Q(a)
                        }
                    }
                    var e = b.ke(a);
                    if (e) {
                        var g = new Image;
                        b.m(g, "src2", e);
                        z.push(g)
                    }
                    c && b.E(a, (b.E(a) || 0) + 1)
                }
                var i = b.Nb(a);
                b.f(i, function(a) {
                    var e = a.tagName,
                        g = b.m(a, "u");
                    if (g == "player" && !x) {
                        x = a;
                        if (x.pInstance) O();
                        else b.c(x, "dataavailable", O)
                    }
                    if (g == "caption") {
                        if (f) {
                            b.Hf(a, b.m(a, "to"));
                            b.If(a, b.m(a, "bf"));
                            J && b.m(a, "3d") && b.Kf(a, "preserve-3d")
                        }
                    } else if (!F && !c && !j) {
                        if (e == "A") {
                            if (b.m(a, "u") == "image") j = b.of(a, "IMG");
                            else j = b.Mb(a, "image", d);
                            if (j) {
                                y = a;
                                b.qb(y, "block");
                                b.D(y, cb);
                                D = b.V(y, d);
                                b.I(y, "relative");
                                b.tc(D, 0);
                                b.mb(D, "backgroundColor", "#000")
                            }
                        } else if (e == "IMG" && b.m(a, "u") == "image") j = a;
                        if (j) {
                            j.border = 0;
                            b.D(j, cb)
                        }
                    }
                    R(a, f, c + 1, h)
                })
            }
            e.nc = function(c, b) {
                var a = t - b;
                gc(M, a)
            };
            e.jb = g;
            o.call(e);
            J = b.m(m, "p");
            b.Lf(m, J);
            b.zf(m, b.m(m, "po"));
            var L = b.Mb(m, "thumb", d);
            if (L) {
                e.hf = b.V(L);
                b.Q(L)
            }
            b.H(m);
            v = b.V(T);
            b.E(v, 1e3);
            b.c(m, "click", Z);
            E(d);
            e.Tb = j;
            e.nd = D;
            e.Rc = m;
            e.Jb = M = m;
            b.J(M, v);
            a.Cb(203, G);
            a.Cb(28, eb);
            a.Cb(24, db)
        }

        function wc(z, g, p, q) {
            var c = this,
                n = 0,
                v = 0,
                i, j, f, e, m, t, s, o = B[g];
            l.call(c, 0, 0);

            function w() {
                b.Fd(O);
                ic && m && o.nd && b.J(O, o.nd);
                b.H(O, !m && o.Tb)
            }

            function x() { c.qc() }

            function y(a) {
                s = a;
                c.sb();
                c.qc()
            }
            c.qc = function() {
                var b = c.ub();
                if (!D && !S && !s && r == g) {
                    if (!b) {
                        if (i && !m) {
                            m = d;
                            c.zd(d);
                            a.j(h.df, g, n, v, i, e)
                        }
                        w()
                    }
                    var k, p = h.Hd;
                    if (b != e)
                        if (b == f) k = e;
                        else if (b == j) k = f;
                    else if (!b) k = j;
                    else k = c.Md();
                    a.j(p, g, b, n, j, f, e);
                    var l = u && (!F || J);
                    if (b == e)(f != e && !(F & 12) || l) && o.Ze();
                    else(l || b != f) && c.Jd(k, x)
                }
            };
            c.Ke = function() { f == e && f == c.ub() && c.T(j) };
            c.Ye = function() {
                C && C.jb == g && C.kb();
                var b = c.ub();
                b < e && a.j(h.Hd, g, -b - 1, n, j, f, e)
            };
            c.zd = function(a) { p && b.vb(R, a && p.Pc.Rf ? "" : "hidden") };
            c.nc = function(c, b) {
                if (m && b >= i) {
                    m = k;
                    w();
                    o.cd();
                    C.kb();
                    a.j(h.Ve, g, n, v, i, e)
                }
                a.j(h.Te, g, b, n, j, f, e)
            };
            c.Bd = function(a) {
                if (a && !t) {
                    t = a;
                    a.Cb($JssorPlayer$.Vd, y)
                }
            };
            p && c.ic(p);
            i = c.jc();
            c.ic(q);
            j = i + q.kd;
            e = c.jc();
            f = u ? i + q.hd : e
        }

        function zb(a, c, d) {
            b.M(a, c);
            b.K(a, d)
        }

        function gc(c, b) {
            var a = x > 0 ? x : U,
                d = lb * b * (a & 1),
                e = mb * b * (a >> 1 & 1);
            zb(c, d, e)
        }

        function Xb() {
            xb = S;
            Sb = A.Md();
            E = y.ab()
        }

        function kc() {
            Xb();
            if (D || !J && F & 12) {
                A.sb();
                a.j(h.Se)
            }
        }

        function jc(f) {
            if (!D && (J || !(F & 12)) && !A.Mc()) {
                var b = y.ab(),
                    a = c.ceil(E);
                if (f && c.abs(M) >= n.sd) {
                    a = c.ceil(b);
                    a += ib
                }
                if (!(L & 1)) a = c.min(q - t, c.max(a, 0));
                var d = c.abs(a - b);
                if (d < 1 && n.Nc != e.db) d = 1 - c.pow(1 - d, 5);
                if (!Q && xb) A.ae(Sb);
                else if (b == a) {
                    Ab.We();
                    Ab.ec()
                } else A.cc(b, a, d * kb)
            }
        }

        function Rb(a) {!b.wb(b.rc(a), "nodrag") && b.Lb(a) }

        function uc(a) { fc(a, 1) }

        function fc(c, g) {
            c = b.ld(c);
            var e = b.rc(c);
            Jb = k;
            var l = b.wb(e, "1", Tb);
            if ((!l || l === v) && !P && (!g || c.touches.length == 1) && !b.wb(e, "nodrag") && vc()) {
                var n = b.wb(e, i, m.vc);
                if (n) Ub = b.F(n, m.vc);
                if (g) {
                    var p = c.touches[0];
                    Db = p.clientX;
                    Eb = p.clientY
                } else {
                    var o = b.jd(c);
                    Db = o.x;
                    Eb = o.y
                }
                D = d;
                X = f;
                b.c(j, g ? "touchmove" : "mousemove", Lb);
                b.Z();
                Q = 0;
                kc();
                if (!xb) x = 0;
                M = 0;
                qb = 0;
                ib = 0;
                a.j(h.Re, w(E), E, c)
            }
        }

        function Lb(a) {
            if (D) {
                a = b.ld(a);
                var e;
                if (a.type != "mousemove")
                    if (a.touches.length == 1) {
                        var m = a.touches[0];
                        e = { x: m.clientX, y: m.clientY }
                    } else bb();
                else e = b.jd(a);
                if (e) {
                    var f = e.x - Db,
                        g = e.y - Eb;
                    if (x || c.abs(f) > 1.5 || c.abs(g) > 1.5) {
                        if (c.floor(E) != E) x = x || U & P;
                        if ((f || g) && !x)
                            if (P == 3)
                                if (c.abs(g) > c.abs(f)) x = 2;
                                else x = 1;
                        else {
                            x = P;
                            var n = [0, c.abs(f), c.abs(g)],
                                p = n[x],
                                o = n[~x & 3];
                            if (o > p) Jb = d
                        }
                        if (x && !Jb) {
                            var l = g,
                                h = mb;
                            if (x == 1) {
                                l = f;
                                h = lb
                            }
                            if (M - qb < -1.5) ib = 0;
                            else if (M - qb > 1.5) ib = -1;
                            qb = M;
                            M = l;
                            W = E - M / h / Ub;
                            if (!(L & 1)) {
                                var j = 0,
                                    i = [-E, 0, E - q + t];
                                b.f(i, function(b, d) {
                                    if (b > 0) {
                                        var a = c.pow(b, 1 / 1.6);
                                        a = c.tan(a * c.PI / 2);
                                        j = (a - b) * (d - 1)
                                    }
                                });
                                var k = j + W;
                                i = [-k, 0, k - q + t];
                                b.f(i, function(a, b) {
                                    if (a > 0) {
                                        a = c.min(a, h);
                                        a = c.atan(a) * 2 / c.PI;
                                        a = c.pow(a, 1.6);
                                        W = a * (b - 1);
                                        if (b) W += q - t
                                    }
                                })
                            }
                            b.Lb(a);
                            if (!S) A.xe(W);
                            else A.we(W)
                        }
                    }
                }
            }
        }

        function bb() {
            tc();
            if (D) {
                D = k;
                Q = M;
                b.Z();
                b.P(j, "mousemove", Lb);
                b.P(j, "touchmove", Lb);
                Q && u & 8 && (u = 0);
                A.sb();
                var c = y.ab();
                a.j(h.Qe, w(c), c, w(E), E);
                F & 12 && Xb();
                jc(d)
            }
        }

        function pc(c) {
            var a = b.rc(c),
                d = b.wb(a, "1", Qb);
            if (v === d)
                if (Q) {
                    b.jf(c);
                    while (a && v !== a) {
                        (a.tagName == "A" || b.F(a, "data-jssor-button")) && b.Lb(c);
                        a = a.parentNode
                    }
                } else u & 4 && (u = 0)
        }

        function lc(a) {
            B[r];
            r = w(a);
            Ab = B[r];
            y.T(r);
            bc(r);
            return r
        }

        function Cc(b, c) {
            x = 0;
            lc(b);
            if (u & 2 && (fb > 0 && r == q - 1 || fb < 0 && !r)) u = 0;
            a.j(h.Pe, r, c)
        }

        function bc(a, c) {
            N = a;
            b.f(Y, function(b) { b.sc(w(a), a, c) })
        }

        function vc() {
            var b = h.Dd || 0,
                a = gb;
            if (Gb) a & 1 && (a &= 1);
            h.Dd |= a;
            return P = a & ~b
        }

        function tc() {
            if (P) {
                h.Dd &= ~gb;
                P = 0
            }
        }

        function ec() {
            var a = b.zb();
            b.D(a, cb);
            b.I(a, "absolute");
            return a
        }

        function w(b, a) { a = a || q || 1; return (b % a + a) % a }

        function wb(c, a, b) {
            u & 8 && (u = 0);
            sb(c, kb, a, b)
        }

        function Fb() { b.f(Y, function(a) { a.yc(a.Xb.Sf <= J) }) }

        function nc() {
            if (!J) {
                J = 1;
                Fb();
                if (!D) {
                    F & 12 && jc();
                    F & 3 && B[r] && B[r].ec()
                }
            }
            a.j(h.Oe)
        }

        function mc() {
            if (J) {
                J = 0;
                Fb();
                D || !(F & 12) || kc()
            }
            a.j(h.Ne)
        }

        function oc() {
            b.f(Z, function(a) {
                b.D(a, cb);
                b.I(a, "absolute");
                b.vb(a, "hidden");
                b.Q(a)
            });
            b.D(T, cb)
        }

        function Ib(b, a) { sb(b, a, d) }

        function sb(g, f, m, o) {
            if (Cb && (!D && (J || !(F & 12)) || n.id)) {
                S = d;
                D = k;
                A.sb();
                if (f == i) f = kb;
                var e = Mb.ub(),
                    b = g;
                if (m) {
                    b = N + g;
                    if (g > 0) b = c.ceil(b);
                    else b = c.floor(b)
                }
                var a = b;
                if (!(L & 1))
                    if (o) a = w(a);
                    else if (L & 2 && (a < 0 && !N || a > q - t && N >= q - t)) a = a < 0 ? q - t : 0;
                else a = c.max(0, c.min(a, q - t));
                var l = (a - e) % q;
                a = e + l;
                var h = e == a ? 0 : f * c.abs(l),
                    j = 1;
                if (t > 1) j = (U & 1 ? Ob : Nb) / nb;
                h = c.min(h, f * j * 1.5);
                A.cc(e, a, h || 1)
            }
        }
        a.xb = function(a) {
            if (a == i) return a;
            if (a != u) {
                u = a;
                u && B[r] && B[r].ec()
            }
        };
        a.kf = function() { return Q };
        a.gb = function() { return H };
        a.hb = function() { return G };
        a.ff = function(b) {
            if (b == i) return Kb || H;
            a.Yb(b, b / H * G)
        };
        a.Yb = function(c, a) {
            b.n(v, c);
            b.o(v, a);
            hc = c / H;
            cc = a / G;
            b.f(Vb, function(a) { a.Yb(hc, cc) });
            if (!Kb) {
                b.Db(R, z);
                b.K(R, 0);
                b.M(R, 0)
            }
            Kb = c
        };
        a.wf = sb;
        a.Ld = function() { a.xb(u || 1) };
        a.Kd = function(a) {
            var d = c.ceil(w(jb / nb)),
                b = w(a - N + d);
            if (b > t) {
                if (a - N > q / 2) a -= q;
                else if (a - N <= -q / 2) a += q
            } else a = N + b - d;
            if (!(L & 1)) a = w(a);
            return a
        };
        a.Kb = function(r, l) {
            a.fb = v = b.Ab(r);
            H = b.n(v);
            G = b.o(v);
            n = b.G({ Eb: 0, Le: 1, Dc: 1, Cc: 0, xb: 0, Vb: 1, Bc: d, id: d, Ee: 1, Vc: 3e3, ud: 1, td: 500, Nc: e.Xe, sd: 20, rd: 0, p: 1, wc: 0, Ge: 1, vd: 1, wd: 1 }, l);
            n.Bc = n.Bc && b.Bf();
            if (n.Ie != i) n.Vc = n.Ie;
            if (n.Jf != i) n.wc = n.Jf;
            U = n.vd & 3;
            eb = n.De;
            V = b.G({ S: s }, n.Nf);
            ob = n.Of;
            db = n.uf;
            hb = n.de;
            !n.Ge;
            var x = b.Nb(v);
            b.f(x, function(a, d) {
                var c = b.m(a, "u");
                if (c == "loading") T = a;
                else { if (c == "slides") z = a; if (c == "navigator") Pb = a; if (c == "arrowleft") vb = a; if (c == "arrowright") ub = a; if (c == "thumbnavigator") pb = a; if (a.tagName == "DIV" || a.tagName == "SPAN") Vb[c || d] = new qc(a, c == "slides", b.qf(["slides", "thumbnavigator"])[c]) }
            });
            T = T || b.zb(j);
            Ob = b.n(z);
            Nb = b.o(z);
            K = n.Mf || Ob;
            I = n.Ff || Nb;
            cb = { q: K, s: I, i: 0, g: 0 };
            Hb = n.rd;
            lb = K + Hb;
            mb = I + Hb;
            nb = U & 1 ? lb : mb;
            fb = n.Ee;
            F = n.ud;
            Yb = n.Vc;
            kb = n.td;
            tb = new sc;
            if (n.Bc) zb = function(a, c, d) { b.dc(a, { lb: c, ib: d }) };
            u = n.xb & 63;
            a.Xb = l;
            b.F(v, Qb, "1");
            b.E(z, b.E(z) || 0);
            b.I(z, "absolute");
            R = b.V(z, d);
            b.Db(R, z);
            ab = new yc;
            b.J(R, ab.Jb);
            b.vb(z, "hidden");
            F &= Gb ? 10 : 5;
            var y = b.Nb(z);
            b.f(y, function(a) {
                a.tagName == "DIV" && !b.m(a, "u") && Z.push(a);
                b.E(a, (b.E(a) || 0) + 1)
            });
            O = ec();
            b.mb(O, "backgroundColor", "#000");
            b.tc(O, 0);
            b.E(O, 0);
            b.Db(O, z.firstChild, z);
            q = Z.length;
            t = c.min(n.p, q);
            Cb = t < q;
            L = Cb ? n.Vb : 0;
            if (q) {
                oc();
                if (eb) {
                    ic = eb.Uf;
                    yb = eb.S;
                    Bb = t == 1 && q > 1 && yb && (!b.yd() || b.qd() >= 9)
                }
                jb = Bb || t >= q || !(L & 1) ? 0 : n.wc;
                gb = (t > 1 || jb ? U : -1) & n.wd;
                rb.Qc && b.mb(z, rb.Qc, ([f, "pan-y", "pan-x", "none"])[gb] || "");
                if (Bb) C = new yb(tb, K, I, eb, Gb, zb);
                for (var k = 0; k < Z.length; k++) {
                    var m = Z[k],
                        o = new xc(m, k);
                    B.push(o)
                }
                b.Q(T);
                Mb = new zc;
                A = new rc(Mb, ab);
                b.c(v, "click", pc, d);
                b.c(v, "mouseleave", nc);
                b.c(v, "mouseenter", mc);
                if (gb) {
                    b.c(v, "mousedown", fc);
                    b.c(v, "touchstart", uc);
                    b.c(v, "dragstart", Rb);
                    b.c(v, "selectstart", Rb);
                    b.c(g, "mouseup", bb);
                    b.c(j, "mouseup", bb);
                    b.c(j, "touchend", bb);
                    b.c(j, "touchcancel", bb);
                    b.c(g, "blur", bb)
                }
                if (Pb && ob) {
                    Zb = new ob.S(Pb, ob, H, G);
                    Y.push(Zb)
                }
                if (db && vb && ub) {
                    db.Vb = L;
                    db.p = t;
                    ac = new db.S(vb, ub, db, H, G);
                    Y.push(ac)
                }
                if (pb && hb) {
                    hb.Cc = n.Cc;
                    Wb = new hb.S(pb, hb);
                    b.F(pb, Tb, "1");
                    Y.push(Wb)
                }
                b.f(Y, function(a) {
                    a.pc(q, B, T);
                    a.Cb(p.lc, wb)
                });
                b.mb(v, "visibility", "visible");
                a.Yb(H, G);
                Fb();
                n.Dc && b.c(j, "keydown", function(a) {
                    if (a.keyCode == 37) wb(-n.Dc, d);
                    else a.keyCode == 39 && wb(n.Dc, d)
                });
                var h = n.Cc;
                h = w(h);
                A.cc(h, h, 0)
            }
        };
        b.Kb(a)
    };
    h.bf = 21;
    h.Re = 22;
    h.Qe = 23;
    h.ze = 24;
    h.ye = 25;
    h.af = 26;
    h.ue = 27;
    h.Se = 28;
    h.Ne = 31;
    h.Oe = 32;
    h.Ae = 202;
    h.Pe = 203;
    h.df = 206;
    h.Ve = 207;
    h.Te = 208;
    h.Hd = 209;
    jssor_1_slider_init = function() {
        var i = [{ Qb: 1200, x: -.3, Ec: { g: [.3, .7] }, L: { g: e.Sc, u: e.db }, u: 2 }, { Qb: 1200, x: .3, Pb: d, L: { g: e.Sc, u: e.db }, u: 2 }],
            j = { xb: 1, De: { S: t, Ac: i, je: 1 }, uf: { S: u }, de: { S: r, p: 1, ob: 2, wc: 0, Ue: d } },
            f = new h("jssor_1", j);

        function a() {
            var b = f.fb.parentNode.clientWidth;
            if (b) {
                b = c.min(b, 1180);
                f.ff(b)
            } else g.setTimeout(a, 30)
        }
        a();
        b.c(g, "load", a);
        b.c(g, "resize", a);
        b.c(g, "orientationchange", a)
    }
})(window, document, Math, null, true, false)