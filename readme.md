- git clone https://gitlab.com/ibnukipa/kipa-ppi.git
- Create a database for the project
- From the projects root run cp .env.example .env

- Configure .env file


-- setting database nya
-- setting recaptcha nya
-- yang lain biarin dulu aja


- Run composer update from the projects root folder
- From the projects root folder run php artisan key:generate

- From the projects root folder run php artisan migrate

- From the projects root folder run composer dump-autoload

- From the projects root folder run php artisan db:seed