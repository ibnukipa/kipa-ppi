<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
[
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
], 
function () {
    try {
        DB::connection()->getPdo();
        // if(DB::connection()->getDatabaseName()) {
        //     die("Yes! Successfully connected to the DB: " . DB::connection()->getDatabaseName());
        // }

    } catch (\Exception $e) {
        abort(500);
    }

    Route::get('/', function () {
        abort(404);
    })->name('404');

    Route::get('/', [
        'menu_name'     => 'home',
        'uses'          => 'PageController@index',
    ]);

    Route::get('/search', [
        'menu_name'     => 'home',
        'uses'          => 'PageController@search',
    ]);

    Auth::routes();

    // IF YOU NEED ROLED MENUS
    Route::group(['prefix' => 'admin', 'middleware' => 'roles'], function() {
        Route::get('/', [
            'menu_name'         => 'dashboard',
            'page_name'         => 'Dashboard',
            'page_description'  => 'Monitoring semua kegiatan Admin',
            'uses'              => 'DashboardController@index',
            'default_roles'     => 'all',
        ])->name('admin');

        Route::get('/edit/{idpage}', [
            'uses'              => 'DashboardController@editPage',
            'default_roles'     => 'all',
        ])->name('edit.page');

        Route::get('/dashboard', [
            'menu_name'         => 'dashboard',
            'page_name'         => 'Dashboard',
            'page_description'  => 'Monitoring semua kegiatan Admin',
            'uses'              => 'DashboardController@index',
            'default_roles'     => 'all',
        ])->name('dashboard');

        Route::post('/file/store', [
            'menu_name'         => 'store_file',
            'uses'              => 'FileController@store',
            'default_roles'     => 'all',
        ])->name('file.store');

        Route::get('/file/get/{filename}', [
            'menu_name'         => 'get_file',
            'uses'              => 'FileController@getContent',
            'default_roles'     => 'all',
        ])->name('file.get');

        Route::get('/page', [
            'menu_name'         => 'main_page',
            'page_name'         => 'Halaman',
            'page_description'  => 'Pengelolaan untuk semua halaman web',
            'uses'              => 'PageController@show',
            'default_roles'     => 'all',
        ])->name('page.main');

        Route::get('/page/create', [
            'menu_name'         => 'create_page',
            'page_name'         => 'Tambah Laman Baru',
            'page_description'  => 'Membuat Laman baru untuk website',
            'uses'              => 'PageController@create',
            'default_roles'     => 'all',
        ])->name('page.create');

        Route::get('/page/edit/{id}', [
            'menu_name'         => 'edit_page',
            'page_name'         => 'Edit Laman',
            'page_description'  => 'Perbarui Laman yang telah terbuat',
            'uses'              => 'PageController@edit',
            'default_roles'     => 'all',
        ])->name('page.edit');

        Route::post('/page/store', [
            'menu_name'         => 'store_page',
            'uses'              => 'PageController@store',
            'default_roles'     => 'all',
        ])->name('page.store');

        Route::post('/page/update', [
            'menu_name'         => 'update_page',
            'uses'              => 'PageController@update',
            'default_roles'     => 'all',
        ])->name('page.update');

        Route::get('/page/destroy/{id}', [
            'menu_name'         => 'delete_page',
            'uses'              => 'PageController@destroy',
            'default_roles'     => 'all',
        ])->name('page.destroy');

        Route::get('/api/page/urlisexsist/{url}', [
            'uses'              => 'PageApiController@cekUrlIsExsist',
            'default_roles'     => 'all',
        ])->name('page.api.urlisexsist');

        //journal
        Route::get('/journal', [
            'menu_name'         => 'main_page',
            'page_name'         => 'Jurnal',
            'page_description'  => 'Pengelolaan untuk semua jurnal',
            'uses'              => 'JournalController@show',
            'default_roles'     => 'all',
        ])->name('journal.main');

        Route::get('/journal/create', [
            'menu_name'         => 'create_journal',
            'page_name'         => 'Tambah Jurnal Baru',
            'page_description'  => 'Membuat Jurnal baru untuk website',
            'uses'              => 'JournalController@create',
            'default_roles'     => 'all',
        ])->name('journal.create');

        Route::get('/journal/edit/{id}', [
            'menu_name'         => 'edit_journal',
            'page_name'         => 'Edit Journal',
            'page_description'  => 'Perbarui Journal yang telah terbuat',
            'uses'              => 'JournalController@edit',
            'default_roles'     => 'all',
        ])->name('journal.edit');

        Route::post('/journal/store', [
            'menu_name'         => 'store_journal',
            'uses'              => 'JournalController@store',
            'default_roles'     => 'all',
        ])->name('journal.store');

        Route::post('/journal/update', [
            'menu_name'         => 'update_journal',
            'uses'              => 'JournalController@update',
            'default_roles'     => 'all',
        ])->name('journal.update');

        Route::get('/journal/destroy/{id}', [
            'menu_name'         => 'delete_journal',
            'uses'              => 'JournalController@destroy',
            'default_roles'     => 'all',
        ])->name('journal.destroy');

        Route::get('/api/journal/oai/update', [
            'uses'              => 'JournalApiController@oaiupdatejournal',
            'default_roles'     => 'all',
        ])->name('oai.request');

        Route::get('/api/journal/urlisexsist/{url}', [
            'uses'              => 'JournalApiController@cekUrlIsExsist',
            'default_roles'     => 'all',
        ])->name('journal.api.urlisexsist');

        //post
        Route::get('/post', [
            'menu_name'         => 'main_page_post',
            'page_name'         => 'Artikel',
            'page_description'  => 'Pengelolaan untuk semua Artikel',
            'uses'              => 'PostController@show',
            'default_roles'     => 'all',
        ])->name('post.main');

        Route::get('/post/create', [
            'menu_name'         => 'create_post',
            'page_name'         => 'Tambah Artikel Baru',
            'page_description'  => 'Membuat Artikel baru untuk website',
            'uses'              => 'PostController@create',
            'default_roles'     => 'all',
        ])->name('post.create');

        Route::get('/post/edit/{id}', [
            'menu_name'         => 'edit_post',
            'page_name'         => 'Edit Artikel',
            'page_description'  => 'Perbarui Artikel yang telah terbuat',
            'uses'              => 'PostController@edit',
            'default_roles'     => 'all',
        ])->name('post.edit');

        Route::post('/post/store', [
            'menu_name'         => 'store_post',
            'uses'              => 'PostController@store',
            'default_roles'     => 'all',
        ])->name('post.store');

        Route::post('/post/update', [
            'menu_name'         => 'update_post',
            'uses'              => 'PostController@update',
            'default_roles'     => 'all',
        ])->name('post.update');

        Route::get('/post/destroy/{id}', [
            'menu_name'         => 'delete_post',
            'uses'              => 'PostController@destroy',
            'default_roles'     => 'all',
        ])->name('post.destroy');

        Route::get('/api/post/urlisexsist/{url}', [
            'uses'              => 'PostApiController@cekUrlIsExsist',
            'default_roles'     => 'all',
        ])->name('post.api.urlisexsist');



        Route::get('/setting', [
            'menu_name'         => 'pengaturan',
            'page_name'         => 'Pengaturan',
            'page_description'  => null,
            'uses'              => 'SettingController@index',
            'default_roles'     => 'all',
        ])->name('setting');

        Route::post('/setting/header', [
            'menu_name'         => 'pengaturan',
            'page_name'         => 'Pengaturan',
            'page_description'  => null,
            'uses'              => 'SettingController@updateHeader',
            'default_roles'     => 'all',
        ])->name('setting.header');

        Route::post('/setting/sosmed', [
            'menu_name'         => 'pengaturan',
            'page_name'         => 'Pengaturan',
            'page_description'  => null,
            'uses'              => 'SettingController@updateSosmed',
            'default_roles'     => 'all',
        ])->name('setting.sosmed');
        
        Route::post('/setting/clasification', [
            'menu_name'         => 'pengaturan',
            'page_name'         => 'Pengaturan',
            'page_description'  => null,
            'uses'              => 'SettingController@updateClasification',
            'default_roles'     => 'all',
        ])->name('setting.clasification');

        Route::post('/setting/updateusernamepassword', [
            'menu_name'         => 'pengaturan',
            'page_name'         => 'Pengaturan',
            'page_description'  => null,
            'uses'              => 'SettingController@updateUsernamePassword',
            'default_roles'     => 'all',
        ])->name('setting.updateusernamepassword');
        //DUMP!!!

        Route::get('/appearance/menu', [
            'menu_name'         => 'main_menu',
            'page_name'         => 'Menu',
            'page_description'  => 'Pengelolaan untuk semua Menu',
            'uses'              => 'MenuController@index',
            'default_roles'     => 'all',
        ])->name('appearance.menu.main');

        Route::post('/appearance/menu/store', [
            'menu_name'         => 'store_menu',
            'uses'              => 'MenuController@store',
            'default_roles'     => 'all',
        ])->name('appearance.menu.store');

        Route::get('/datatable/menu/all', [
            'uses'              => 'MenuDatatableController@all',
            'default_roles'     => 'all',
        ])->name('menu.datatable.all');

        Route::get('/appearance/header', [
            'menu_name'         => 'main_header',
            'page_name'         => 'Header',
            'page_description'  => 'Pengelolaan untuk semua header',
            'uses'              => 'HeaderController@index',
            'default_roles'     => 'all',
        ])->name('appearance.header.main');

        Route::post('/appearance/header/store', [
            'menu_name'         => 'store_header',
            'uses'              => 'HeaderController@store',
            'default_roles'     => 'all',
        ])->name('appearance.header.store');

        Route::get('/appearance/widget', [
            'menu_name'         => 'main_widget',
            'page_name'         => 'Widget',
            'page_description'  => 'Pengelolaan untuk semua widget',
            'uses'              => 'WidgetController@index',
            'default_roles'     => 'all',
        ])->name('appearance.widget.main');

        Route::post('/appearance/widget/store', [
            'menu_name'         => 'store_widget',
            'uses'              => 'WidgetController@store',
            'default_roles'     => 'all',
        ])->name('appearance.widget.store');

        Route::get('/appearance/footer', [
            'menu_name'         => 'main_footer',
            'page_name'         => 'Footer',
            'page_description'  => 'Pengelolaan untuk semua footer',
            'uses'              => 'FooterController@index',
            'default_roles'     => 'all',
        ])->name('appearance.footer.main');

        Route::post('/appearance/footer/store', [
            'menu_name'         => 'store_footer',
            'uses'              => 'FooterController@store',
            'default_roles'     => 'all',
        ])->name('appearance.footer.store');

        Route::get('/appearance/runningtext', [
            'menu_name'         => 'main_runningtext',
            'page_name'         => 'Running Text',
            'page_description'  => 'Pengelolaan untuk text yang berjalan',
            'uses'              => 'RunningTextController@index',
            'default_roles'     => 'all',
        ])->name('appearance.runningtext.main');
    });

    //JSON
    Route::get('/json/imagebrowser', [
        'menu_name'         => 'json_getmedia',
        'uses'              => 'FileController@getFileImageBrowser',
        'default_roles'     => 'all',
    ])->name('json.imagebrowser');

    Route::get('/{name}', [
        'uses'          => 'PageController@page',
    ]);

    Route::get('/datatable/page/all', [
        'uses'              => 'PageDatatableController@all',
        'default_roles'     => 'all',
    ])->name('page.datatable.all');

    Route::get('/datatable/journal/all', [
        'uses'              => 'JournalDatatableController@all',
        'default_roles'     => 'all',
    ])->name('page.journal.all');

    Route::get('/datatable/post/all', [
        'uses'              => 'PostDatatableController@all',
        'default_roles'     => 'all',
    ])->name('page.post.all');

    Route::get('images/imageslider/{filename}', function ($filename)
    {
        return Image::make(storage_path('app/uploaded/images/imageslider/' . $filename))->response();
    });

    Route::get('images/coverjournal/{filename}', function ($filename)
    {
        
        if(file_exists(storage_path('app/uploaded/images/coverjournal/' . $filename))) {
            
            return Image::make(storage_path('app/uploaded/images/coverjournal/' . $filename))->response();
        }
        else {
            return Image::make(public_path('images/imagenotfound'.Request::input('orientation').'.png'))->response();
        }
    });

    Route::post('upload', ['as' => 'upload-post', 'uses' =>'ImageController@postUpload']);
    Route::post('upload/delete', ['as' => 'upload-remove', 'uses' =>'ImageController@deleteUpload']);

    Route::get('oai/tes', [
        'uses' => 'JournalApiController@oaitest',
        'default_roles'     => 'all',
    ]);
});
