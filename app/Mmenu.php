<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mmenu extends Model
{
    protected $table = 'mmenus';

    protected $fillable = [
        'name', 'display', 'description',
    ];

    public function roles() {
        return $this->belongsToMany('App\Role', 'menu_role', 'menu_id', 'role_id');
    }
}