<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'name', 'description',
    ];

    public function users() {
        return $this->belongsToMany('App\User' , 'user_role', 'role_id', 'user_id');
    }

    // Menu
    public function menus() {
        return $this->belongsToMany('App\Mmenu', 'menu_role', 'role_id', 'menu_id');
    }

    public function hasAnyMenu($menus) {
        if(is_array($menus)) {
            foreach($menus as $menu)
                if($this->hasMenu($menu))
                    return true;  
        } else
            if($this->hasMenu($menus))
                return true;
    }

    public function hasMenu($menu)
    {
        if($this->menus()->where('name', $menu)->first())
            return true;
        return false;
    }

    public function assignMenu($menu_id)
    {
        return $this->menus()->attach($menu_id);
    }

    public function removeMenu($menu_id)
    {
        return $this->menus()->detach($menu_id);
    }
}
