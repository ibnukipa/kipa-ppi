<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTag extends Model
{
    protected $table = 'page_tags';

    protected $fillable = [
        'name', 'description',
    ];
}
