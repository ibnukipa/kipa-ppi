<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mfooter extends Model
{
    protected $table = 'mfooters';

    protected $fillable = [
        'name', 'image', 'description',
    ];
}
