<?php

namespace App\Services;

use Illuminate\Http\Request;
use Storage;

class FileService
{
    public $storageRootPathFiles;
    public $storageRootPathImages;

    public $storagePublicRoot;

    public function __construct()
    {
        $this->storageRootPathFiles = Storage::disk('files')->getDriver()->getAdapter()->getPathPrefix();
        $this->storageRootPathImages = Storage::disk('images')->getDriver()->getAdapter()->getPathPrefix();

        $this->storagePublicRoot = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
    }

    public function isFileExist($disk, $nameFile) {
        return Storage::disk($disk)->exists($nameFile);
    }

    public function getUrlFile($disk, $nameFile) {
        if($this->isFileExist($disk, $nameFile))
            return Storage::disk($disk)->url($nameFile);
        else
            return "storage/not_found.png";
    }

    public function getFile($disk, $nameFile) {
        if($this->isFileExist($disk, $nameFile))
            return Storage::disk($disk)->get($nameFile);
        else
            return "storage/not_found.png";
    }

    public function storeFile($disk, $nameFile) {
        
    }
}
