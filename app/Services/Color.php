<?php

namespace App\Services;

use Illuminate\Http\Request;
use Auth;

class Color
{    
    public static function getColor() {
        if(!Auth::guest()) {
            if(Auth::user()->hasRole('Front_Office')) {
                // rgb(13,71,161) 900
                return 'blue';    
            } elseif(Auth::user()->hasRole('Analyst')) {
                // rgb(27,94,32) 900
                return 'green';
            } elseif(Auth::user()->hasRole('Supervisor')) {
                // rgb(74,20,140) 900
                return 'purple';
            } elseif(Auth::user()->hasRole('Technical_manager')) {
                // rgb(230,81,0) 900
                return 'orange';
            } elseif(Auth::user()->hasRole('Head_of_unit')) {
                // rgb(26,35,126) 900
                return 'indigo';
            } elseif(Auth::user()->hasRole('Section_head')) {
                // rgb(49,27,146) 900
                return 'deeppurple';
            } elseif(Auth::user()->hasRole('Viewer')) {
                // rgb(183,28,28) 900
                return 'red';
            } elseif(Auth::user()->hasRole('Administrator')) {
                // rgb(0,77,64) 900
                return 'teal';
            } else {
                return 'teal';
            }
        } else {
            return 'teal';
        }
    }

    public static function getColorUI() {
        if(!Auth::guest()) {
            if(Auth::user()->hasRole('Front_Office')) {
                return 'blue';    
            } elseif(Auth::user()->hasRole('Analyst')) {
                return 'green';
            } elseif(Auth::user()->hasRole('Supervisor')) {
                return 'purple';
            } elseif(Auth::user()->hasRole('Technical_manager')) {
                return 'orange';
            } elseif(Auth::user()->hasRole('Head_of_unit')) {
                return 'violet';
            } elseif(Auth::user()->hasRole('Section_head')) {
                return 'violet';
            } elseif(Auth::user()->hasRole('Viewer')) {
                return 'red';
            } elseif(Auth::user()->hasRole('Administrator')) {
                return 'teal';
            } else {
                return 'teal';
            }
        } else {
            return 'teal';
        }
    }
}
