<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WidgetType extends Model
{
    protected $table = 'widget_types';

    protected $fillable = [
        'name', 'image', 'description',
    ];
}
