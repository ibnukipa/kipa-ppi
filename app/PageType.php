<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageType extends Model
{
    protected $table = 'page_types';

    protected $fillable = [
        'name', 'description',
    ];
}
