<?php

namespace App\Http\Middleware;

use Closure;
use App\Menu;
use Auth;
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest()) {
            return redirect('/login');
        }

        if ($request->user() === null) {
            return response("Insufficient permissions", 401);
        }

        $current_route      = $request->route()->getAction();
        $menu_name          = isset($current_route['menu_name']) ? $current_route['menu_name'] : null;
        $default_roles      = isset($current_route['default_roles']) ? $current_route['default_roles'] : null;
        
        // FIRST
        //Check from database
        $current_menu       = Menu::where('name', $menu_name)->first();
        
        if($current_menu) {
            foreach($request->user()->roles as $role) {
                if($role->hasAnyMenu($menu_name)){
                    return $next($request);
                }
            }
        }
        
        // SECOND
        // If menu not registered in database
        if($request->user()->hasAnyRole($default_roles) || (!is_array($default_roles) && $default_roles == 'all')) {
            return $next($request);
        }
        
        return response("Insufficient permissions", 401);
    }
}
