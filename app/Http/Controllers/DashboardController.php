<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class DashboardController extends Controller
{
    public function __construct() {}

    public function index() {
        $data = ['page' => [
            'name' => "Dashboard Controller",
            'description' => 'Description'
        ]];

        return view('admin.dashboard')->with($data);
    }

    public function editPage($idpage) {
        $page = Page::find($idpage);

        if($page) {
            if($page->page_type_id == 6) {
                //Artikel
                return redirect('/admin/post/edit/'.$idpage);
            } elseif($page->page_type_id == 5) {
                //Journal
                return redirect('/admin/journal/edit/'.$idpage);
            } else {
                //Laman
                return redirect('/admin/page/edit/'.$idpage);
            }
        } else {
            return back()->withInput()->with([
                    'alert' => [
                        'type' => 'warning',
                        'pesan' => "Laman tidak ditemukan"
                    ]
            ]);
        }
    }
}
