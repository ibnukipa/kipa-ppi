<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Datatables;

class PostDatatableController extends Controller
{
    public function __construct() {}
    
    public function all(Request $req) {   
        if($req->input('with')) {
            $withArray = explode(',', $req->input('with'));
            $dataForDatatables = Page::where('page_type_id', '=', 6)->with($withArray)->get();
        } else {
            $dataForDatatables = Page::where('page_type_id', '=', 6)->get();
        }

        return Datatables::of($dataForDatatables)
            ->editColumn('created_at', function($page){
                return $page->created_at->format('Y-m-d');
            })
            ->editColumn('url', function($page) {
                return '/'.$page->url.'/';
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') like ?", ["%$keyword%"]);
            })
            ->addColumn('action', function ($page) {
                return '<div class="ui icon tiny buttons">
                            <a href="/'.$page->url.'/" target="_blank" class="ui green basic icon button" 
                                    data-tooltip="Pertinjau" 
                                    data-position="top center"><i class="unhide icon"></i>
                            </a>
                            <a href="'.url('/admin/post/edit/'.$page->id).'" class="ui blue basic icon button" 
                                        data-tooltip="Sunting" 
                                        data-position="top center"><i class="edit icon"></i>
                            </a>
                            <button onclick="showConfirmModal(this)" 
                                    data-modalid="m_confirm_delete"
                                    data-action="'.url('/admin/post/destroy/'.$page->id).'"
                                    data-method="GET"
                                    data-m-header="Hapus Laman ID #'.$page->id.'"
                                    data-m-sub-header="Apakah Anda yakin akan menghapus Laman Artikel <strong>'.$page->title.'</strong> dengan ID <strong>#'.$page->id.'</strong>?"
                                    class="ui red basic icon button" 
                                    data-tooltip="Hapus" 
                                    data-position="top center"><i class="delete icon"></i>
                            </button>
                        </div>';
            })
            ->make(true);
    }
}
