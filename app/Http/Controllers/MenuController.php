<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function __construct() {}

    public function index() {
        return view('admin.menu.create');
    }

    public function store(Request $req) {
        dd($req->input());
    }
}
