<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Page;
use Illuminate\Http\UploadedFile; 
use File;
use Storage;
class JournalApiController extends Controller
{
    public function __construct() {}

    public function cekUrlIsExsist($url) {
        $curPage = Page::where('url', '=', $url)->first();
        if($curPage)
            return 1;
        else
            return 0;
    }

    public function oaitest() {
        // http://iptek.its.ac.id/index.php/index/oai
        // $url = 'http://iptek.its.ac.id/index.php/index/oai?verb=ListRecords&metadataPrefix=oai_dc&set=joae';
        $url = 'http://iptek.its.ac.id/index.php/index/oai?verb=ListSets';
        
        $xmlObj = simplexml_load_file($url);
        // dd($xmlObj);
        // $xmlNode = $xmlObj->ListRecords;
        $xmlNode = $xmlObj->ListSets;
        // dd($xmlNode);
        // foreach ($xmlNode->record as $rNode) {
        foreach ($xmlNode->set as $rNode) {
            // dd($rNode);
            // dump($rNode->metadata);
            // dd($rNode->metadata);
            // dump($rNode->metadata->children('oai_dc', 1)->dc->children('dc', 1));

            // dump($rNode->setDescription);
            dump($rNode->setDescription->children('oai_dc', 1)->dc->children('dc', 2));
            // dump($rNode->metadata->children('oai_dc', 1)->dc->children('dc', 1));
        }
    }

    function get_data($url) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    function createFileObject($url){
        try {
            $path_parts = pathinfo($url);

            $imgInfo = getimagesize($url);
            

            $filename = 'temp-image.jpg';
            $tempImage = tempnam(sys_get_temp_dir(), $filename);
            $thumbUrl = $path_parts['dirname'].'/journalThumbnail_en_US.png';
            $stringThumb = "journalThumbnail_en_US.png";
            if(get_headers($thumbUrl, 1)[0] == 'HTTP/1.1 404 Not Found') {
                $thumbUrl = $path_parts['dirname'].'/journalThumbnail_en_US.gif';
                $stringThumb = "journalThumbnail_en_US.gif";
            } 
            if(get_headers($thumbUrl, 1)[0] == 'HTTP/1.1 404 Not Found') {
                $thumbUrl = $path_parts['dirname'].'/journalThumbnail_en_US.jpg';
                $stringThumb = "journalThumbnail_en_US.jpg";
            }
            
            if(get_headers($thumbUrl, 1)[0] != 'HTTP/1.1 404 Not Found') {
                copy($thumbUrl, $tempImage);
                $fileThumb = new UploadedFile(
                    $tempImage,
                    $path_parts['basename'],
                    $imgInfo['mime'],
                    null,
                    null,
                    TRUE
                );   
            } else {
                $fileThumb = null;
            }

            //COVER LANDSCAPE
            $filename = 'temp-image.jpg';
            $tempImage = tempnam(sys_get_temp_dir(), $filename);
            copy($url, $tempImage);
            $file = new UploadedFile(
                $tempImage,
                $path_parts['basename'],
                $imgInfo['mime'],
                null,
                null,
                TRUE
            );
            
            return ['lan' => $file, 'pot' => $fileThumb];
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function oaiupdatejournal() {
        // oai_dc
        //CONTENT
        $error;
        // $file=$this->createFileObject('http://iptek.its.ac.id/public/journals/11/homeHeaderTitleImage_en_US.jpg');
        // dd($file);

        \DB::beginTransaction();
        try {
            $client = new \Phpoaipmh\Client('http://iptek.its.ac.id/index.php/index/oai');
            $myEndpoint = new \Phpoaipmh\Endpoint($client);
            $recs = $myEndpoint->listSets('oai_dc');
            
            $specNotIn = array('ART', 'COV', 'edt', 'vol+1.1');
    
            $countUpdated = 0;
            $countCreated = 0;
            foreach($recs as $rec) {
                $specJournal    = explode(':', $rec->setSpec->__toString());
                $curSet         = [ 'url' => 'http://iptek.its.ac.id/index.php/'.$rec->setSpec->__toString(), 'title' => $rec->setName->__toString()];
                $urlImageCoverLandscape = 'noimage';
                $error = $curSet;

                if((count($specJournal) > 1 && !in_array($specJournal[1], $specNotIn)) || count($specJournal) == 1) {
                    $existingJournal = Page::where('journal_source_page', '=',$curSet['url'])->first();
                    
                    //CONTENT
                    $html = file_get_contents($curSet['url']);
                    $dom = new \DOMDocument();
                    libxml_use_internal_errors(true);
                    $dom->loadHTML($html);
            
                    foreach ($dom->getElementsByTagName('img') as $item) {
                        for ($i = 0; $i < $item->attributes->length; ++$i) {
                          $node = $item->attributes->item($i);
                          if($node->nodeName == 'src' || $node->nodeName == 'href') {
                            if(substr($node->nodeValue, 0, 7) == '/public') {
                                if($node->nodeName == 'src' )
                                    $item->setAttribute('src', 'http://iptek.its.ac.id'.$node->nodeValue);
                                if($node->nodeName == 'href')
                                    $item->setAttribute('href', 'http://iptek.its.ac.id'.$node->nodeValue);
                                $dom->saveHTML();
                            }   
                          }
                          
                          if($node->nodeName == 'alt' && $node->nodeValue == 'Page Header') {
                              $urlImageCoverLandscape = $item->getAttribute('src');
                          }
                        }
                    }
                    $contents_div = $dom->getElementById("content");
                    if ($contents_div) {
                       $contentJournal = $dom->saveHTML($contents_div);
                    }

                    if($existingJournal) {
                        // $this->updateJournal($existingJournal, $curSet, $contentJournal);
                        $existingJournal->update([
                            'title'                 => $curSet['title'],
                            'title_en'              => $curSet['title'],
                            'menu_name'             => $curSet['title'],
                            'menu_name_en'          => $curSet['title'],
                            'content'            => htmlspecialchars($contentJournal),
                            'content_en'            => htmlspecialchars($contentJournal),
                            'journal_source_page'   => $curSet['url'],
                        ]);

                        if($existingJournal->image_cover_landscape) {
                            Storage::disk('images')->delete($existingJournal->image_cover_landscape);
                        }
                        if($existingJournal->image_cover_potrait) {
                            Storage::disk('images')->delete($existingJournal->image_cover_potrait);
                        }
                        //IMAGE
                        // $url = 'http://iptek.its.ac.id/public/journals/5/cover_issue_321_en_US.jpg';
                        if($urlImageCoverLandscape != 'noimage'){
                            if($file=$this->createFileObject($urlImageCoverLandscape)) {
                                $image_cover_landscape = $file['lan']->store(
                                    'coverjournal', 'images'
                                );
                                $existingJournal->image_cover_landscape = $image_cover_landscape;
                                $existingJournal->image_cover_landscape_bool = true;
                                $existingJournal->save();

                                if($file['pot'] != null) {
                                    $image_cover_potrait = $file['pot']->store(
                                        'coverjournal', 'images'
                                    );
                                    $existingJournal->image_cover_potrait = $image_cover_potrait;
                                    $existingJournal->save();
                                }
                            }
                        }
                        $countUpdated++;
                    }
                    else {
                        // $this->createJournal($curSet, $contentJournal);
                        \DB::beginTransaction();
                        try {
                
                            $newPage = Page::create([
                                'title'                 => $curSet['title'],
                                'title_en'              => $curSet['title'],
                                'url'                   => $this->toAscii($curSet['title']),
                                'status'                => 'draft',
                                'visible'               => false,
                                'content'               => htmlspecialchars($contentJournal),
                                'content_en'            => htmlspecialchars($contentJournal),
                
                                'menu_visible'          => true,
                                'menu_name'             => $curSet['title'],
                                'menu_name_en'          => $curSet['title'],
                
                                'journal_source_page'   => $curSet['url'],
                
                                'page_type_id'          => 5,
                                'creator_id'            => \Auth::user()->id,
                            ]);

                            if($urlImageCoverLandscape != 'noimage'){
                                if($file=$this->createFileObject($urlImageCoverLandscape)) {
                                    $image_cover_landscape = $file['lan']->store(
                                        'coverjournal', 'images'
                                    );
                                    $newPage->image_cover_landscape = $image_cover_landscape;
                                    $newPage->image_cover_landscape_bool = true;
                                    $newPage->save();

                                    if($file['pot'] != null) {
                                        $image_cover_potrait = $file['pot']->store(
                                            'coverjournal', 'images'
                                        );
                                        $newPage->image_cover_potrait = $image_cover_potrait;
                                        $newPage->save();
                                    }
                                }
                            }
                
                            \DB::commit();
                        } catch (\Exception $e) {
                            $pesan   = $e->getMessage();
                            \DB::rollback();
                            return back()->with([
                                    'alert' => [
                                        'type' => 'warning',
                                        'pesan' => $pesan
                                    ]
                            ]);
                        }
                        $countCreated++;
                    }
                }
            }

            \DB::commit();
            return back()->with([
                'alert' => [
                    'type' => 'warning',
                    'color' => 'green',
                    'pesan' => 'Berhasil <strong>memperbarui '.$countUpdated.'</strong> dan <strong>menambah '.$countCreated.'</strong> data jurnal.'
                    ]
            ]);

        } catch (\Exception $e) {
            
            $pesan   = $e->getMessage();
            // $html = file_get_contents($error['url']);
            // $dom = new \DOMDocument();
            
            // $dom->loadHTML($html);
            // dd($dom);
            \DB::rollback();
            return back()->withInput()->with([
                    'alert' => [
                        'type' => 'warning',
                        'color' => 'red',
                        'pesan' => $pesan
                    ]
            ]);
        }
    }

    // function updateJournal($page, $dataFromOAI, $contentJournal) {
    //     $page->update([
    //         'title'                 => $dataFromOAI['title'],
    //         'title_en'              => $dataFromOAI['title'],
    //         'menu_name'             => $dataFromOAI['title'],
    //         'menu_name_en'          => $dataFromOAI['title'],
    //         'content'            => htmlspecialchars($contentJournal),
    //         'content_en'            => htmlspecialchars($contentJournal),
    //         'journal_source_page'   => $dataFromOAI['url'],
    //     ]);
    // }

    // function createJournal($dataFromOAI, $contentJournal) {

    //     \DB::beginTransaction();
    //     try {

    //         $newPage = Page::create([
    //             'title'                 => $dataFromOAI['title'],
    //             'title_en'              => $dataFromOAI['title'],
    //             'url'                   => $this->toAscii($dataFromOAI['title']),
    //             'status'                => 'draft',
    //             'visible'               => false,
    //             'content'               => htmlspecialchars($contentJournal),
    //             'content_en'            => htmlspecialchars($contentJournal),

    //             'menu_visible'          => true,
    //             'menu_name'             => $dataFromOAI['title'],
    //             'menu_name_en'          => $dataFromOAI['title'],

    //             'journal_source_page'   => $dataFromOAI['url'],

    //             'page_type_id'          => 5,
    //             'creator_id'            => \Auth::user()->id,
    //         ]);

    //         \DB::commit();
    //     } catch (\Exception $e) {
    //         $pesan   = $e->getMessage();
    //         \DB::rollback();
    //         return back()->with([
    //                 'alert' => [
    //                     'type' => 'warning',
    //                     'pesan' => $pesan
    //                 ]
    //         ]);
    //     }
    // }

    function toAscii($str, $replace=array(), $delimiter='-') {
        if( !empty($replace) ) {
         $str = str_replace((array)$replace, ' ', $str);
        }
       
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
       
        return $clean;
    }
}
