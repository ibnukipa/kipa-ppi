<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\PageType;
use App\PageTag;
use App\JournalClasification;
use App\Imageslider;

use Purifier;
class JournalController extends Controller
{
    public function __construct() {}

    public function index() {
        // $data = ['page' => [
        //     'name' => "Home",
        //     'description' => 'Description'
        // ]];
        $curPage = Page::first();
        $data = ['page' => $curPage];
        return view('pages.page1')->with($data);
    }

    public function page($name) {
        $curPage = Page::where('url', '=', $name)->first();

        if($curPage) {
            $data = ['page' => $curPage];
            
            return view('pages.page1')->with($data);
        } else {
            abort(404);
        }
    }

    public function show() {
        $data = ['page' => [
            // 'name' => "Halaman",
            // 'description' => 'Description'
        ]];

        return view('admin.journal.show')->with($data);
    }

    public function edit($id) {
        $page = Page::find($id);
        
        if($page) {
            $data = [
                'page' => $page,
                'master' => [
                    'page_type'         => PageType::where('name', '!=', 'journal')
                                            ->Where('name', '!=', 'post')
                                            ->get(),
                    'page_parent'       => Page::where('page_type_id', '!=', 5)
                                            ->where('page_type_id', '!=', 6)
                                            ->get(),
                    'page_clasification' => JournalClasification::get(),
                    'page_tag'         => PageTag::get(),
                    'page_all'          => Page::get()
                ]
            ];

            return view('admin.journal.edit')->with($data);

        } else return back()->withInput()->with([
                        'alert' => [
                            'type' => 'error',
                            'pesan' => 'Data Journal dengan ID #'.$id.' tidak ditemukan.'
                        ]
                ]);  
    }

    public function create() {
        $data = [
            'page' => [
                // 'name' => "Tambah Laman",
                // 'description' => 'Description'
            ],
            'master' => [
                'page_type'         => PageType::where('name', '=', 'journal')
                                        ->get(),
                'page_parent'       => Page::where('page_type_id', '=', 5)
                                        ->get(),
                'page_clasification' => JournalClasification::get(),
                'page_tag'         => PageTag::get(),
                'page_all'          => Page::get()
            ]
        ];

        return view('admin.journal.create')->with($data);
    }

    public function destroy($id) {
        
        $page = Page::find($id);
        if($page) {
            \DB::beginTransaction();
            try {
                foreach ($page->sidebars() as $key => $value) {
                    $value->delete();
                }
                foreach ($page->imagesliders() as $key => $value) {
                    $value->delete();
                }
                
                $page->delete();
                \DB::commit();
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'success',
                            'pesan' => 'Berhasil menghapus Laman dengan ID #'.$id
                        ]
                ]);  
            } catch(\Exception $e) {
                $success = false;
                $pesan   = $e->getMessage();
                \DB::rollback();
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => $pesan
                        ]
                ]);  
            }
        } else return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => 'Page dengan ID #'.$id.' tidak tersedia.'
                        ]
                ]);  
    }

    public function update(Request $req) {
        // dd($req->input());
        
        if($req->input('page')['menu_name'])
            $name_menu = $req->input('page')['menu_name'];
        else
            $name_menu = $req->input('page')['title'];
        
        if($req->input('page')['menu_name_en'])
            $name_menu_en = $req->input('page')['menu_name_en'];
        else
            $name_menu_en = $req->input('page')['title_en'];
        
        \DB::beginTransaction();

        try {
            $curPage = Page::find($req->input('page')['id']);
            if($curPage->status == 'draft')
                $newIsVisible = true;
            else
                $newIsVisible = !isset($req->input('page')['visible']);

            if(!$curPage)
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => 'Journal dengan ID'.$req->input('page')['id'].' tidak ada.'
                        ]
                ]);
            $anotherPage = Page::where('id', '!=', $curPage->id)->where('url', '=', $req->input('page')['url'])->first();

            //URL EXSIST
            if($anotherPage)
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => 'URL <strong>/'.$curPage->url.'/</strong> SUDAH terpakai. Mohon gunakan URL yang lain'
                        ]
                ]);

            
            //UPDATE PAGE
            $journal_source_page = explode("://", $req->input('page')['journal_source_page']);
            if(count($journal_source_page) == 1)
                $journal_source_page = 'http://'.$journal_source_page;
            else
                $journal_source_page = $req->input('page')['journal_source_page'];

            $curPage->update([
                'title'                 => $req->input('page')['title'],
                'title_en'              => $req->input('page')['title_en'],
                'subtitle'              => $req->input('page')['subtitle'],
                'subtitle_en'           => $req->input('page')['subtitle_en'],
                'subject'               => '',
                'subject_en'            => '',
                'content'               => htmlspecialchars($req->input('page')['content']),
                'content_en'            => htmlspecialchars($req->input('page')['content_en']),
                'meta'                  => '',
                'url'                   => $req->input('page')['url'],
                'status'                => 'publish',
                'visible'               => $newIsVisible,
                'accreditation'         => isset($req->input('page')['accreditation']),
                'textrunning_bool'      => isset($req->input('page')['textrunning_visible']),
                'textrunning_content'   => '',
                'textrunning_content_en'   => '',
                'textrunning_speed'     => 0,
                'textrunning_color'     => '',

                'menu_visible'          => true,
                'menu_name'             => $name_menu,
                'menu_name_en'          => $name_menu_en,

                'image_slider_bool'     => isset($req->input('page')['imageslider_visible']),
                'siderbar_bool'         => isset($req->input('page')['imageslider_visible']),
                'journal_source_page'   => $journal_source_page,
                'journal_clasification_id' => (int)$req->input('page')['journal_clasification_id'],
                
                'image_cover_landscape_bool' => isset($req->input('page')['image_cover_landscape_bool']),
                'siderbar_recent_journal_bool' => isset($req->input('page')['siderbar_recent_journal_bool']),
                'siderbar_recent_article_bool' => isset($req->input('page')['siderbar_recent_article_bool']),

                'page_type_id'          => 5,
                'creator_id'            => \Auth::user()->id,
            ]);

            //UPDATE TAGS
            $curPage->tags()->detach();
            if(isset($req->input('page')['tags'])) {
                foreach ($req->input('page')['tags'] as $key => $tag) {
                    $curTag = PageTag::where('name', '=', $tag)->first();
                    if($curTag) {
                        // TAG LAMA
                        $curPage->tags()->attach($curTag->id);
                    } else {
                        // TAG BARU
                        $newTag = PageTag::create(['name' => $tag]);
                        $curPage->tags()->attach($newTag->id);
                    }
                }
            }

            // CREATING SIDEBAR
            $curPage->sidebars()->delete();
            if(isset($req->input('page')['sidebar']['label'])) {
                foreach ($req->input('page')['sidebar']['label'] as $key => $label) {
                    if($req->input('page')['sidebar']['label'][$key] && $req->input('page')['sidebar']['url'][$key])
                        $curPage->sidebars()->create([
                            'url'       => $req->input('page')['sidebar']['url'][$key],
                            'label'     => $label
                        ]);
                }
            }
            
            if($file=$req->file('image_cover_potrait')){
                $path = $file->store(
                    'coverjournal', 'images'
                );
                $curPage->image_cover_potrait = $path;
                $curPage->save();
            }

            if($file=$req->file('image_cover_landscape')){
                $path = $file->store(
                    'coverjournal', 'images'
                );
                $curPage->image_cover_landscape = $path;
                $curPage->save();
            }

            //CREATING TEXT RUNNING
            if(isset($req->input('page')['textrunning_visible'])) {
                $curPage->textrunning_content = $req->input('page')['runningtext']['content'];
                $curPage->textrunning_content_en = $req->input('page')['runningtext']['content_en'];
                $curPage->textrunning_speed = $req->input('page')['runningtext']['speed'];
                $curPage->textrunning_color = $req->input('page')['runningtext']['color'];
                $curPage->save();
            }

            \DB::commit();
            return redirect('/'.$req->input('page')['url']);
        } catch (\Exception $e) {
            
            $pesan   = $e->getMessage();
            \DB::rollback();

            return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => $pesan
                        ]
                ]);
        }
    }

    public function store(Request $req) {
        // dd($req->input());
        if($req->input('page')['menu_name'])
            $name_menu = $req->input('page')['menu_name'];
        else
            $name_menu = $req->input('page')['title'];

        if($req->input('page')['menu_name_en'])
            $name_menu_en = $req->input('page')['menu_name_en'];
        else
            $name_menu_en = $req->input('page')['title_en'];

        \DB::beginTransaction();

        try {
            $curPage = Page::where('url', '=', $req->input('page')['url'])->first();

            //URL EXSIST
            if($curPage)
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => 'URL Laman <strong>/'.$curPage->url.'/</strong> SUDAH terpakai. Mohon gunakan URL Laman yang lain'
                        ]
                ]);

            //CREATING NEW PAGE
            $journal_source_page = explode("://", $req->input('page')['journal_source_page']);
            if(count($journal_source_page) == 1)
                $journal_source_page = 'http://'.$journal_source_page[0];
            else
                $journal_source_page = $req->input('page')['journal_source_page'];

            $newPage = Page::create([
                'title'                 => $req->input('page')['title'],
                'title_en'              => $req->input('page')['title_en'],
                'subtitle'              => $req->input('page')['subtitle'],
                'subtitle_en'           => $req->input('page')['subtitle_en'],
                'subject'               => '',
                'subject_en'            => '',
                'content'               => htmlspecialchars($req->input('page')['content']),
                'content_en'            => htmlspecialchars($req->input('page')['content_en']),
                'meta'                  => '',
                'url'                   => $req->input('page')['url'],
                'status'                => 'publish',
                'visible'               => !isset($req->input('page')['visible']),
                'accreditation'         => isset($req->input('page')['accreditation']),

                'textrunning_bool'      => isset($req->input('page')['textrunning_visible']),
                'textrunning_content'   => '',
                'textrunning_content_en'=> '',
                'textrunning_speed'     => 0,
                'textrunning_color'     => '',

                'menu_visible'          => true,
                'menu_name'             => $name_menu,
                'menu_name_en'          => $name_menu_en,

                'image_slider_bool'     => isset($req->input('page')['imageslider_visible']),
                'siderbar_bool'         => isset($req->input('page')['sidebar_visible']),
                'journal_source_page'   => $journal_source_page,
                'image_cover_landscape_bool' => isset($req->input('page')['image_cover_landscape_bool']),
                'siderbar_recent_journal_bool' => isset($req->input('page')['siderbar_recent_journal_bool']),
                'siderbar_recent_article_bool' => isset($req->input('page')['siderbar_recent_article_bool']),

                'page_type_id'          => 5,
                'creator_id'            => \Auth::user()->id,
            ]);

            //CREATING TAGS
            if(isset($req->input('page')['tags'])) {
                foreach ($req->input('page')['tags'] as $key => $tag) {
                    $curTag = PageTag::where('name', '=', $tag)->first();
                    if($curTag) {
                        // TAG LAMA
                        $newPage->tags()->attach($curTag->id);
                    } else {
                        // TAG BARU
                        $newTag = PageTag::create(['name' => $tag]);
                        $newPage->tags()->attach($newTag->id);
                    }
                }
            }

            // CREATING SIDEBAR
            if(isset($req->input('page')['sidebar']['label'])) {
                foreach ($req->input('page')['sidebar']['label'] as $key => $label) {
                    
                    if($req->input('page')['sidebar']['label'][$key] && $req->input('page')['sidebar']['url'][$key])
                        $newPage->sidebars()->create([
                            'url'       => $req->input('page')['sidebar']['url'][$key],
                            'label'     => $label
                        ]);
                }
            }

            if($file=$req->file('image_cover_potrait')){
                $path = $file->store(
                    'coverjournal', 'images'
                );
                $newPage->image_cover_potrait = $path;
                $newPage->save();
            }

            if($file=$req->file('image_cover_landscape')){
                $path = $file->store(
                    'coverjournal', 'images'
                );
                $newPage->image_cover_landscape = $path;
                $newPage->save();
            }

            //CREATING TEXT RUNNING
            if(isset($req->input('page')['textrunning_visible'])) {
                $newPage->textrunning_content = $req->input('page')['runningtext']['content'];
                $newPage->textrunning_content_en = $req->input('page')['runningtext']['content_en'];
                $newPage->textrunning_speed = $req->input('page')['runningtext']['speed'];
                $newPage->textrunning_color = $req->input('page')['runningtext']['color'];
                $newPage->save();
            }

            \DB::commit();
            return redirect('/'.$req->input('page')['url']);
        } catch (\Exception $e) {
            $pesan   = $e->getMessage();
            \DB::rollback();

            return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => $pesan
                        ]
                ]);
        }
    }
}
