<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Datatables;

class PageDatatableController extends Controller
{
    public function __construct() {}
    
    public function all(Request $req) {   
        if($req->input('with')) {
            $withArray = explode(',', $req->input('with'));
            $dataForDatatables = Page::where('page_type_id', '!=', 5)->where('page_type_id', '!=', 6)->with($withArray)->get();
        } else {
            $dataForDatatables = Page::where('page_type_id', '!=', 5)->where('page_type_id', '!=', 6)->get();
        }

        return Datatables::of($dataForDatatables)
            ->editColumn('created_at', function($page){
                return $page->created_at->format('Y-m-d');
            })
            ->editColumn('url', function($page) {
                return '/'.$page->url.'/';
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') like ?", ["%$keyword%"]);
            })
            ->addColumn('menu-name', function ($page) {
                return (string) $page->menu_name;
            })
            ->addColumn('menu-position', function ($page) {
                if($page->page_type_id == 0)
                    return "Tidak pada Menu";
                else
                    return $page->type->description;
            })
            ->addColumn('action', function ($page) {
                if($page->id != 1 && $page->id != 2 && $page->id != 3)
                    return '<div class="ui icon tiny buttons">
                                <a href="/'.$page->url.'/" target="_blank" class="ui green basic icon button" 
                                        data-tooltip="Pertinjau" 
                                        data-position="top center"><i class="unhide icon"></i>
                                </a>
                                <a href="'.url('/admin/page/edit/'.$page->id).'" class="ui blue basic icon button" 
                                        data-tooltip="Sunting" 
                                        data-position="top center"><i class="edit icon"></i>
                                </a>
                                <button onclick="showConfirmModal(this)" 
                                        data-modalid="m_confirm_delete"
                                        data-action="'.url('/admin/page/destroy/'.$page->id).'"
                                        data-method="GET"
                                        data-m-header="Hapus Laman ID #'.$page->id.'"
                                        data-m-sub-header="Apakah Anda yakin akan menghapus Laman <strong>'.$page->title.'</strong> dengan ID <strong>#'.$page->id.'</strong>?"
                                        class="ui red basic icon button" 
                                        data-tooltip="Hapus" 
                                        data-position="top center"><i class="delete icon"></i>
                                </button>
                            </div>';
                else 
                    return '<div class="ui icon tiny buttons">
                            <a href="/'.$page->url.'/" target="_blank" class="ui green basic icon button" 
                                    data-tooltip="Pertinjau" 
                                    data-position="top center"><i class="unhide icon"></i>
                            </a>
                            <a href="'.url('/admin/page/edit/'.$page->id).'" class="ui blue basic icon button" 
                                    data-tooltip="Sunting" 
                                    data-position="top center"><i class="edit icon"></i>
                            </a>
                        </div>';
            })
            ->make(true);
    }
}
