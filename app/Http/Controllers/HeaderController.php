<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Header;

class HeaderController extends Controller
{
    public function __construct() {}

    public function index() {
        return view('admin.header.create');
    }

    public function store(Request $req) {
        $header = new Header;
        $header->name = $req->header['name'];
        $header->description = $req->header['description'];
        $header->mheader_id = $req->header['mheader_id'];
        $header->menu_id = $req->header['menu_id'];
        $header->save();

        return back();
    }
}
