<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageApiController extends Controller
{
    public function __construct() {}

    public function cekUrlIsExsist($url) {
        $curPage = Page::where('url', '=', $url)->first();
        if($curPage)
            return 1;
        else
            return 0;
    }
}
