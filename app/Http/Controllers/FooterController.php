<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Footer;

class FooterController extends Controller
{
    public function __construct() {}

    public function index() {
        return view('admin.footer.create');
    }

    public function store(Request $req) {
        $footer = new Footer;
        $footer->name = $req->footer['name'];
        $footer->description = $req->footer['description'];
        $footer->mfooter_id = $req->footer['mfooter_id'];
        $footer->menu_id = $req->footer['menu_id'];
        $footer->save();

        return back();
    }
}