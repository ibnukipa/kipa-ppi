<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\PageType;
use App\PageTag;
use App\Imageslider;

use Purifier;
class PageController extends Controller
{
    public function __construct() {}

    public function index() {
        // $data = ['page' => [
        //     'name' => "Home",
        //     'description' => 'Description'
        // ]];
        $curPage = Page::first();
        $data = ['page' => $curPage];
        return view('pages.page1')->with($data);
    }

    public function search(Request $req) {
        // dd($req->input());
        $key            = $req->input('search')['key'];
        $pageCur        = $req->input('search')['pageCur'];
        $pageSize       = $req->input('search')['pageSize'];
        $classification = $req->input('search')['classification'];
        $type           = $req->input('search')['type'];
        $sort           = $req->input('search')['sort'];

        $resultPpi = Page::where('title', 'like', '%'.$key.'%')
                        ->orWhere('title_en', 'like', '%'.$key.'%')
                        ->orWhere('subtitle_en', 'like', '%'.$key.'%')
                        ->orWhere('subtitle', 'like', '%'.$key.'%')
                        ->orWhere('menu_name', 'like', '%'.$key.'%')
                        ->orWhere('menu_name_en', 'like', '%'.$key.'%')
                        ->get();
        $resultPpi = $resultPpi->where('status', '=', 'publish');

        $url = "https://doaj.org/api/v1/search/journals/".$key;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        $resultDoaj = json_decode($response->getBody())->results;
        
        $data = [
            'resultPpi' => $resultPpi,
            'resultDoaj'=> $resultDoaj,
            'search'    => [
                '_key'              => $key,
                '_pageCur'          => $pageCur,
                '_pageSize'         => $pageSize,
                '_classification'   => $classification,
                '_type'             => $type,
                '_sort'             => $sort,
            ],
        ];
        return view('pages.search')->with($data);
    }

    public function page($name) {
        $curPage = Page::where('url', '=', $name)->where('status', '=', 'publish')->where('visible', '=', 1)->first();
        // dd("asd");
        if($curPage) {
            $data = ['page' => $curPage];
            
            return view('pages.page1')->with($data);
        } else {
            abort(404);
        }
    }

    public function edit($id) {
        $page = Page::find($id);
        
        if($page) {
            $data = [
                'page' => $page,
                'master' => [
                    'page_type'         => PageType::where('name', '!=', 'journal')
                                            ->Where('name', '!=', 'post')
                                            ->get(),
                    'page_parent'       => Page::where('page_type_id', '!=', 5)
                                            ->where('page_type_id', '!=', 6)
                                            ->where('id', '!=', $id)
                                            ->get(),
                    'page_tag'         => PageTag::get(),
                    'page_all'          => Page::get()
                ]
            ];

            return view('admin.page.edit')->with($data);

        } else return back()->withInput()->with([
                        'alert' => [
                            'type' => 'error',
                            'pesan' => 'Data Laman dengan ID #'.$id.' tidak ditemukan.'
                        ]
                ]);  
    }

    public function show() {
        $data = ['page' => [
            // 'name' => "Halaman",
            // 'description' => 'Description'
        ]];

        return view('admin.page.show')->with($data);
    }

    public function create() {
        $data = [
            'page' => [
                // 'name' => "Tambah Laman",
                // 'description' => 'Description'
            ],
            'master' => [
                'page_type'         => PageType::where('name', '!=', 'journal')
                                        ->Where('name', '!=', 'post')
                                        ->get(),
                'page_parent'       => Page::where('page_type_id', '!=', 5)
                                        ->where('page_type_id', '!=', 6)
                                        ->get(),
                'page_tag'         => PageTag::get(),
                'page_all'          => Page::get()
            ]
        ];

        return view('admin.page.create')->with($data);
    }

    public function destroy($id) {
        
        $page = Page::find($id);
        
        if($page) {
            \DB::beginTransaction();
            try {
                foreach ($page->sidebars() as $key => $value) {
                    $value->delete();
                }
                foreach ($page->imagesliders() as $key => $value) {
                    $value->delete();
                }

                $pagechild = Page::where('parent_id', '=', $page->id)->get();
                if($pagechild->count() > 0) {
                    $parentCandidate = $pagechild[0];
                    

                    foreach ($pagechild as $key => $value) {
                        $value->parent_id = $parentCandidate->id;
                        $value->save();
                    }

                    $parentCandidate->parent_id = 0;
                    $parentCandidate->save();
                }
                $page->delete();
                \DB::commit();
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'success',
                            'pesan' => 'Berhasil menghapus Laman dengan ID #'.$id
                        ]
                ]);  
            } catch(\Exception $e) {
                $success = false;
                $pesan   = $e->getMessage();
                \DB::rollback();
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => $pesan
                        ]
                ]);  
            }
        } else return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => 'Page dengan ID #'.$id.' tidak tersedia.'
                        ]
                ]);  
    }

    public function update(Request $req) {
        // dd($req->input());
        $curPage = Page::find($req->input('page')['id']);
        // dd($req->input('deletedimageslider'));
        // $curPage->imagesliders()->where('id', '=', );
        if($req->input('page')['menu_name'])
            $name_menu = $req->input('page')['menu_name'];
        else
            $name_menu = $req->input('page')['title'];

        if($req->input('page')['menu_name_en'])
            $name_menu_en = $req->input('page')['menu_name_en'];
        else
            $name_menu_en = $req->input('page')['title_en'];
        
        \DB::beginTransaction();

        try {
            if(!$curPage)
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => 'Laman dengan ID'.$req->input('page')['id'].' tidak ada.'
                        ]
                ]);
            $anotherPage = Page::where('id', '!=', $curPage->id)->where('url', '=', $req->input('page')['url'])->first();

            //URL EXSIST
            if($anotherPage)
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => 'URL Laman <strong>/'.$curPage->url.'/</strong> SUDAH terpakai. Mohon gunakan URL Laman yang lain'
                        ]
                ]);

            if((int)$req->input('page')['type'] == 0)
                $menu_visible = false;
            else 
                $menu_visible = true;

            if($curPage->id == 1 || $curPage->id == 2 || $curPage->id == 3) {
                $urlForPage = $curPage->url;
            } else {
                $urlForPage = $req->input('page')['url'];
            }
            
            $curPage->update([
                'title'                 => $req->input('page')['title'],
                'title_en'              => $req->input('page')['title_en'],
                'subtitle'              => $req->input('page')['subtitle'],
                'subtitle_en'           => $req->input('page')['subtitle_en'],
                'subject'               => '',
                'subject_en'            => '',
                'content'               => htmlspecialchars($req->input('page')['content']),
                'content_en'            => htmlspecialchars($req->input('page')['content_en']),
                'meta'                  => '',
                'url'                   => $urlForPage,
                'status'                => 'publish',
                'visible'               => !isset($req->input('page')['visible']),

                'textrunning_bool'      => isset($req->input('page')['textrunning_visible']),
                'textrunning_content'   => '',
                'textrunning_content_en'=> '',
                'textrunning_speed'     => 0,
                'textrunning_color'     => '',

                'menu_visible'          => $menu_visible,
                'menu_name'             => $name_menu,
                'menu_name_en'          => $name_menu_en,

                'image_slider_bool'     => isset($req->input('page')['imageslider_visible']),
                'siderbar_bool'         => isset($req->input('page')['sidebar_visible']),

                'siderbar_recent_journal_bool' => isset($req->input('page')['siderbar_recent_journal_bool']),
                'siderbar_recent_article_bool' => isset($req->input('page')['siderbar_recent_article_bool']),

                'parent_id'             => (int)$req->input('page')['parent'],
                'page_type_id'          => (int)$req->input('page')['type'],
                'creator_id'            => \Auth::user()->id,
            ]);

            //UPDATE TAGS
            $curPage->tags()->detach();
            if(isset($req->input('page')['tags'])) {
                foreach ($req->input('page')['tags'] as $key => $tag) {
                    $curTag = PageTag::where('name', '=', $tag)->first();
                    if($curTag) {
                        // TAG LAMA
                        $curPage->tags()->attach($curTag->id);
                    } else {
                        // TAG BARU
                        $newTag = PageTag::create(['name' => $tag]);
                        $curPage->tags()->attach($newTag->id);
                    }
                }
            }

            // CREATING SIDEBAR
            $curPage->sidebars()->delete();
            if(isset($req->input('page')['sidebar']['label'])) {
                foreach ($req->input('page')['sidebar']['label'] as $key => $label) {
                    
                    if($req->input('page')['sidebar']['label'][$key] && $req->input('page')['sidebar']['url'][$key])
                        $curPage->sidebars()->create([
                            'url'       => $req->input('page')['sidebar']['url'][$key],
                            'label'     => $label
                        ]);
                }
                if(count($curPage->sidebars()) == 0) {
                    $curPage->sidebar_bool = false;
                    $curPage->save();
                }
            }

            //Creating ImageSlider
            // $curPage->imagesliders()->delete();
            if($req->input('deletedimageslider') )
                foreach ($req->input('deletedimageslider') as $key => $deletedimageslide) {
                    if($deleteSLide = $curPage->imagesliders()->where('image_url', '=', $deletedimageslide))
                        $deleteSLide->delete();
                }

            if(isset($req->input('page')['imageslider_visible'])) {
                $input=$req->all();
                $images=array();
                if($files=$req->file('imageslider')){
                    foreach($files as $file){
                        $path = $file->store(
                            'imageslider', 'images'
                        );
                        $curPage->imagesliders()->create([
                            'image_url'   => $path,
                            'caption'     => ''
                        ]);
                    }
                }
                if(count($curPage->imagesliders()) == 0) {
                    $curPage->image_slider_bool = false;
                    $curPage->save();
                }
            }

            //CREATING TEXT RUNNING
            if(isset($req->input('page')['textrunning_visible'])) {
                $curPage->textrunning_content = $req->input('page')['runningtext']['content'];
                $curPage->textrunning_content_en = $req->input('page')['runningtext']['content_en'];
                $curPage->textrunning_speed = $req->input('page')['runningtext']['speed'];
                $curPage->textrunning_color = $req->input('page')['runningtext']['color'];
                $curPage->save();
            }

            \DB::commit();
            return redirect('/'.$curPage->url);
        } catch (\Exception $e) {
            
            $pesan   = $e->getMessage();
            \DB::rollback();

            return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => $pesan
                        ]
                ]);
        }
    }

    public function store(Request $req) {
        // dd($req->input('page')['menu_name']);
        
        if($req->input('page')['menu_name'])
            $name_menu = $req->input('page')['menu_name'];
        else
            $name_menu = $req->input('page')['title'];
        
        if($req->input('page')['menu_name_en'])
            $name_menu_en = $req->input('page')['menu_name_en'];
        else
            $name_menu_en = $req->input('page')['title_en'];

        \DB::beginTransaction();

        try {
            $curPage = Page::where('url', '=', $req->input('page')['url'])->first();

            //URL EXSIST
            if($curPage)
                return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => 'URL Laman <strong>/'.$curPage->url.'/</strong> SUDAH terpakai. Mohon gunakan URL Laman yang lain'
                        ]
                ]);

            if((int)$req->input('page')['type'] == 0)
                $menu_visible = false;
            else 
                $menu_visible = true;

            //CREATING NEW PAGE
            $newPage = Page::create([
                'title'                 => $req->input('page')['title'],
                'title_en'              => $req->input('page')['title_en'],
                'subtitle'              => $req->input('page')['subtitle'],
                'subtitle_en'           => $req->input('page')['subtitle_en'],
                'subject'               => '',
                'subject_en'            => '',
                'content'               => htmlspecialchars($req->input('page')['content']),
                'content_en'            => htmlspecialchars($req->input('page')['content_en']),
                'meta'                  => '',
                'url'                   => $req->input('page')['url'],
                'status'                => 'publish',
                'visible'               => !isset($req->input('page')['visible']),

                'textrunning_bool'      => isset($req->input('page')['textrunning_visible']),
                'textrunning_content'   => '',
                'textrunning_content_en'=> '',
                'textrunning_speed'     => 0,
                'textrunning_color'     => '',

                'menu_visible'          => $menu_visible,
                'menu_name_en'          => $name_menu_en,
                'menu_name'             => $name_menu,

                'image_slider_bool'     => isset($req->input('page')['imageslider_visible']),
                'siderbar_bool'         => isset($req->input('page')['sidebar_visible']),
                
                'siderbar_recent_journal_bool' => isset($req->input('page')['siderbar_recent_journal_bool']),
                'siderbar_recent_article_bool' => isset($req->input('page')['siderbar_recent_article_bool']),

                'parent_id'             => (int)$req->input('page')['parent'],
                'page_type_id'          => (int)$req->input('page')['type'],
                'creator_id'            => \Auth::user()->id,
            ]);

            //CREATING TAGS
            if(isset($req->input('page')['tags'])) {
                foreach ($req->input('page')['tags'] as $key => $tag) {
                    $curTag = PageTag::where('name', '=', $tag)->first();
                    if($curTag) {
                        // TAG LAMA
                        $newPage->tags()->attach($curTag->id);
                    } else {
                        // TAG BARU
                        $newTag = PageTag::create(['name' => $tag]);
                        $newPage->tags()->attach($newTag->id);
                    }
                }
            }

            // CREATING SIDEBAR
            if(isset($req->input('page')['sidebar']['label'])) {
                foreach ($req->input('page')['sidebar']['label'] as $key => $label) {
                    
                    if($req->input('page')['sidebar']['label'][$key] && $req->input('page')['sidebar']['url'][$key])
                        $newPage->sidebars()->create([
                            'url'       => $req->input('page')['sidebar']['url'][$key],
                            'label'     => $label
                        ]);
                }
                if(count($newPage->sidebars()) == 0) {
                    $newPage->sidebar_bool = false;
                    $newPage->save();
                }
            }

            //Creating ImageSlider
            if(isset($req->input('page')['imageslider_visible'])) {
                $input=$req->all();
                $images=array();
                if($files=$req->file('imageslider')){
                    foreach($files as $file){
                        $path = $file->store(
                            'imageslider', 'images'
                        );
                        $newPage->imagesliders()->create([
                            'image_url'   => $path,
                            'caption'     => ''
                        ]);
                    }
                }
                if(count($curPage->imagesliders()) == 0) {
                    $newPage->image_slider_bool = false;
                    $newPage->save();
                }

            }

            //CREATING TEXT RUNNING
            if(isset($req->input('page')['textrunning_visible'])) {
                $newPage->textrunning_content = $req->input('page')['runningtext']['content'];
                $newPage->textrunning_content_en = $req->input('page')['runningtext']['content_en'];
                $newPage->textrunning_speed = $req->input('page')['runningtext']['speed'];
                $newPage->textrunning_color = $req->input('page')['runningtext']['color'];
                $newPage->save();
            }

            \DB::commit();
            return redirect('/'.$req->input('page')['url']);
        } catch (\Exception $e) {
            
            $pesan   = $e->getMessage();
            \DB::rollback();

            return back()->withInput()->with([
                        'alert' => [
                            'type' => 'warning',
                            'pesan' => $pesan
                        ]
                ]);
        }
    }
}
