<?php

namespace App\Http\Controllers;

use App\Services\ImageRepo;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    protected $image;

    public function __construct(ImageRepo $imageRepository)
    {
        $this->image = $imageRepository;
    }

    public function postUpload()
    {
        $photo = Input::all();
        $response = $this->image->upload($photo);
        return $response;

    }

    public function deleteUpload()
    {

        $filename = Input::get('id');

        if(!$filename)
        {
            return 0;
        }

        $response = $this->image->delete( $filename );

        return $response;
    }
}
