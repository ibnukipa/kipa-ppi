<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Mheader;
use Datatables;

class MenuDatatableController extends Controller
{
    public function all(Request $req) {   
        if($req->input('with')) {
            $withArray = explode(',', $req->input('with'));
            $dataForDatatables = Menu::with($withArray)->get();
        } else {
            $dataForDatatables = Menu::get();
        }
        //return Menu::get();
        return Datatables::of($dataForDatatables)
            ->editColumn('created_at', function($menu){
                return $menu->created_at->format('Y-m-d');
            })
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') like ?", ["%$keyword%"]);
            })
            ->addColumn('action', function ($menu) {
                return '<div class="fluid ui icon tiny buttons">
                            <a class="ui green basic icon button" 
                                    data-tooltip="Pertinjau" 
                                    data-position="top center"><i class="unhide icon"></i>
                            </a>
                            <button class="ui blue basic icon button" 
                                    data-tooltip="Sunting" 
                                    data-position="top center"><i class="edit icon"></i>
                            </button>
                            <button onclick="showConfirmModal(this)" 
                                    data-modalid="m_confirm_delete"
                                    data-action="#"
                                    data-method="GET"
                                    data-m-header="Hapus Menu ID #'.$menu->id.'"
                                    data-m-sub-header="Apakah Anda yakin akan menghapus Menu <strong>'.$menu->name.'</strong> dengan ID <strong>#'.$menu->id.'</strong>?"
                                    class="ui red basic icon button" 
                                    data-tooltip="Hapus" 
                                    data-position="top center"><i class="delete icon"></i>
                            </button>
                        </div>';
            })
            ->make(true);
    }
}
