<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FileService;
use File;
use Response;

class FileController extends Controller
{
    public function store(Request $req) {
        $fileService = new FileService;
        
        return response()->json([
            'uploaded' => 1, 
            'fileName' => 'logo-square-green-512.png', 
            'url' => $fileService->getUrlFile('public', 'logo-square-green-512.png')
        ]);
    }

    public function getContent($filename) {
        $fileService = new FileService;
        $path = storage_path('app\\public\\ava\\' . $filename);
        // dd($path);
        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);
        

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;

        // return $fileService->getUrlFile('public', 'logo-square-green-512.png');
    }

    public function getFileImageBrowser() {
        $fileService = new FileService;
        
        return response()->json([
            [
                'image'     => $fileService->getUrlFile('public', 'logo-square-green-512.png'),
                'thumb'     => $fileService->getUrlFile('public', 'logo-square-green-512.png'),
                'folder'    => "gambar"
            ],
            [
                'image'     => $fileService->getUrlFile('public', 'logo-square-green-512.png'),
                'thumb'     => $fileService->getUrlFile('public', 'logo-square-green-512.png'),
                'folder'    => "gambar"
            ],
            [
                'image'     => $fileService->getUrlFile('public', 'logo-square-green-512.png'),
                'thumb'     => $fileService->getUrlFile('public', 'logo-square-green-512.png'),
                'folder'    => "gambar"
            ],
            [
                'image'     => $fileService->getUrlFile('public', 'logo-square-green-512.png'),
                'thumb'     => $fileService->getUrlFile('public', 'logo-square-green-512.png'),
                'folder'    => "gambar"
            ],
        ]);
    }
}
