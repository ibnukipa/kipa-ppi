<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Header;
use App\Setting;
use App\Page;
use App\JournalClasification;

class SettingController extends Controller
{
    public function __construct() {}

    public function index() {
        return view('admin.setting');
    }

    public function updateHeader(Request $request) {
        Setting::where('name','=','header')->update([
                'value' => $request->input('header')['title']
        ]);

        Setting::where('name','=','subheader')->update([
                'value' => $request->input('header')['subtitle']
        ]);

        return back()->withInput()->with([
            'alert' => [
                'type' => 'success',
                'color' => 'green',
                'pesan' => 'Berhasil meng-update setting Header Utama.'
            ]
        ]);  
    }

    public function updateClasification(Request $request) {
        // dd($request->input());
        foreach (JournalClasification::get() as $key => $value) {
            Page::where('journal_clasification_id', '=', $value->id)
                    ->update([
                        'journal_clasification_id' => null,
                        'status'    => 'draft',
                        'visible' => false,
                    ]);
        }

        JournalClasification::truncate();

        if($request->input('klasifikasi'))
            foreach ($request->input('klasifikasi')['name'] as $key => $value) {
                JournalClasification::create([
                    'code'          => substr(strtolower($request->input('klasifikasi')['name'][$key]), 0, 3),
                    'name'          => $request->input('klasifikasi')['name'][$key],
                    'priority'      => $request->input('klasifikasi')['priority'][$key],
                    'description'   => $request->input('klasifikasi')['description'][$key],
                ]); 
            }

        return back()->withInput()->with([
            'alert' => [
                'type' => 'success',
                'color' => 'green',
                'pesan' => 'Berhasil meng-update setting Header Utama.'
            ]
        ]);
    }

    public function updateSosmed(Request $request) {
        // dd($request->input());

        Setting::where('name','=','sosmed_facebook')->update([
            'value' => $request->input('sosmed')['facebook']
        ]);

        Setting::where('name','=','sosmed_twitter')->update([
            'value' => $request->input('sosmed')['twitter']
        ]);

        Setting::where('name','=','sosmed_world')->update([
            'value' => $request->input('sosmed')['world']
        ]);

        Setting::where('name','=','sosmed_youtube')->update([
            'value' => $request->input('sosmed')['youtube']
        ]);

        Setting::where('name','=','sosmed_linkedin')->update([
            'value' => $request->input('sosmed')['linkedin']
        ]);

        Setting::where('name','=','sosmed_skype')->update([
            'value' => $request->input('sosmed')['skype']
        ]);

        return back()->withInput()->with([
            'alert' => [
                'type' => 'success',
                'color' => 'green',
                'pesan' => 'Berhasil meng-update setting Sosial Media.'
            ]
        ]); 
    }

    public function updateUsernamePassword(Request $request) {
        
        $user = \Auth::user();
        
        $user->email = $request->input('user')['email'];
    
        if ( ! $request->input('user')['password'] == '')
        {
            $user->password = bcrypt($request->input('user')['password']);
        }
    
        $user->save();
    
        return back()->withInput()->with([
            'alert' => [
                'type' => 'success',
                'color' => 'green',
                'pesan' => 'Berhasil meng-update username dan password.'
            ]
        ]); 
    }
}
