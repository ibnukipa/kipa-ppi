<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Datatables;

class JournalDatatableController extends Controller
{
    public function __construct() {}
    
    public function all(Request $req) {   
        if($req->input('with')) {
            $withArray = explode(',', $req->input('with'));
            $dataForDatatables = Page::where('page_type_id', '=', 5)->with($withArray)->orderBy('updated_at', 'DESC')->get();
        } else {
            $dataForDatatables = Page::where('page_type_id', '=', 5)->orderBy('updated_at', 'DESC')->get();
        }

        return Datatables::of($dataForDatatables)
            ->editColumn('updated_at', function($page){
                return $page->updated_at->format('Y-m-d H:m:i');
            })
            ->editColumn('url', function($page) {
                return '/'.$page->url.'/';
            })
            ->editColumn('status', function($page) {
                if($page->status == 'publish')
                    return 'Publish';
                else
                    return 'Draft';
            })
            ->addColumn('klasifikasi', function($page) {
                if($page->journal_clasification_id)
                    return $page->clasification->name;
                else
                    return 'Belum terklasifikasi';
            })
            ->filterColumn('updated_at', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(updated_at,'%Y-%m-%d') like ?", ["%$keyword%"]);
            })
            ->addColumn('action', function ($page) {
                if($page->status == 'publish')
                    return '<div class="ui icon tiny buttons">
                                <a href="/'.$page->url.'/" target="_blank" class="ui green basic icon button" 
                                        data-tooltip="Pertinjau" 
                                        data-position="top center"><i class="unhide icon"></i>
                                </a>
                                <a href="'.url('/admin/journal/edit/'.$page->id).'" class="ui blue basic icon button" 
                                            data-tooltip="Sunting" 
                                            data-position="top center"><i class="edit icon"></i>
                                </a>
                                <button onclick="showConfirmModal(this)" 
                                        data-modalid="m_confirm_delete"
                                        data-action="'.url('/admin/journal/destroy/'.$page->id).'"
                                        data-method="GET"
                                        data-m-header="Hapus Laman ID #'.$page->id.'"
                                        data-m-sub-header="Apakah Anda yakin akan menghapus Laman Journal <strong>'.$page->title.'</strong> dengan ID <strong>#'.$page->id.'</strong>?"
                                        class="ui red basic icon button"
                                        data-tooltip="Hapus" 
                                        data-position="top center"><i class="delete icon"></i>
                                </button>
                            </div>';
                else 
                    return '<div class="ui icon tiny buttons">
                                <a href="'.url('/admin/journal/edit/'.$page->id).'" class="ui blue basic icon button" 
                                            data-tooltip="Sunting" 
                                            data-position="top center"><i class="edit icon"></i>
                                </a>
                                <button onclick="showConfirmModal(this)" 
                                        data-modalid="m_confirm_delete"
                                        data-action="'.url('/admin/journal/destroy/'.$page->id).'"
                                        data-method="GET"
                                        data-m-header="Hapus Laman ID #'.$page->id.'"
                                        data-m-sub-header="Apakah Anda yakin akan menghapus Laman Journal <strong>'.$page->title.'</strong> dengan ID <strong>#'.$page->id.'</strong>?"
                                        class="ui red basic icon button" 
                                        data-tooltip="Hapus"
                                        data-position="top center"><i class="delete icon"></i>
                                </button>
                            </div>';
            })
            ->make(true);
    }
}
