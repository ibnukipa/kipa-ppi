<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $table = 'footers';

    protected $fillable = [
        'name', 'description', 'mfooter_id'
    ];

    public function menu() {
        return $this->belongsTo('App\Menu', 'menu_id');
    }

    public function mheader() {
        return $this->belongsTo('App\Mfooter', 'mfooter_id');
    }
}
