<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'title', 'title_en',
        'subtitle', 'subtitle_en',
        'subject', 'subject_en',
        'content', 'content_en',
        'meta', 'url', 'status', 
        'textrunning_bool', 
        'textrunning_content', 'textrunning_content_en',
        'textrunning_speed',
        'textrunning_color',
        'visible', 'menu_visible', 'menu_clickable',
        'menu_name', 'menu_name_en',
        'image_slider_bool',
        'siderbar_bool',
        'siderbar_recent_journal_bool',
        'siderbar_recent_article_bool',
        'journal_source_page',
        'image_cover_potrait',
        'image_cover_landscape_bool',
        'image_cover_landscape',
        'image_cover_thumbnail',
        'siderbar_with_tag_bool',
        'journal_clasification_id',
        'parent_id',
        'page_type_id',
        'creator_id',
        'accreditation'
    ];

    // public function layout() {
    //     return $this->belongsTo('App\Layout', 'layout_id');
    // }

    // public function header() {
    //     return $this->belongsTo('App\Header', 'header_id');
    // }

    // public function footer() {
    //     return $this->belongsTo('App\Footer', 'footer_id');
    // }

    public function creator() {
        return $this->belongsTo('App\User', 'creator_id');
    }

    //POS, PAGE, JOURNAL, GUIDE
    public function type() {
        return $this->belongsTo('App\PageType', 'page_type_id');
    }

    public function clasification() {
        return $this->belongsTo('App\JournalClasification', 'journal_clasification_id');
    }

    // public function category() {
    //     return $this->belongsTo('App\PageCategory', 'page_category_id');
    // }

    public function tags() {
        return $this->belongsToMany('App\PageTag', 'page_tag', 'page_id', 'tag_id');
    }

    public function sidebars() {
        return $this->hasMany('App\Sidebar', 'page_id');
    }

    public function imagesliders() {
        return $this->hasMany('App\Imageslider', 'page_id');
    }

    // public function widgets() {
    //     return $this->belongsToMany('App\Widget', 'widget_page', 'page_id', 'widget_id');
    // }
}
