<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mlayout extends Model
{
    protected $table = 'mlayouts';

    protected $fillable = [
        'name', 'image', 'description',
    ];
}
