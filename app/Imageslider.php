<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imageslider extends Model
{
    protected $table = 'imageslider';
    protected $fillable = [
        'page_id', 
        'image_url',
        'filename',
        'original_name',
        'caption', 
    ];
}
