<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    protected $table = 'headers';

    protected $fillable = [
        'name', 'description', 'mheader_id'
    ];

    public function menu() {
        return $this->belongsTo('App\Menu', 'menu_id');
    }

    public function mheader() {
        return $this->belongsTo('App\Mheader', 'mheader_id');
    }

}
