<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    protected $table = 'layouts';

    protected $fillable = [
        'name', 'image', 'description',
    ];

    public function mlayout() {
        return $this->belongsTo('App\MLayout', 'mlayout_id');
    }

    public function header() {
        return $this->belongsTo('App\Header', 'header_id');
    }

    public function footer() {
        return $this->belongsTo('App\Footer', 'footer_id');
    }

    public function pages() {
        return $this->hasMany('App\Page', 'layout_id');
    }

    public function widgets() {
        return $this->belongsToMany('App\Widget', 'widget_layout', 'layout_id', 'widget_id');
    }
}
