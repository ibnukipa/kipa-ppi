<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mheader extends Model
{
    protected $table = 'mheaders';

    protected $fillable = [
        'name', 'image', 'description',
    ];
}
