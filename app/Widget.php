<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $table = 'widgets';

    protected $fillable = [
        'name', 'position',
    ];

    public function type() {
        return $this->belongsTo('App\WidgetType', 'widget_type_id');
    }
}
