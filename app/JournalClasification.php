<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalClasification extends Model
{
    protected $table = 'journal_clasification';
    
    protected $fillable = [
        'code', 'name', 'priority','description'
    ];
}
