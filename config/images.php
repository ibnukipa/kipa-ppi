<?php
return [
    'full_size'   => env('UPLOAD_FULL_SIZE', public_path('images/imageslider/full_size/')),
    'icon_size'   => env('UPLOAD_ICON_SIZE', public_path('images/imageslider/icon_size/')),
];