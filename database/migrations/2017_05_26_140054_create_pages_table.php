<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('title_en')->nullable();
            $table->text('subtitle')->nullable();
            $table->text('subtitle_en')->nullable();
            $table->string('subject')->nullable();
            $table->string('subject_en')->nullable();
            $table->text('content')->nullable();
            $table->text('content_en')->nullable();
            $table->text('meta')->nullable();
            $table->string('url')->nullable();
            $table->string('status')->nullable();
            $table->boolean('visible')->nullable();
            $table->boolean('accreditation')->nullable()->default(false);

            $table->boolean('textrunning_bool')->nullable();
            $table->text('textrunning_content')->nullable();
            $table->text('textrunning_content_en')->nullable();
            $table->integer('textrunning_speed')->nullable();
            $table->string('textrunning_color')->nullable();

            $table->boolean('menu_visible')->nullable();
            $table->boolean('menu_clickable')->nullable();
            $table->string('menu_name')->nullable();
            $table->string('menu_name_en')->nullable();
            
            $table->boolean('image_slider_bool')->nullable();
            $table->boolean('siderbar_bool')->nullable();

            $table->string('journal_source_page')->nullable();
            $table->integer('journal_clasification_id')->nullable();

            $table->string('image_cover_potrait')->nullable();
            $table->boolean('image_cover_landscape_bool')->nullable();
            $table->string('image_cover_landscape')->nullable();
            $table->string('image_cover_thumbnail')->nullable();

            $table->boolean('siderbar_recent_journal_bool')->nullable();
            $table->boolean('siderbar_recent_article_bool')->nullable();
            $table->boolean('siderbar_with_tag_bool')->nullable();

            //layout
            // $table->integer('layout_id')->nullable();
            //or
            // $table->integer('mlayout_id')->nullable();
            // $table->integer('header_id')->nullable();
            // $table->integer('footer_id')->nullable();

            $table->integer('parent_id')->nullable();

            $table->integer('page_type_id')->nullable();
            // $table->integer('page_category_id')->nullable();
            $table->integer('creator_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
