<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layouts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();

            // $table->integer('mlayout_id')->unsigned()->index()->nullable();
            // $table->foreign('mlayout_id')->references('id')->on('mlayout')->onDelete('cascade');
            $table->integer('mlayout_id');
            // $table->integer('headers_id')->unsigned()->index()->nullable();
            // $table->foreign('headers_id')->references('id')->on('headers')->onDelete('cascade');
            $table->integer('header_id');
            // $table->integer('footers_id')->unsigned()->index()->nullable();
            // $table->foreign('footers_id')->references('id')->on('footers')->onDelete('cascade');
            $table->integer('footer_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layouts');
    }
}
