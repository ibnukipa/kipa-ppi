<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('description')->nullable();

            // $table->integer('mheader_id')->unsigned()->index()->nullable();
            // $table->foreign('mheader_id')->references('id')->on('mheader')->onDelete('cascade');
            $table->integer('mheader_id');
            // $table->integer('menu_id')->unsigned()->index()->nullable();
            // $table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade');
            $table->integer('menu_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headers');
    }
}
