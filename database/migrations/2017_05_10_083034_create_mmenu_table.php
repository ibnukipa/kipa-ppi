<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMmenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mmenus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('display');
            $table->string('description')->nullable();
            // $table->boolean('create')->default(0);
            // $table->boolean('read')->default(1);
            // $table->boolean('update')->default(0);
            // $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mmenus');
    }
}
