<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextrunningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('textrunning', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('content')->nullable();
            $table->string('color')->nullable();
            $table->string('background')->nullable();
            $table->integer('speed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('textrunning');
    }
}
