<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWidgetListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget_lists', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('widget_id');

            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->string('sub_title')->nullable();

            $table->string('value')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget_lists');
    }
}
