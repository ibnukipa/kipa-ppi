<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_structures', function (Blueprint $table) {
            $table->increments('id');

            // $table->integer('menus_id')->unsigned()->index()->nullable();
            // $table->foreign('menus_id')->references('id')->on('menus')->onDelete('cascade');
            $table->integer('menu_id');

            $table->string('navigation_label')->nullable();
            $table->string('url')->nullable();
            $table->tinyInteger('level')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_structures');
    }
}
