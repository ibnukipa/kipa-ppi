<?php

use Illuminate\Database\Seeder;
use App\Mlayout;
use App\Mheader;
use App\Mfooter;

use App\Layout;
use App\Header;
use App\Footer;
class LayoutKomponenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $mlayout = Mlayout::create([
        //     'name'          => 'layout1',
        //     'image'         => 'http://',
        //     'description'   => 'Layout 1',
        // ]);

        $mheader = Mheader::create([
            'name'          => 'header1',
            'image'         => 'http://',
            'description'   => 'Header 1',
        ]);

        $mfooter = Mfooter::create([
            'name'          => 'footer1',
            'image'         => 'http://',
            'description'   => 'Footer 1',
        ]);

        Header::create([
            'name'          => 'Header 1',
            'description'   => 'Contoh Header Pertama',
            'mheader_id'     => $mheader->id,
            'menu_id'       => 0,
        ]);

        Header::create([
            'name'          => 'Header 2',
            'description'   => 'Contoh Header Kedua',
            'mheader_id'     => $mheader->id,
            'menu_id'       => 0,
        ]);

        Footer::create([
            'name'          => 'Footer 1',
            'description'   => 'Contoh Footer Pertama',
            'mfooter_id'     => $mfooter->id,
            'menu_id'       => 0,
        ]);

        Footer::create([
            'name'          => 'Footer 2',
            'description'   => 'Contoh Footer Kedua',
            'mfooter_id'     => $mfooter->id,
            'menu_id'       => 0,
        ]);
    }
}
