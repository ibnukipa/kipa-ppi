<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Mmenu;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $role = Role::create([
        //     'name'          => 'user',
        //     'display'       => 'user',
        //     'description'   => 'A normal user'
        // ]);

        // $role = Role::create([
        //     'name'          => 'author',
        //     'display'       => 'author',
        //     'description'   => 'An Author'
        // ]);

        // $role = Role::create([
        //     'name'          => 'admin',
        //     'display'       => 'admin',
        //     'description'   => 'An Admin'
        // ]);

        $role = Role::create([
            'name'          => 'superadmin',
            'display'       => 'super admin',
            'description'   => 'An Superadmin'
        ]);
    }
}
