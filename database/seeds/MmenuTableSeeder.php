<?php

use Illuminate\Database\Seeder;
use App\Mmenu;

class MmenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Mmenu::create([
            'name'          => 'post',
            'display'       => 'management posts',
            'description'   => 'An post menu'
        ]);

        Mmenu::create([
            'name'          => 'feedback',
            'display'       => 'management feedbacks',
            'description'   => 'An post menu'
        ]);

        Mmenu::create([
            'name'          => 'user',
            'display'       => 'management users',
            'description'   => 'An user menu'
        ]);

        Mmenu::create([
            'name'          => 'page',
            'display'       => 'management pages',
            'description'   => 'An page menu'
        ]);


        Mmenu::create([
            'name'          => 'journal',
            'display'       => 'management journals',
            'description'   => 'An journal menu'
        ]);

        Mmenu::create([
            'name'          => 'menu',
            'display'       => 'management menus',
            'description'   => 'An menu menu'
        ]);


        Mmenu::create([
            'name'          => 'role',
            'display'       => 'management roles',
            'description'   => 'An role menu'
        ]);
    }
}
