<?php

use Illuminate\Database\Seeder;
use App\Page;
use App\PageCategory;
use App\PageTag;
use App\PageType;
use App\JournalClasification;
use App\Setting;

class PageKomponenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'name'  =>  'header',
            'value' =>  'Pusat Publikasi Ilmiah'
        ]);

        Setting::create([
            'name'  =>  'subheader',
            'value' =>  'portal keilmiahan ITS'
        ]);

        Setting::create([
            'name'  =>  'judul_footer1',
            'value' =>  'Information For'
        ]);

        Setting::create([
            'name'  =>  'judul_footer2',
            'value' =>  'Open Access'
        ]);

        Setting::create([
            'name'  =>  'judul_footer3',
            'value' =>  'Help And Info'
        ]);

        Setting::create([
            'name'  =>  'sosmed_facebook',
            'value' =>  'https://facebook.co.id'
        ]);

        Setting::create([
            'name'  =>  'sosmed_twitter',
            'value' =>  'https://facebook.co.id'
        ]);

        Setting::create([
            'name'  =>  'sosmed_world',
            'value' =>  'http://iptek.its.ac.id'
        ]);

        Setting::create([
            'name'  =>  'sosmed_youtube',
            'value' =>  'https://facebook.co.id'
        ]);

        Setting::create([
            'name'  =>  'sosmed_linkedin',
            'value' =>  'https://facebook.co.id'
        ]);

        Setting::create([
            'name'  =>  'sosmed_skype',
            'value' =>  'https://facebook.co.id'
        ]);

        PageType::create([
            'name'          => 'header',
            'description'   => 'Header',
        ]);

        PageType::create([
            'name'          => 'footer1',
            'description'   => 'Footer 1 (information for)',
        ]);

        PageType::create([
            'name'          => 'footer2',
            'description'   => 'Footer 2 (open access)',
        ]);

        PageType::create([
            'name'          => 'footer1',
            'description'   => 'Footer 3 (help and info)',
        ]);

        PageType::create([
            'name'          => 'journal',
            'description'   => 'Journal',
        ]);

        PageType::create([
            'name'          => 'post',
            'description'   => 'Post',
        ]);

        JournalClasification::create([
            'code'          => 'int',
            'name'          => 'International',
            'priority'      => 1,
            'description'   => 'International',
        ]);

        JournalClasification::create([
            'code'          => 'nat',
            'name'          => 'Nasional',
            'priority'      => 2,
            'description'   => 'Nasional',
        ]);

        JournalClasification::create([
            'code'          => 'swa',
            'name'          => 'Abmas - Swagati',
            'priority'      => 3,
            'description'   => 'Abmas - Swagati',
        ]);

        // PageTag::create([
        //     'name'          => 'tata-cara',
        //     'description'   => 'Tata Cara Penggunaan',
        // ]);

        Page::create([
            'title'                 => "Beranda",
            'title_en'              => "Home",
            'subtitle'              => 'Website Pusat Publikasi Ilmiah',
            'subtitle_en'           => 'Pusat Publikasi Ilmiah Website',
            'subject'               => '',
            'subject_en'            => '',
            'content'               => '',
            'content_en'            => '',
            'meta'                  => '',
            'url'                   => 'home',
            'status'                => 'publish',
            'visible'               => true,

            'textrunning_bool'      => true,
            'textrunning_content'   => 'Home Indo | Home Indo | Home Indo',
            'textrunning_content_en'=> 'Home English | Home English | Home English',
            'textrunning_speed'     => 20,
            'textrunning_color'     => 'blue',

            // 'menu_visible'          => true,
            // 'menu_clickable'        => true,
            'menu_name'             => 'Beranda',
            'menu_name_en'          => 'Home',

            'image_slider_bool'     => false,
            'siderbar_bool'         => false,
            'image_cover_landscape_bool' => false,

            'parent_id'             => 0,
            'page_type_id'          => 1,
            'creator_id'            => 1,
        ]);

        Page::create([
            'title'                 => "Journals",
            'title_en'              => "Journals",
            'subtitle'              => 'Kumpulan semua journal yang ada di Pusat Publikasi Ilmiah ITS',
            'subtitle_en'           => 'Kumpulan semua journal yang ada di Pusat Publikasi Ilmiah ITS',
            'subject'               => '',
            'subject_en'            => '',
            'content'               => '',
            'content_en'            => '',
            'meta'                  => '',
            'url'                   => 'journals',
            'status'                => 'publish',
            'visible'               => true,

            'textrunning_bool'      => true,
            'textrunning_content'   => 'Kumpulan semua journal yang ada di Pusat Publikasi Ilmiah ITS | Kumpulan semua journal yang ada di Pusat Publikasi Ilmiah ITS | Kumpulan semua journal yang ada di Pusat Publikasi Ilmiah ITS',
            'textrunning_content_en'=> 'Kumpulan semua journal yang ada di Pusat Publikasi Ilmiah ITS | Kumpulan semua journal yang ada di Pusat Publikasi Ilmiah ITS | Kumpulan semua journal yang ada di Pusat Publikasi Ilmiah ITS',
            'textrunning_speed'     => 20,
            'textrunning_color'     => 'blue',

            // 'menu_visible'          => true,
            // 'menu_clickable'        => true,
            'menu_name'             => 'Journals',
            'menu_name_en'          => 'Journals',

            'image_slider_bool'     => false,
            'siderbar_bool'         => false,
            'image_cover_landscape_bool' => false,

            'parent_id'             => 0,
            'page_type_id'          => 1,
            'creator_id'            => 1,
        ]);

        Page::create([
            'title'                 => "Articles",
            'title_en'              => "Articles",
            'subtitle'              => 'Kumpulan artikel terbaru hanya untuk Anda',
            'subtitle_en'           => 'Kumpulan artikel terbaru hanya untuk Anda',
            'subject'               => '',
            'subject_en'            => '',
            'content'               => '',
            'content_en'            => '',
            'meta'                  => '',
            'url'                   => 'articles',
            'status'                => 'publish',
            'visible'               => true,

            'textrunning_bool'      => true,
            'textrunning_content'   => 'Kumpulan artikel terbaru hanya untuk Anda | Kumpulan artikel terbaru hanya untuk Anda | Kumpulan artikel terbaru hanya untuk Anda',
            'textrunning_content_en'=> 'Kumpulan artikel terbaru hanya untuk Anda | Kumpulan artikel terbaru hanya untuk Anda | Kumpulan artikel terbaru hanya untuk Anda',
            'textrunning_speed'     => 20,
            'textrunning_color'     => 'blue',

            // 'menu_visible'          => true,
            // 'menu_clickable'        => true,
            'menu_name'             => 'Articles',
            'menu_name_en'             => 'Articles',

            'image_slider_bool'     => false,
            'siderbar_bool'         => false,
            'image_cover_landscape_bool' => false,

            'parent_id'             => 0,
            'page_type_id'          => 1,
            'creator_id'            => 1,
        ]);
    }
}
