<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_superadmin    = Role::where('name', 'superadmin')->first();

        $user = User::create([
            'uuid'          => Uuid::generate(1),
            'first_name'    => 'Super Admin',
            'last_name'    => 'PPI',
            'called_name'    => 'Super Admin',
            'nickname'    => 'SuperAdmin',
            'email'    => 'superadmin@ppi.com',
            'password'    => bcrypt('superadmin@ppi.com'),
        ]);
        $user->assignRole($role_superadmin->id);

    }
}
