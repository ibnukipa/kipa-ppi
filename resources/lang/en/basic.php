<?php

return [
    'lang'        => 'language',
    'login'       => 'login',
    'home'        => 'home',
    'register'    => 'register',
    'logout'      => 'logout',
    'dashboard'   => 'dashboard',

    'password'          => 'password',
    'password-remember' => 'remember me',
    'password-confirm'  => 'confirm password',
    'password-forgot'   => 'forgot your password?',
    'password-reset'    => 'reset password',
    'password-reset-send' => 'send password reset link',

    'email-address' => 'e-mail address',

    'user-name'     => 'full name',
];