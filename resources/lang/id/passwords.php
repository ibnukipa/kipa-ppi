<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password'  => 'password setidaknya 6 karakter dan harus sesuai dengan konfirmasi password.',
    'reset'     => 'password anda berhasil di-reset!',
    'sent'      => 'kami telah mengirim email untuk me-reset password anda!',
    'token'     => 'token reset password tidak valid.',
    'user'      => "email tidak terdaftar pada sistem kamu.",

];
