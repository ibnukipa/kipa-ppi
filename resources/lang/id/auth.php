<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => 'Username dan password tidak cocok.',
    'throttle'      => 'Terlalu banyak upaya masuk. Tolong coba lagi setelah :seconds detik.',

    'lost-password-1' => 'Lupa password?',
    'lost-password-2' => 'Reset disini.',


];
