<?php

return [
    'app-name'    => 'KiFramework',
    'app-slogan'  => 'Jelajahi <b>karya</b> kami!',
    'lang'        => 'bahasa',
    'login'       => 'masuk',
    'home'        => 'home',
    'register'    => 'daftar',
    'logout'      => 'keluar',
    'dashboard'   => 'dashboard',

    'password'          => 'kata kunci',
    'password-remember' => 'ingatkan saya',
    'password-confirm'  => 'konfirmasi password',
    'password-forgot'   => 'lupa password?',
    'password-reset'    => 'reset password',
    'password-reset-send' => 'kirim link reset password',

    'email-address' => 'e-mail',

    'user-name'     => 'nama lengkap',
];
