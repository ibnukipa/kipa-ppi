<div class="ui small modal" id="m_confirm_delete">
    <i class="close icon"></i>
    <div class="content no-padding">
        <form class="ui large form no-margin no-padding" method="" action="#" style="padding: .5em 2em">
            {{ csrf_field() }}
            <h2 class="ui header no-margin center" style="padding: 1em">
                <span id="confirm_header">sdf</span>
                <div class="sub header"><span id="confirm_sub_header"></span></div>
            </h2>
            <div class="actions right m-top1">
                <div class="ui fluid buttons">
                    <div class="ui black deny button borderradiusless" style="margin-left: 0px">
                        Tidak
                    </div>
                    <button type="submit" class="ui red right button borderradiusless" style="margin-left: 0px">
                        Ya
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>