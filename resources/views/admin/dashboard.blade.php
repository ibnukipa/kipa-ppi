@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {{ $page['name'] }}
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {{ Request::route()->getAction()['page_name'] }}
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui segment main basic">
    <div class="ui statistics three">
        <div class="statistic">
            <div class="value">
            <i class="folder open icon"></i> {{ App\Page::where('page_type_id', '=', 5)->count() }}
            </div>
            <div class="label">
                Journals
            </div>
        </div>
        <div class="statistic">
            <div class="value">
            <i class="file icon"></i> {{ App\Page::where('page_type_id', '!=', 5)->where('page_type_id', '!=', 6)->count() }}
            </div>
            <div class="label">
                Pages
            </div>
        </div>
        <div class="statistic">
            <div class="value">
            <i class="eyedropper icon"></i> {{ App\Page::where('page_type_id', '=', 6)->count() }}
            </div>
            <div class="label">
                Posts
            </div>
        </div>
    </div>
</div>

@include('footers.footer2')
@endsection
