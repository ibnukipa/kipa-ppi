@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {{ $page['name'] }}
        @php $page_name = $page['name'] @endphp
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {{ Request::route()->getAction()['page_name'] }}
        @php $page_name = Request::route()->getAction()['page_name'] @endphp
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection

@section('extend_lib')
    <script src="{{ asset('libs/ckeditor/ckeditor.js') }}"></script>
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui clearing padded segment basic no-margin roboto" style="background-color: #F9FAFB; padding-top: .5em; padding-bottom: .8em">
    <div class="ui breadcrumb right">
        <a class="section">Dashboard</a>
        <i class="right chevron icon divider"></i>
        <a class="section">Halaman</a>
        <i class="right arrow icon divider"></i>
        <div class="active section">Tambah</div>
    </div>
</div>
<div class="ui segment padded main basic no-margin" style="background: #f1f1f1">
    <form id="form_page_create" class="ui form" action="{{ route('page.store') }}"  role="form" method="POST">
        {{ csrf_field() }}
        <div class="ui basic clearing segment no-padding no-margin center aligned">
            <div class="ui buttons left floated">
                <button type="submit" class="ui blue left labeled icon button"><i class="save icon"></i>Simpan</button>
            </div>
            <div class="ui small buttons">
                <!--<a class="ui green button">Tambah</a>-->
            </div>
            <div class="ui buttons right floated middle">
                <!--<button class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah pelanggan</button>-->
            </div>
        </div>
        <div class="ui divider"></div>

        <div class="equal width fields">
            <div class="field">
                <div class="ui massive input">
                    <input name="page[title]" placeholder="Judul laman" type="text">
                </div>
            </div>
        </div>

        <div class="fields">
            <div class="field">
                <div class="ui selection dropdown costum" id="page_type">
                    <input name="page[type]" type="hidden">
                    <div class="default text">Tipe laman</div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        @foreach($master['page_type'] as $key => $page_type)
                            <div class="item" data-value="{{ $page_type->name }}">
                                <!--<i class="visa icon"></i>-->
                                {{ $page_type->description }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="six wide field">
                <div class="ui right labeled input">
                    <div class="ui label">
                        {{ url('/') }}/
                    </div>
                    <input name="page[url]" value="tentang-ppi" type="text">
                    <div class="ui label">
                        /
                    </div>
                </div>
            </div>
        </div>

        <div class="equal width fields">
            <div class="field">
                <textarea name="page[content]" placeholder="Content laman"></textarea>
            </div>
        </div>

        <!--<div class="ui accordion">
            <a class="ui right labeled icon basic button borderradiusless trigger">
                Forward
                <i class="down chevron icon"></i>
            </a>
            <div class="content">
                tes
            </div> 
        </div>-->
        <div class="ui grid">
            <div class="row">
                <div class="sixteen wide column">
                    <div class="ui accordion field" style="background: white">
                        <div class="title no-padding">
                            <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                <div  class="ui header no-margin">
                                    <i class="wizard icon"></i>
                                    <div class="content">
                                        Personalizing
                                        <div class="sub header">Manage your page's personalizing</div>
                                    </div>
                                </div >
                                <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                            </a>
                        </div>
                        <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                            <div class="equal width fields">
                                <div class="field">
                                    <label>
                                        <div  class="ui small header">
                                            <!--<i class="quote right icon"></i>-->
                                            <div class="content">
                                                Header
                                                <!--<div class="sub header">Manage your preferences</div>-->
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui selection dropdown costum" id="page_header">
                                        <input name="page[header]" type="hidden">
                                        <div class="default text">Header laman</div>
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            @foreach($master['page_type'] as $key => $page_type)
                                                <div class="item" data-value="{{ $page_type->name }}">
                                                    {{ $page_type->description }}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                Footer
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui selection dropdown costum" id="page_footer">
                                        <input name="page[footer]" type="hidden">
                                        <div class="default text">Footer laman</div>
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            @foreach($master['page_type'] as $key => $page_type)
                                                <div class="item" data-value="{{ $page_type->name }}">
                                                    {{ $page_type->description }}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                Image Slider
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui selection dropdown costum" id="page_imageslider">
                                        <input name="page[imageslider]" type="hidden">
                                        <div class="default text">Image slider laman</div>
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            @foreach($master['page_type'] as $key => $page_type)
                                                <div class="item" data-value="{{ $page_type->name }}">
                                                    {{ $page_type->description }}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                Running Text
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui selection dropdown costum" id="page_runningtext">
                                        <input name="page[runningtext]" type="hidden">
                                        <div class="default text">Running text laman</div>
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            @foreach($master['page_type'] as $key => $page_type)
                                                <div class="item" data-value="{{ $page_type->name }}">
                                                    {{ $page_type->description }}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top: 0">
                <div class="sixteen wide column">
                    <div class="ui accordion field" style="background: white">
                        <div class="title no-padding">
                            <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                <div  class="ui header no-margin">
                                    <i class="cubes icon"></i>
                                    <div class="content">
                                        Widgets
                                        <div class="sub header">Manage your page's widgets</div>
                                    </div>
                                </div >
                                <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                            </a>
                        </div>
                        <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                            <label>Maiden Name</label>
                            <input placeholder="Maiden Name" type="text">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
</div>

@include('footers.footer2')
@endsection

@section('footerscript')
<script>
    $(document).ready(function() {
        //INITIALIZE
        $('#form_page_create').addClass('loading');
        CKEDITOR.replace( 'page[content]', {
            toolbar: [
                { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                { name: 'styles', items: [ 'Styles', 'Format' ] },
                { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
                { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [ 'Image', 'EmbedSemantic', 'Table' ] },
                { name: 'tools', items: [ 'Maximize' ] },
                { name: 'editing', items: [ 'Scayt' ] }
            ],
            customConfig: '',
            extraPlugins: 'autoembed,embedsemantic,image2,uploadimage,uploadfile',
            removePlugins: 'image',
            uploadUrl : "{{ route('file.store', ['_token' => csrf_token()]) }}",
            imageUploadUrl : "{{ route('file.store', ['_token' => csrf_token(), 'type' => 'image']) }}",
            height: 461,
            format_tags: 'p;h1;h2;h3;pre',
            removeDialogTabs: 'image:advanced;link:advanced',
        });

        CKEDITOR.on("instanceReady",function() {
            $('#form_page_create').removeClass('loading');
        });

        //FORM
        $('#page_type').dropdown('set selected', 'page');
        $('#page_type').dropdown({
            onChange: function(value, text, $selectedItem) {
                $('#form_page_create').addClass('loading');
                //do something
                // change url
                // change header
                // change footer
                $('#form_page_create').removeClass('loading');
            }
        });

        $('#page_header').dropdown('set selected', 0);
        $('#page_footer').dropdown('set selected', 0);
        $('#page_imageslider').dropdown('set selected', 0);
        $('#page_runningtext').dropdown('set selected', 0);

        //ACCORDION
        $('.accordion')
            .accordion({
                selector: {
                    trigger: '.trigger'
                },
                onOpening: function() {
                    $(this).parent().find('.title').find('.trigger').find('.icon').removeClass('down');
                    $(this).parent().find('.title').find('.trigger').find('.icon').addClass('up');
                },
                onClosing: function() {
                    $(this).parent().find('.title').find('.trigger').find('.icon').removeClass('up');
                    $(this).parent().find('.title').find('.trigger').find('.icon').addClass('down');
                }
            })
        ;

        //FORM VALIDATION
        $('#form_page_create')
            .form({
                fields : {
                    page_judul: {
                        identifier : 'page[title]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    page_type: {
                        identifier : 'page[type]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    page_content: {
                        identifier : 'page[content]',
                        rules: [
                            {
                                type   : 'minLength[25]',
                                prompt : '{name} harus minimal {ruleValue}'
                            }
                        ]
                    },
                    page_url: {
                        identifier : 'page[url]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : 'Tolong masukkan url yang valid'
                            }
                        ]
                    }
                },
                inline : true,
                on     : 'blur'
            })
        ;
    });
</script>
@endsection
