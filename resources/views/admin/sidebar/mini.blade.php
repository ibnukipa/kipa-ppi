<div class="ui visible inverted sidebar vertical labeled icon menu" style="overflow-y: initial !important">
  <a href="{{ url('/') }}" class="item">
    <img class="ui mini image centered" src="{{ asset('storage/logo.png') }}">
  </a>
  <a href="{{ url('/admin/journal') }}" class="item {{ areActiveRoutes(['journal.*'], 'actived') }}">
    <i class="folder open icon"></i>
    Journal
  </a>
  <a href="{{ url('/admin/page') }}" class="item {{ areActiveRoutes(['page.*'], 'actived') }}">
    <i class="file icon"></i>
    Laman
  </a>
  <a href="{{ url('/admin/post') }}" class="item {{ areActiveRoutes(['post.*'], 'actived') }}">
    <i class="eyedropper icon"></i>
    Pos
  </a>
  <a href="{{ url('/admin/setting') }}" class="item {{ isActiveURL(LaravelLocalization::getCurrentLocale().'/admin/setting', 'actived') }}">
    <i class="setting icon"></i>
    Setting
  </a>
  <!--<div class="ui left pointing  dropdown link item">
    <i class="settings icon"></i>
    Settings
    <div class="menu" style="margin: 0 !important">
      <a class="item {{ isActiveURL(LaravelLocalization::getCurrentLocale().'/admin/menu', 'active') }}">
        <i class="cubes icon" style="margin: 0 !important"></i> Kategori
      </a>
      <a class="item {{ isActiveURL(LaravelLocalization::getCurrentLocale().'/admin/widget', 'active') }}">
        <i class="cubes icon" style="margin: 0 !important"></i> Media
      </a>
    </div>
  </div>-->
</div>