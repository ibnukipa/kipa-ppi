<div class="ui left visible sidebar vertical menu">
  <div class="item">
    <img class="ui mini image" src="{{ asset('storage/logo-teal-1024.png') }}">
  </div>
  <a class="item">
    <i class="gamepad icon"></i>
    Games
  </a>
  <a class="item active">
    <i class="video camera icon"></i>
    Channels
  </a>
  <a class="item">
    <i class="video play icon"></i>
    Videos
  </a>
  <a class="item">
    <i class="gamepad icon"></i>
    Games
  </a>
</div>