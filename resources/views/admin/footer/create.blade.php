@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {{ $page['name'] }}
        @php $page_name = $page['name'] @endphp
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {{ Request::route()->getAction()['page_name'] }}
        @php $page_name = Request::route()->getAction()['page_name'] @endphp
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui clearing padded segment basic no-margin roboto" style="background-color: #F9FAFB; padding-top: .5em; padding-bottom: .8em">
    <div class="ui breadcrumb right">
        <a class="section">Dashboard</a>
        <i class="right chevron icon divider"></i>
        <a class="section">Tampilan</a>
        <i class="right arrow icon divider"></i>
        <div class="active section">Footer</div>
    </div>
</div>
<div class="ui segment padded main basic no-margin" style="background: #f1f1f1">
    <div class="ui grid">
        <div class="ten wide column">
            <table id="pages" class="ui celled table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="five wide">Title</th>
                        <th class="five wide">Subject</th>
                        <th class="three wide">Kategori</th>
                        <th class="one wide">Tanggal</th>
                        <th class="two wide">Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="six wide column">
           <!--  <h3>Tambah Header</h3>
            <div class="ui divider"></div> -->
            <form id="form_footer_create" class="ui form" action="{{ route('appearance.footer.store') }}"  role="form" method="POST">
                {{ csrf_field() }}
                <div class="equal width fields">
                    <div class="field">
                        <div class="ui input">
                            <input name="footer[name]" placeholder="Nama" type="text">
                        </div>
                    </div>
                </div>

                <div class="equal width fields">
                    <div class="field">
                        <div class="ui  input">
                            <textarea name="footer[description]" placeholder="Deskripsi" type="text"></textarea>
                        </div>
                    </div>
                </div>

                <div class="fields">
                    <div class="equal width field">
                        <div class="ui selection dropdown costum" id="footer_mfooter">
                            <input name="footer[mfooter_id]" type="hidden">
                            <div class="default text">Template Footer</div>
                            <i class="dropdown icon"></i>
                            <div class="menu">
                             <div class="item">1</div>
                            </div>
                        </div>
                    </div>
                   <div class="equal width field">
                        <div class="ui selection dropdown costum" id="footer_menu">
                            <input name="footer[menu_id]" type="hidden">
                            <div class="default text">Menu</div>
                            <i class="dropdown icon"></i>
                            <div class="menu">
                               <div class="item">1</div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--<div class="ui accordion">
                    <a class="ui right labeled icon basic button borderradiusless trigger">
                        Forward
                        <i class="down chevron icon"></i>
                    </a>
                    <div class="content">
                        tes
                    </div> 
                </div>-->
                <div class="ui divider"></div>
                <div class="ui basic clearing segment no-padding no-margin center aligned">
                    <div class="ui buttons left floated">
                        <button type="submit" class="ui blue left labeled icon button"><i class="save icon"></i>Tambah</button>
                    </div>
                    <div class="ui small buttons">
                        <!--<a class="ui green button">Tambah</a>-->
                    </div>
                    <div class="ui buttons right floated middle">
                        <!--<button class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah pelanggan</button>-->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@include('footers.footer2')
@endsection

@section('footerscript')
<script>
    $(document).ready(function() {
        
        //FORM
        $('#footer_mfooter').dropdown('set selected', 'footer');
        $('#footer_mfooter').dropdown({
            onChange: function(value, text, $selectedItem) {
                $('#form_footer_create').addClass('loading');
                //do something
                // change url
                // change header
                // change footer
                $('#form_footer_create').removeClass('loading');
            }
        });
        $('#footer_menu').dropdown('set selected', 'footer');
        $('#footer_menu').dropdown({
            onChange: function(value, text, $selectedItem) {
                $('#form_footer_create').addClass('loading');
                //do something
                // change url
                // change header
                // change footer
                $('#form_footer_create').removeClass('loading');
            }
        });

        //FORM VALIDATION
        $('#form_footer_create')
            .form({
                fields : {
                    page_judul: {
                        identifier : 'footer[name]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    page_type: {
                        identifier : 'footer[description]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    page_content: {
                        identifier : 'footer[mhfooter_id]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    page_url: {
                        identifier : 'footer[menu_id]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    }
                },
                inline : true,
                on     : 'blur'
            })
        ;
    });
</script>
@endsection
