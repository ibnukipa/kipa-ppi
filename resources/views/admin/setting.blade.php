@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {{ $page['name'] }}
        @php $page_name = $page['name'] @endphp
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {{ Request::route()->getAction()['page_name'] }}
        @php $page_name = Request::route()->getAction()['page_name'] @endphp
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection

@section('extend_lib')
    <script src="{{ asset('libs/tinymce/tinymce.min.js') }}"></script>
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui clearing padded segment basic no-margin roboto" style="background-color: #F9FAFB; padding-top: .5em; padding-bottom: .8em">
    <div class="ui breadcrumb right">
        <a class="section">Dashboard</a>
        <i class="right chevron icon divider"></i>
        <a class="section">Setting</a>
    </div>
</div>
<div class="ui segment padded main basic no-margin" style="background: #f1f1f1">
    @if(session('alert') !=null)
    <div class="ui message @if(isset(session('alert')['color'])) {{session('alert')['color']}} @endif closeable">
        <p>{!! session('alert')['pesan'] !!}</p>
    </div>
    @endif
    <div class="ui grid">
        <div class="row no-padding-b">
            <div class="sixteen wide column">
                <div class="ui accordion field" style="background: white">
                    <div class="title no-padding">
                        <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                            <div  class="ui header no-margin">
                                <i class="quote right icon"></i>
                                <div class="content">
                                    Setting Header 
                                    <!-- <span style="font-size: .8em">(wajib)</span> -->
                                    <div class="sub header">Mengelola Header Utama</div>
                                </div>
                            </div >
                            <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                        </a>
                    </div>
                    <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                        <form class="ui form" action="{{ route('setting.header') }}"  role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="ui equal width fields">
                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i>Judul
                                                        <!-- <span style="font-size: .8em">(wajib)</span> -->
                                                    </i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="header[title]" value="{{ App\Setting::where('name','=','header')->first()->value }}" placeholder="Judul" type="text">
                                    </div>
                                </div>

                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i> Sub Judul
                                                        <!-- <span style="font-size: .8em">(wajib)</span> -->
                                                    </i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="header[subtitle]" value="{{ App\Setting::where('name','=','subheader')->first()->value }}" placeholder="Sub Judul" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="ui grid right aligned">
                                <div class="sixteen wide column">
                                    <button class="ui right labeled icon blue button">
                                        <i class="save icon"></i>
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row no-padding-b">
            <div class="sixteen wide column">
                <div class="ui accordion field" style="background: white">
                    <div class="title no-padding">
                        <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                            <div  class="ui header no-margin">
                                <i class="star icon"></i>
                                <div class="content">
                                    Setting Sosial Media
                                    <!-- <span style="font-size: .8em">(wajib)</span> -->
                                    <div class="sub header">Mengelola Sosial Media (kosongkan jika tidak ingin ditampilkan)</div>
                                </div>
                            </div >
                            <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                        </a>
                    </div>
                    <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                        <form class="ui form" action="{{ route('setting.sosmed') }}" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="ui three fields">
                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i>URL Facebook
                                                        <!-- <span style="font-size: .8em">(wajib)</span> -->
                                                    </i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="sosmed[facebook]" value="{{ App\Setting::where('name','=','sosmed_facebook')->first()->value }}" placeholder="URL Facebook" type="text">
                                    </div>
                                </div>

                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i>URL Twitter
                                                        <!-- <span style="font-size: .8em">(wajib)</span> -->
                                                    </i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="sosmed[twitter]" value="{{ App\Setting::where('name','=','sosmed_twitter')->first()->value }}" placeholder="URL Facebook" type="text">
                                    </div>
                                </div>

                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i>URL IPTEK</i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="sosmed[world]" value="{{ App\Setting::where('name','=','sosmed_world')->first()->value }}" placeholder="URL IPTEK" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="ui three fields">
                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i>URL Youtube</i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="sosmed[youtube]" value="{{ App\Setting::where('name','=','sosmed_youtube')->first()->value }}" placeholder="URL Youtube" type="text">
                                    </div>
                                </div>

                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i>URL Linkedin</i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="sosmed[linkedin]" value="{{ App\Setting::where('name','=','sosmed_linkedin')->first()->value }}" placeholder="URL Linkedin" type="text">
                                    </div>
                                </div>

                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i>URL Skype</i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="sosmed[skype]" value="{{ App\Setting::where('name','=','sosmed_skype')->first()->value }}" placeholder="URL Skype" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="ui grid right aligned">
                                <div class="sixteen wide column">
                                    <button class="ui right labeled icon blue button">
                                        <i class="save icon"></i>
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row no-padding-b">
            <div class="sixteen wide column">
                <div class="ui accordion field" style="background: white">
                    <div class="title no-padding">
                        <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                            <div  class="ui header no-margin">
                                <i class="wizard icon"></i>
                                <div class="content">
                                    Klasifikasi Journal
                                    <div class="sub header">Pengaturan kalsifikasi journal</div>
                                </div>
                            </div >
                            <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                        </a>
                    </div>
                    <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                        <form class="ui form" action="{{ route('setting.clasification') }}" role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="allmenuitem-sidebar ui basic segment no-padding-b">
                                @foreach(App\JournalClasification::get() as $key => $klasifikasi)
                                <div class="ui menuitem segment" style="padding: .5em; border: 1px dashed rgba(0, 0, 0, .2)">
                                    <div class="ui fields no-margin">
                                        <div class="ui five wide field">
                                            <label>
                                                <div  class="ui small header">
                                                    <div class="content">
                                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                            <i>Nama</i>
                                                        </label>
                                                    </div>
                                                </div >
                                            </label>
                                            <div class="ui input">
                                                <input name="klasifikasi[name][]" value="{{ $klasifikasi->name }}" placeholder="Nama" type="text">
                                            </div>
                                        </div>
                                        <div class="ui eight wide field">
                                            <label>
                                                <div  class="ui small header">
                                                    <div class="content">
                                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                            <i>Deskripsi</i>
                                                        </label>
                                                    </div>
                                                </div >
                                            </label>
                                            <div class="ui input">
                                                <input name="klasifikasi[description][]" value="{{ $klasifikasi->description }}" placeholder="Deskripsi" type="text">
                                            </div>
                                        </div>
                                        <div class="ui two wide field">
                                            <label>
                                                <div  class="ui small header">
                                                    <div class="content">
                                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                            <i>Prioritas</i>
                                                        </label>
                                                    </div>
                                                </div >
                                            </label>
                                            <div class="ui input">
                                                <input name="klasifikasi[priority][]" value="{{ $klasifikasi->priority }}" placeholder="Preoritas" type="number">
                                            </div>
                                        </div>
                                        <div class="ui one wide field">
                                            <label>
                                                <div  class="ui small header">
                                                    <div class="content">
                                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400; visibility: hidden">
                                                            <i>Aksi</i>
                                                        </label>
                                                    </div>
                                                </div >
                                            </label>
                                            <div class="ui icon buttons">
                                                <a onclick="deleteMenuItem(this)" class="ui button removeitem"><i class="trash icon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            
                            <div class="ui basic clearing segment no-margin center aligned">
                                <div class="ui buttons left floated">
                                    <a onclick="addItem('sidebar')" class="ui green left labeled icon button tambahitem-sidebar">
                                        <i class="add icon"></i>
                                        Tambah Item
                                    </a>
                                </div>
                                <div class="ui small buttons">
                                    <!--<a class="ui green button">Tambah</a>-->
                                </div>
                                <div class="ui buttons right floated middle">
                                    <button type="submit" class="ui blue left labeled icon button"><i class="save icon"></i>Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row no-padding-b">
            <div class="sixteen wide column">
                <div class="ui accordion field" style="background: white">
                    <div class="title no-padding">
                        <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                            <div  class="ui header no-margin">
                                <i class="user icon"></i>
                                <div class="content">
                                    Update Username Password
                                    <div class="sub header">Pembaruan email dan password untuk login Administrator</div>
                                </div>
                            </div >
                            <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                        </a>
                    </div>
                    <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                        <form class="ui form" action="{{ route('setting.updateusernamepassword') }}"  role="form" method="POST">
                            {{ csrf_field() }}
                            <div class="ui two fields">
                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i>Email/Username
                                                        <span style="font-size: .8em">(wajib)</span>
                                                    </i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="user[email]" value="{{ Auth::user()->email }}" placeholder="Email/username" type="text">
                                    </div>
                                </div>

                                <div class="ui field">
                                    <label>
                                        <div  class="ui small header">
                                            <div class="content">
                                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400">
                                                    <i> Password
                                                        <span style="font-size: .8em">(wajib)</span>
                                                    </i>
                                                </label>
                                            </div>
                                        </div >
                                    </label> 
                                    <div class="ui input">
                                        <input name="user[password]" value="" placeholder="(Tidak berubah)" type="password">
                                    </div>
                                </div>
                            </div>
                            <div class="ui grid right aligned">
                                <div class="sixteen wide column">
                                    <button class="ui right labeled icon blue button">
                                        <i class="save icon"></i>
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>    
</div>

@include('footers.footer2')
@endsection

@section('footerscript')
<script>
    $itemSiderBar = '<div class="ui menuitem segment" style="padding: .5em; border: 1px dashed rgba(0, 0, 0, .2)">\
                        <div class="ui fields no-margin">\
                            <div class="ui five wide field">\
                                <label>\
                                    <div  class="ui small header">\
                                        <div class="content">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400">\
                                                <i>Nama</i>\
                                            </label>\
                                        </div>\
                                    </div >\
                                </label>\
                                <div class="ui input">\
                                    <input name="klasifikasi[name][]" placeholder="Nama" type="text">\
                                </div>\
                            </div>\
                            <div class="ui eight wide field">\
                                <label>\
                                    <div  class="ui small header">\
                                        <div class="content">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400">\
                                                <i>Deskripsi</i>\
                                            </label>\
                                        </div>\
                                    </div >\
                                </label>\
                                <div class="ui input">\
                                    <input name="klasifikasi[description][]" placeholder="Deskripsi" type="text">\
                                </div>\
                            </div>\
                            <div class="ui two wide field">\
                                <label>\
                                    <div  class="ui small header">\
                                        <div class="content">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400">\
                                                <i>Prioritas</i>\
                                            </label>\
                                        </div>\
                                    </div >\
                                </label>\
                                <div class="ui input">\
                                    <input name="klasifikasi[priority][]" placeholder="Preoritas" type="number">\
                                </div>\
                            </div>\
                            <div class="ui one wide field">\
                                <label>\
                                    <div  class="ui small header">\
                                        <div class="content">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400">\
                                                <i>Aksi</i>\
                                            </label>\
                                        </div>\
                                    </div >\
                                </label>\
                                <div class="ui icon buttons">\
                                    <a onclick="deleteMenuItem(this)" class="ui button removeitem"><i class="trash icon"></i></a>\
                                </div>\
                            </div>\
                        </div>\
                    </div>';

    function addItem($whatitem) {
        if($whatitem == 'sidebar') {
            if($('.allmenuitem-sidebar').children('.itemkosong').length == 1)
                $('.allmenuitem-sidebar').children('.itemkosong').remove();

            $('.allmenuitem-sidebar').append($itemSiderBar);
        }
    }

    function deleteMenuItem($caller) {
        var $selectedMenuitem = $($caller).closest('.menuitem');
        $selectedMenuitem.remove();
        if($('.allmenuitem-sidebar').children().length < 1)
            $('.allmenuitem-sidebar').append('<div class="itemkosong" style="text-align: center"><i>Tidak ada klasifikasi</i></div>');
    }

    $(document).ready(function() {
        
        //ACCORDION
        $('.accordion')
            .accordion({
                selector: {
                    trigger: '.trigger'
                },
                onOpening: function() {
                    $(this).parent().find('.title').find('.trigger').find('.icon').removeClass('down');
                    $(this).parent().find('.title').find('.trigger').find('.icon').addClass('up');
                },
                onClosing: function() {
                    $(this).parent().find('.title').find('.trigger').find('.icon').removeClass('up');
                    $(this).parent().find('.title').find('.trigger').find('.icon').addClass('down');
                }
            })
        ; 
    });
</script>
@endsection
