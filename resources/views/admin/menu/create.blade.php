@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {{ $page['name'] }}
        @php $page_name = $page['name'] @endphp
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {{ Request::route()->getAction()['page_name'] }}
        @php $page_name = Request::route()->getAction()['page_name'] @endphp
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui clearing padded segment basic no-margin roboto" style="background-color: #F9FAFB; padding-top: .5em; padding-bottom: .8em">
    <div class="ui breadcrumb right">
        <a class="section">Dashboard</a>
        <i class="right chevron icon divider"></i>
        <a class="section">Tampilan</a>
        <i class="right arrow icon divider"></i>
        <div class="active section">Menu</div>
    </div>
</div>
<div class="ui segment padded main basic no-margin" style="background: #f1f1f1">
    <div class="ui grid">
        <div class="six wide column">
            <div class="ui accordion field" style="background: white">

                <!--LAMAN START-->
                <div class="title no-padding">
                    <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left; background-color: #fafafa !important">
                        <div  class="ui header no-margin">
                            <div class="content">
                                Laman
                            </div>
                        </div >
                        <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                    </a>
                </div>
                <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                    <form class="ui form">
                        <div class="field">
                            <label>Pilih halaman</label>
                            <div class="ui search selection dropdown drafdropdown">
                                <input name="draf_laman[id_laman]" type="hidden">
                                <div class="default text">Pilih laman...</div>
                                <i class="dropdown icon"></i>
                                <div class="menu" data-input="draf_laman[id_laman]">
                                    <div class="item" data-value="1" data-url="http://google.com">Home</div>
                                    <div class="item" data-value="2" data-url="http://google.com">Books</div>
                                    <div class="item" data-value="3" data-url="http://google.com">Online publishing</div>
                                    <div class="item" data-value="4" data-url="http://google.com">About us</div>
                                    <div class="item" data-value="5" data-url="http://google.com">Newsroom</div>
                                    <div class="item" data-value="6" data-url="http://google.com">Contact us</div>
                                </div>
                            </div>
                        </div>
                        <div class="ui basic clearing segment no-padding no-margin center aligned">
                            <div class="ui tiny buttons right floated middle">
                                <a href="#" onclick="toMenu(this)" class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah ke Menu</a>
                            </div>
                        </div>
                    </form>
                </div>
                <!--LAMAN END-->

                <!--JOURNAL START-->
                <div class="title no-padding">
                    <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left; background-color: #fafafa !important">
                        <div  class="ui header no-margin">
                            <div class="content">
                                Journal
                            </div>
                        </div >
                        <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                    </a>
                </div>
                <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                    <form class="ui form">
                        <div class="field">
                            <label>Pilih journal</label>
                            <div class="ui search selection dropdown drafdropdown">
                                <input name="draf_journal[id_journal]" type="hidden">
                                <div class="default text">Pilih journal...</div>
                                <i class="dropdown icon"></i>
                                <div class="menu" data-input="draf_journal[id_journal]">
                                    <div class="item" data-value="1" data-url="http://google.com">Journal Teknik</div>
                                    <div class="item" data-value="2" data-url="http://google.com">Journal Sains</div>
                                    <div class="item" data-value="3" data-url="http://google.com">Journal Lainnya</div>
                                </div>
                            </div>
                        </div>
                        <div class="ui basic clearing segment no-padding no-margin center aligned">
                            <div class="ui tiny buttons right floated middle">
                                <a href="#" onclick="toMenu(this)" class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah ke Menu</a>
                            </div>
                        </div>
                    </form>
                </div>
                <!--JOURNAL END-->

                <!--GUIDE START-->
                <div class="title no-padding">
                    <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left; background-color: #fafafa !important">
                        <div  class="ui header no-margin">
                            <div class="content">
                                Guide
                            </div>
                        </div >
                        <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                    </a>
                </div>
                <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                    <form class="ui form">
                        <div class="field">
                            <label>Pilih guide</label>
                            <div class="ui search selection dropdown drafdropdown">
                                <input name="draf_guide[id_guide]" type="hidden" data-url="">
                                <div class="default text">Pilih guide...</div>
                                <i class="dropdown icon"></i>
                                <div class="menu" data-input="draf_guide[id_guide]">
                                    <div class="item" data-value="1" data-url="http://google.com">Guide Author</div>
                                    <div class="item" data-value="2" data-url="http://google.com">Guide Editor</div>
                                    <div class="item" data-value="3" data-url="http://google.com">Guide Administrator</div>
                                </div>
                            </div>
                        </div>
                        <div class="ui basic clearing segment no-padding no-margin center aligned">
                            <div class="ui tiny buttons right floated middle">
                                <a href="#" onclick="toMenu(this)" class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah ke Menu</a>
                            </div>
                        </div>
                    </form>
                </div>
                <!--GUIDE END-->

                <!--URL START-->
                <div class="title no-padding">
                    <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left; background-color: #fafafa !important">
                        <div  class="ui header no-margin">
                            <div class="content">
                                Tautan
                            </div>
                        </div >
                        <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                    </a>
                </div>
                <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                    <form class="ui form">
                        <div class="field">
                            <label>URL</label>
                            <div class="ui input">
                                <input name="draf_url[url]" value="http://" type="text">
                            </div>
                        </div>
                        <div class="ui basic clearing segment no-padding no-margin center aligned">
                            <div class="ui tiny buttons right floated middle">
                                <a href="#" onclick="toMenu(this)" class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah ke Menu</a>
                            </div>
                        </div>
                    </form>
                </div>
                <!--URL END-->
            </div>
        </div>
        <div class="ten wide column">
            <form id="form_menu_create" class="ui form" action="{{ route('appearance.menu.store') }}"  role="form" method="POST">
                
                <h3 class="ui top attached small header borderradiusless" style="background-color: #fafafa !important">
                    {{ csrf_field() }}
                    <div class="ui fields">
                        <div class="ten wide field">
                            <label> <i>Nama Menu</i> </label>
                            <div class="ui input">
                                <input name="menu[name]" placeholder="Nama Menu" type="text">
                            </div>
                        </div>
                        <div class="six wide field">
                            <label>Pilih menu</label>
                            <div class="ui search selection dropdown costum">
                                <input name="menu[id_menu]" type="hidden">
                                <div class="default text">Pilih menu...</div>
                                <i class="dropdown icon"></i>
                                <div class="menu">
                                    <div class="item" data-value="0">Tambah Baru</div>
                                    <div class="item" data-value="1">Menu 1</div>
                                    <div class="item" data-value="2">Menu 2</div>
                                    <div class="item" data-value="3">Menu 3</div>
                                    <div class="item" data-value="4">Menu 4</div>
                                    <div class="item" data-value="5">Menu 5</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </h3>
                <div class="ui attached segment borderradiusless menustructer">
                    <h3 class="ui header">
                        Struktur Menu
                        <div class="sub header">Manage your account settings and set e-mail preferences.</div>
                    </h3>
                    <div class="allmenuitem">
                        <div class="emptyitem ui segment basic center aligned">
                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Pilihlah menu pada panel sebelah kiri.</i></label>
                        </div>
                        <!--<div class="ui menuitem segment" style="padding: .5em; border: 1px dashed rgba(0, 0, 0, .2)">
                            <div class="ui fields no-margin">
                                <div class="eight wide field">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Navigasi</i></label>
                                    <div class="ui input">
                                        <input name="menu[structure][label_nav][]" class="borderradiusless" placeholder="Nama Menu" type="text">
                                    </div>
                                </div>
                                <div class="two wide field">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Level</i></label> 
                                    <div class="ui input">
                                        <input name="menu[structure][level][]" class="borderradiusless" value="0" type="number">
                                    </div>
                                </div>
                                <div class="four wide field">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Pilih parent</i></label>
                                    <div class="ui selection dropdown">
                                        <input name="menu[structure][parent][]" type="hidden">
                                        <div class="default text">Pilih parent...</div>
                                        <i class="dropdown icon"></i>
                                        <div class="menu">
                                            <div class="item" data-value="0">Tambah Baru</div>
                                            <div class="item" data-value="1">Pos</div>
                                            <div class="item" data-value="2">Journal</div>
                                            <div class="item" data-value="3">Guide</div>
                                            <div class="item" data-value="4">Link</div>
                                            <div class="item" data-value="5">Tag</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="two wide field ki-right">
                                    <label style="color: rgba(0, 0, 0, 0); font-weight: 400"><i>Aksi</i></label> 
                                    <div class="ui icon buttons">
                                        <a onclick="deleteMenuItem(this)" class="ui button"><i class="trash icon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="ui fields no-margin">
                                <div class="sixteen wide field" style="margin-top: .5em">
                                    <span> source : <a href="#"><i>Google</i></a> </span>
                                </div>
                            </div>
                        </div>-->
                    </div>
                    <div class="ui divider"></div>
                    <div class="ui basic clearing segment no-padding no-margin center aligned">
                        <div class="ui buttons left floated">
                            <a href="#" class="ui red left labeled icon button"><i class="trash icon"></i>Hapus</a>
                            <div class="or"></div>
                            <button type="submit" class="ui green right labeled icon button"><i class="save icon"></i>Simpan</button>
                        </div>
                        <div class="ui small buttons">
                            
                        </div>
                        <div class="ui buttons right floated middle">
                            
                        </div>
                    </div>
                </div>
                <h3 class="ui bottom attached header borderradiusless" style="background-color: #fafafa !important">
                    
                </h3>
            </form>
        </div>
    </div>
</div>
@include('modal.confirm_delete')
@include('footers.footer2')
@endsection

@section('footerscript')
<script>
    //AKSI
    function buildItemMenu($label, $src, $IDmenuitem) {
        return '<div class="ui menuitem segment" style="padding: .5em; border: 1px dashed rgba(0, 0, 0, .2)">\
                            <input name="menu[structure][idmenuitem][]" type="hidden" value="'+$IDmenuitem+'">\
                            <div class="ui fields no-margin">\
                                <div class="eight wide field">\
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Navigasi</i></label>\
                                    <div class="ui input">\
                                        <input name="menu[structure][label_nav][]" class="borderradiusless" placeholder="Nama Menu" type="text" value="'+$label+'">\
                                    </div>\
                                </div>\
                                <div class="two wide field">\
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Level</i></label> \
                                    <div class="ui input">\
                                        <input onchange="cekParent(this)" name="menu[structure][level][]" class="borderradiusless" value="0" type="number" min="0">\
                                    </div>\
                                </div>\
                                <div class="four wide field">\
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Pilih parent</i></label>\
                                    <div class="ui search selection dropdown disabled">\
                                        <input name="menu[structure][parent][]" type="hidden">\
                                        <div class="default text">Pilih parent...</div>\
                                        <i class="dropdown icon"></i>\
                                        <div class="menu menuparent">\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="two wide field ki-right">\
                                    <label style="color: rgba(0, 0, 0, 0); font-weight: 400"><i>Aksi</i></label>\
                                    <div class="ui icon buttons">\
                                        <a onclick="deleteMenuItem(this)" class="ui button"><i class="trash icon"></i></a>\
                                    </div>\
                                </div>\
                            </div>\
                            <div class="ui fields no-margin">\
                                <div class="sixteen wide field" style="margin-top: .5em">\
                                    <span> source : <a target="_blank" href="'+$src+'"><i>'+$label+'</i></a> </span>\
                                </div>\
                            </div>\
                        </div>';
    }

    function getMenuItemCountWithLevel($level) {
        var intCount = 0;
        $.each($('.allmenuitem').children('.menuitem'), function(i, $cur) {
            $input = $($cur).find('input[name="menu[structure][level][]"]');
            if($($cur).find('input[name="menu[structure][level][]"]').val() == $level) {
                intCount++;
            }
                
        });
        
        return intCount;
    }

    function cekParent($curInput) {
        $menuitem       = $curInput.closest('.menuitem');
        $dropdown       = $($menuitem).find(".selection.dropdown");
        if($('.allmenuitem').children('.menuitem').length <= 1 || getMenuItemCountWithLevel(0) < 1) {
            alert("Menu dengan level 0 harus minimal 1 (satu)");
            $($curInput).val(0);
        } else {
            if($($curInput).val() <= 0 ) {
                $($dropdown).dropdown('clear refresh');
                $dropdown.addClass("disabled");
            } else {
                $($menuitem).find(".menuparent").html('');
                $.each($('.allmenuitem').children('.menuitem'), function(i, $cur) {
                    $input = $($cur).find('input[name="menu[structure][level][]"]');
                    if($($cur).find('input[name="menu[structure][level][]"]').val() == ($($curInput).val() - 1)) {
                        $($menuitem).find(".menuparent").append('<div class="item" data-value="'+$($cur).find('input[name="menu[structure][idmenuitem][]"]').val()+'">'+$($cur).find('input[name="menu[structure][label_nav][]"]').val()+'</div>')
                    }
                });
                $dropdown.removeClass("disabled");
            }
        }
    }

    var IDmenuitem = 1;
    function toMenu(curButton) {
        $(curButton).addClass('loading');
        
        if($('.allmenuitem').children().length != 0) {
            $('.emptyitem').addClass('none');
        } else {
            $('.emptyitem').removeClass('none');
        }

        var $formDraf = $(curButton).closest('form');
        var $inputs = $formDraf.serializeArray();

        var $labelNavMenu;
        var $sourceMenu;
        
        $.each($inputs, function(i, field) {
            var selectedInputName = 'input[name="'+field.name+'"';
            $curInput = $(selectedInputName);
            if($curInput.val() != '') {
                if($curInput.attr('type') == 'hidden') {
                    $dropdown       = $curInput.closest('.selection.dropdown');
                    $menuOption     = $curInput.closest('.menu');

                    $dropdownVal    = $dropdown.dropdown('get value');
                    $dropdownText   = $dropdown.dropdown('get text');
                    $labelNavMenu   = $dropdownText;
                    $sourceMenu     = $curInput.attr('data-url');

                } else {
                    $labelNavMenu   = "";
                    $sourceMenu = $curInput.val();
                }
            }
            
        });
        if($curInput.val() != '') {
            $('.allmenuitem').append(buildItemMenu($labelNavMenu, $sourceMenu, IDmenuitem));
            $('.dropdown').dropdown();
        } else {
            alert("Pilih terlebih dahulu!");
        }
        IDmenuitem++;
        $(curButton).removeClass('loading');
    }

    function deleteMenuItem(curDelButMenuitem) {
        var $menuItem = $(curDelButMenuitem).closest('.menuitem');
        $menuItem.remove();

        if($('.allmenuitem').children().length > 0) {
            $('.emptyitem').addClass('none');
        } else {
            $('.emptyitem').removeClass('none');
        }
        
    }

    $(document).ready(function() {
        //FORM
        $('input[name="menu[id_menu]"]').parent().dropdown('set selected', '0');
        $('input[name="menu[id_menu]"]').parent().dropdown({
            onChange: function(value, text, $selectedItem) {
                $('.menustructer').addClass('loading');
                $('.menustructer').removeClass('loading');
            }
        });

        

        $('.drafdropdown').dropdown({
            onChange: function(value, text, $selectedItem) {
                $dataUrl = $selectedItem.attr('data-url');
                $inputName = $selectedItem.closest('.menu').attr('data-input');

                var selectedInputName = 'input[name="'+$inputName+'"';
                $(selectedInputName).attr('data-url', $dataUrl);
                
            }
        });

        //ACCORDION
        $('.accordion')
            .accordion({
                selector: {
                    trigger: '.trigger'
                },
                onOpening: function() {
                    $(this).parent().find('.trigger').find('.icon').addClass('down');

                    $(this).prev().find('.trigger').find('.icon').removeClass('down');
                    $(this).prev().find('.trigger').find('.icon').addClass('up');
                },
                onClosing: function() {
                    
                    $(this).prev().find('.trigger').find('.icon').removeClass('up');
                    $(this).prev().find('.trigger').find('.icon').addClass('down');
                }
            })
        ;

        //FORM VALIDATION
        //
    });
</script>
@endsection