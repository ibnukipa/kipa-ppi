@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {{ $page['name'] }}
        @php $page_name = $page['name'] @endphp
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {{ Request::route()->getAction()['page_name'] }}
        @php $page_name = Request::route()->getAction()['page_name'] @endphp
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection

@section('extend_lib')
    <script src="{{ asset('libs/tinymce/tinymce.min.js') }}"></script>
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui clearing padded segment basic no-margin roboto" style="background-color: #F9FAFB; padding-top: .5em; padding-bottom: .8em">
    <div class="ui breadcrumb right">
        <a class="section">Dashboard</a>
        <i class="right chevron icon divider"></i>
        <a class="section">Laman</a>
        <i class="right arrow icon divider"></i>
        <div class="active section">Tambah</div>
    </div>
</div>
<div class="ui segment padded main basic no-margin" style="background: #f1f1f1">
    <form id="form_page_create" class="ui form" action="{{ route('page.store') }}" enctype="multipart/form-data" role="form" method="POST">
        {{ csrf_field() }}
        <div class="ui basic clearing segment no-padding no-margin center aligned">
            <div class="ui buttons left floated">
                <button type="submit" class="ui blue left labeled icon button"><i class="save icon"></i>Simpan</button>
            </div>
            <div class="ui small buttons">
                <!--<a class="ui green button">Tambah</a>-->
            </div>
            <div class="ui buttons right floated middle">
                <!--<button class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah pelanggan</button>-->
            </div>
        </div>
        <div class="ui divider"></div>
        @if(session('alert') !=null)
        <div class="ui message closeable">
            <!-- <div class="header">
                Terjadi kesalahan!
            </div> -->
            <p>{!! session('alert')['pesan'] !!}</p>
        </div>
        @endif

        <div class="ui top attached tabular menu">
            <div class="item active" data-tab="tab-id">Indonesia</div>
            <div class="item" data-tab="tab-en">English</div>
        </div>
        <div class="ui tab bottom attached segment active" data-tab="tab-id">
            <div class="equal width fields">
                <div class="field">
                    <div class="ui massive input">
                        <input name="page[title]" placeholder="Judul laman" type="text">
                    </div>
                </div>
            </div>
            <div class="equal width fields">
                <div class="field">
                    <div class="ui input">
                        <input name="page[subtitle]" placeholder="Deskripsi singkat laman" type="text">
                    </div>
                </div>
            </div>

            <div class="fields">
                <div class="four wide field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tampil pada Menu <span style="font-size: .8em">(wajib)</span></i></label>
                    <div class="ui selection dropdown costum" id="page_type">
                        <input name="page[type]" type="hidden">
                        <div class="default text">Tampilkan pada menu...</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="0">
                                Tidak pada Menu
                            </div>
                            @foreach($master['page_type'] as $key => $page_type)
                                <div class="item" data-value="{{ $page_type->id }}">
                                    {{ $page_type->description }}
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
                <div class="four wide field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Menu <span style="font-size: .8em">(optional)</span></i></label>
                    <div class="ui input">
                        <input name="page[menu_name]" placeholder="Label Menu" type="text">
                    </div>
                </div>
                <div class="four wide field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Laman Parent <span style="font-size: .8em">(wajib)</span></i></label>
                    <div class="ui selection dropdown costum" id="page_parent">
                        <input name="page[parent]" type="hidden">
                        <div class="default text">Pilih laman parent...</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item" data-value="0">
                                Tidak ada parent
                            </div>
                            @foreach($master['page_parent'] as $key => $cur_page_parent)
                                <div class="item" data-value="{{ $cur_page_parent->id }}">
                                    {{ $cur_page_parent->title }}
                                </div>
                            @endforeach
                        </div>
                    </div> 
                </div>
                <div class="eight wide field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>URL halaman <span style="font-size: .8em">(wajib)</span></i></label>
                    <div class="ui right labeled input">
                        <div class="ui label urllabel">
                            {{ url('/') }}/
                        </div>
                        <input name="page[url]" placeholder="Link..." type="text" id="urlPage">
                        <div class="ui label">
                            /
                        </div>
                    </div>
                </div>
            </div>

            <div class="equal width fields">
                <div class="field">
                    <textarea name="page[content]" placeholder="Content laman"></textarea>
                </div>
            </div>

            <div class="ui grid">
                <div class="equal width row">
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tags <span style="font-size: .8em">(optional)</span></i></label>
                                <select name="page[tags][]" class="ui fluid search dropdown costum" multiple="" id="page_tags">
                                    <option value="">Masukkan Tags Laman...</option>
                                    @foreach($master['page_tag'] as $key => $tag)
                                        <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="equal width row">
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[visible]" type="checkbox">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Nonaktifkan Laman</i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[menu_visible]" type="checkbox">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Jangan ditampilkan pada Menu</i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[menu_clickable]" type="checkbox">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tidak bisa di-click pada menu</i></label>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[siderbar_recent_journal_bool]" type="checkbox">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tampilkan "Recent Journal"</i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[siderbar_recent_article_bool]" type="checkbox">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tampilkan "Recent Articles"</i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Creative Commons License -->
                <div class="row no-padding">
                    <div class="sixteen wide column">
                        <div class="ui accordion field" style="background: white">
                            <div class="title no-padding">
                                <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                    <div  class="ui header no-margin">
                                        <i class="creative commons icon"></i>
                                        <div class="content">
                                            Creative Commons License
                                            <div class="sub header">Creative Commons Attribution-ShareAlike 4.0 International License</div>
                                        </div>
                                    </div >
                                    <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                                </a>
                            </div>
                            <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                                <div class="equal width fields">
                                    <div class="field">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Normal Icon*</i></label>
                                        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>
                                    </div> 

                                    <div class="field">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Compact Icon*</i></label>
                                        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
                                    </div>
                                </div>
                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>*Copy ke text editor</i></label>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SIDEBAR -->
                <div class="row no-padding-b">
                    <div class="sixteen wide column">
                        <div class="ui accordion field" style="background: white">
                            <div class="title no-padding">
                                <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                    <div  class="ui header no-margin">
                                        <i class="wizard icon"></i>
                                        <div class="content">
                                            Sidebar <span style="font-size: .8em">(optional)</span>
                                            <div class="sub header">Pengaturan pada Sidebar halaman</div>
                                        </div>
                                    </div >
                                    <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                                </a>
                            </div>
                            <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                                
                                <div class="field">
                                    <div class="ui toggle checkbox usesidebar">
                                        <input name="page[sidebar_visible]" type="checkbox">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Gunakan Sidebar</i></label>
                                    </div>
                                </div>
                                
                                <div class="allmenuitem-sidebar ui basic segment no-padding-b">
                                </div>

                                <div class="ui segment basic aligned no-padding-t">
                                    <a onclick="addItem('sidebar')" class="ui compact green button tambahitem-sidebar">
                                        Tambah Item
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- IMAGE SLIDER -->
                <div class="row no-padding-b">
                    <div class="sixteen wide column">
                        <div class="ui accordion field" style="background: white">
                            <div class="title no-padding">
                                <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                    <div  class="ui header no-margin">
                                        <i class="image icon"></i>
                                        <div class="content">
                                            Image Slider <span style="font-size: .8em">(optional)</span>
                                            <div class="sub header">Pengaturan untuk Image Slider Laman</div>
                                        </div>
                                    </div >
                                    <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                                </a>
                            </div>
                            <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                                <div class="ui toggle checkbox imageslider">
                                    <input name="page[imageslider_visible]" type="checkbox">
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Gunakan Image Slider</i></label>
                                </div>
                                <div class="fields allmenuitem-imageslider ui basic segment no-padding-b" style="margin-top: 2em">
                                </div>

                                <div class="ui segment basic aligned no-padding-t">
                                    <a onclick="addItem('imageslider')" class="ui compact green button tambahitem-imageslider">
                                        Tambah Item
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- RUNNING TEXT -->
                <div class="row">
                    <div class="sixteen wide column">
                        <div class="ui accordion field" style="background: white">
                            <div class="title no-padding">
                                <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                    <div  class="ui header no-margin">
                                        <i class="text width icon"></i>
                                        <div class="content">
                                            Running Text <span style="font-size: .8em">(optional)</span>
                                            <div class="sub header">Pengaturan untuk Running Text Laman</div>
                                        </div>
                                    </div >
                                    <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                                </a>
                            </div>
                            <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                                <div class="field">
                                    <div class="ui toggle checkbox userunningtext">
                                        <input name="page[textrunning_visible]" type="checkbox">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Gunakan Running Text</i></label>
                                    </div>
                                    <div class="fields allmenuitem-runningtext" style="margin-top: 2em">
                                        <div class="eight wide field">
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Content Running Text</i></label>
                                            <div class="ui input">
                                                <input name="page[runningtext][content]" placeholder="Running text content..." type="text">
                                            </div>
                                        </div>
                                        <div class="four wide field">
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Kecepatan (detik)</i></label>
                                            <div class="ui input">
                                                <input name="page[runningtext][speed]" value="20" type="number">
                                            </div>
                                        </div>
                                        <div class="four wide field">
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Warna</i></label>
                                            <div class="ui selection dropdown">
                                                <input name="page[runningtext][color]" type="hidden">
                                                <div class="default text">Pilih warna...</div>
                                                <i class="dropdown icon"></i>
                                                <div class="menu">
                                                    <div class="item" data-value="normal">
                                                        Normal
                                                    </div>
                                                    <div class="item" data-value="red">
                                                        Merah
                                                    </div>
                                                    <div class="item" data-value="blue">
                                                        Biru
                                                    </div>
                                                    <div class="item" data-value="peach">
                                                        Peach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui tab bottom attached segment" data-tab="tab-en">
            <div class="equal width fields">
                <div class="field">
                    <div class="ui massive input">
                        <input name="page[title_en]" placeholder="Judul laman [english]" type="text">
                    </div>
                </div>
            </div>
            <div class="equal width fields">
                <div class="field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Deskripsi Singkat Laman [english] <span style="font-size: .8em">(optional)</span></i></label>
                    <div class="ui input">
                        <input name="page[subtitle_en]" placeholder="Deskripsi singkat laman [english]" type="text">
                    </div>
                </div>
                <div class="four wide field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Menu [english] <span style="font-size: .8em">(optional)</span></i></label>
                    <div class="ui input">
                        <input name="page[menu_name_en]" placeholder="Label menu [english]" type="text">
                    </div>
                </div>
            </div>
            <div class="equal width fields">
                <div class="field">
                    <textarea name="page[content_en]" placeholder="Content laman [english]"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="sixteen wide column">
                    <div class="ui accordion field" style="background: white">
                        <div class="title no-padding">
                            <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                <div  class="ui header no-margin">
                                    <i class="text width icon"></i>
                                    <div class="content">
                                        Running Text [english] <span style="font-size: .8em">(optional)</span>
                                        <div class="sub header">Pengaturan untuk Running Text Laman</div>
                                    </div>
                                </div >
                                <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                            </a>
                        </div>
                        <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                            <div class="field">
                                <div class="fields allmenuitem-runningtext" style="margin-top: 2em">
                                    <div class="ten wide field">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Content Running Text [english]</i></label>
                                        <div class="ui input">
                                            <input name="page[runningtext][content_en]" placeholder="Running text content [english]..." type="text">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
</div>

@include('footers.footer2')
    <!-- <script src="{{ asset('libs/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('libs/dropzone/mydropzone.js') }}"></script> -->
@endsection

@section('footerscript')
<script>
    $itemImageSlider = '<div class="eight wide field menuitem">\
                                    <div class="ui action actionfile input">\
                                        <input type="text" placeholder="Image" readonly>\
                                        <input type="file" name="imageslider[]" style="display:none" accept="image/*">\
                                        <div class="ui icon button">\
                                            <i class="attach icon"></i>\
                                        </div>\
                                        <a onclick="deleteMenuItem(this)" class="ui icon red button removeitem"><i class="trash icon"></i></a>\
                                    </div>\
                                    <img width="100%" src="{{ url("/images/coverjournal/notfound") }}" alt="Default Cover" />\
                                </div>';
    $itemSiderBar = '<div class="ui menuitem segment" style="padding: .5em; border: 1px dashed rgba(0, 0, 0, .2)">\
                                    <div class="ui fields no-margin">\
                                        <div class="five wide field">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Navigasi</i></label>\
                                            <div class="ui input">\
                                                <input name="page[sidebar][label][]" placeholder="Nama Menu" type="text">\
                                            </div>\
                                        </div>\
                                        <div class="ten wide field">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Pilih link atau input manual</i></label>\
                                            <div class="ui search selection dropdown siderbar costum">\
                                                <input name="page[sidebar][url][]" type="hidden">\
                                                <div class="default text">Link...</div>\
                                                <i class="dropdown icon"></i>\
                                                <div class="menu">'+
                                                    @if($master['page_all'])
                                                        <?php
                                                            $groupedPageByTypeID = $master['page_all']->where('status', '=', 'publish')->groupBy('page_type_id');
                                                        ?>
                                                        @foreach($groupedPageByTypeID as $key => $cur_page_col)
                                                            @if($cur_page_col[0]->page_type_id == 0)
                                                                '<div class="header">\
                                                                    <span style="font-size: 1.3em; color: rgba(0, 0, 0, .4)">Tidak pada Menu</span>\
                                                                </div>'+
                                                            @else
                                                                '<div class="header">\
                                                                    <span style="font-size: 1.3em; color: rgba(0, 0, 0, .4)"> {!! App\PageType::find($cur_page_col[0]->page_type_id)->description !!}</span>\
                                                                </div>'+
                                                            @endif
                                                            @foreach($cur_page_col as $key2 => $cur_page)
                                                                '<div class="item" style="margin-left: 1em" data-value="'+'{{$cur_page->url}}'+'">'+
                                                                    '{{ $cur_page->title }}'+'<span style="color: rgba(0, 0, 0, .2)"> ('+'{{ url("/") }}/{{$cur_page->url}})</span>\
                                                                </div>'+
                                                            @endforeach
                                                        @endforeach
                                                    @endif
                                                '</div>\
                                            </div>\
                                        </div>\
                                        <div class="one wide field ki-right">\
                                            <label style="color: rgba(0, 0, 0, 0); font-weight: 400"><i>Aksi</i></label> \
                                            <div class="ui icon buttons">\
                                                <a onclick="deleteMenuItem(this)" class="ui button removeitem"><i class="trash icon"></i></a>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>';

    function addItem($whatitem) {
        if($whatitem == 'sidebar') {
            $('.allmenuitem-sidebar').append($itemSiderBar);
            if($('.checkbox.usesidebar').checkbox('is checked')) {
                enabledItemSidebar();
            } else {
                disabledItemSidebar();
            }

            $('.ui.dropdown.siderbar').dropdown({
                allowAdditions: true
            });

        } else if($whatitem == 'imageslider') {
            $('.allmenuitem-imageslider').append($itemImageSlider);
            if($('.checkbox.imageslider').checkbox('is checked')) {
                enabledItemImageSlider();
            } else {
                disabledItemImageSlider();
            }

            $('input:text', '.ui.actionfile.input').click(function() {
                $(this).parent().find("input:file").click();
            });
            $('div.button', '.ui.actionfile.input').click(function() {
                $(this).parent().find("input:file").click();
            });

            $('input:file', '.ui.actionfile.input').on('change', function(e) {
                if(e.target.files[0]) {
                    var name = e.target.files[0].name;
                    $('input:text', $(e.target).parent()).val(name);
                    readURL(this);
                }
            });
        }
    }

    function disabledItemSidebar() {
        $('.allmenuitem-sidebar').addClass('disabled');
        $('.allmenuitem-sidebar').find('.ui.input').addClass('disabled');
        $('.allmenuitem-sidebar').find('.dropdown').addClass('disabled');
        $('.allmenuitem-sidebar').find('.button.removeitem').addClass('disabled');
        $('.button.tambahitem-sidebar').addClass('disabled');
    }

    function enabledItemSidebar() {
        $('.allmenuitem-sidebar').removeClass('disabled');
        $('.allmenuitem-sidebar').find('.ui.input').removeClass('disabled');
        $('.allmenuitem-sidebar').find('.dropdown').removeClass('disabled');
        $('.allmenuitem-sidebar').find('.button.removeitem').removeClass('disabled');
        $('.button.tambahitem-sidebar').removeClass('disabled');
    }

    function disabledItemRunningtext() {
        $('.allmenuitem-runningtext').addClass('disabled');
        $('.allmenuitem-runningtext').find('.ui.input').addClass('disabled');
    }

    function enabledItemRunningtext() {
        $('.allmenuitem-runningtext').removeClass('disabled');
        $('.allmenuitem-runningtext').find('.ui.input').removeClass('disabled');
    }

    function disabledItemImageSlider() {
        $('.allmenuitem-imageslider').addClass('disabled');
        $('.allmenuitem-imageslider').find('.ui.input').addClass('disabled');
        $('.allmenuitem-imageslider').find('.button.removeitem').addClass('disabled');
        $('.button.tambahitem-imageslider').addClass('disabled');
    }

    function enabledItemImageSlider() {
        $('.allmenuitem-imageslider').removeClass('disabled');
        $('.allmenuitem-imageslider').find('.ui.input').removeClass('disabled');
        $('.allmenuitem-imageslider').find('.button.removeitem').removeClass('disabled');
        $('.button.tambahitem-imageslider').removeClass('disabled');
    }

    function deleteMenuItem($caller) {
        var $selectedMenuitem = $($caller).closest('.menuitem');
        $selectedMenuitem.remove();
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var tesPare = $(input).parent().parent();
                var img = $(input).parent().parent().find('img');
                $(img).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function() {
        $('.tabular.menu .item').tab();
        //INITIALIZE
        if($('.checkbox.userunningtext').checkbox('is checked')) enabledItemRunningtext();
        else disabledItemRunningtext();

        if($('.checkbox.usesidebar').checkbox('is checked')) enabledItemSidebar();
        else disabledItemSidebar();

        if($('.checkbox.imageslider').checkbox('is checked')) enabledItemImageSlider();
        else disabledItemImageSlider();

        addItem('sidebar');
        addItem('imageslider');
        $('#form_page_create').addClass('loading');
        tinymce.init({
            selector     : "textarea",
            height: 500,
            theme: 'modern',
            plugins      : [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help',
                "insertdatetime media table contextmenu paste responsivefilemanager"
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab : true,
            setup: function(editor) {
                editor.on('init', function() {
                    $('#form_page_create').removeClass('loading');
                });
            },
            relative_urls: false,
            external_filemanager_path:"{!! str_finish(asset('libs/filemanager'),'/') !!}",
            filemanager_title        :"File Manager" ,
            external_plugins         : { "filemanager" : "{{ asset('libs/filemanager/plugin.min.js') }}"} 
        }); 

        //FORM
        $('.checkbox.usesidebar').checkbox({
            onChecked: function() {
                enabledItemSidebar();
            },
            onUnchecked: function() {
                disabledItemSidebar();
            }
        });

        $('.checkbox.imageslider').checkbox({
            onChecked: function() {
                enabledItemImageSlider();
            },
            onUnchecked: function() {
                disabledItemImageSlider();
            }
        });

        $('.checkbox.userunningtext').checkbox({
            onChecked: function() {
                enabledItemRunningtext();
            },
            onUnchecked: function() {
                disabledItemRunningtext();
            }
        });

        //PAGE TITLE
        $('input[name="page[title]"]').on('input', function() {
            $('input[name="page[url]"]').val($('input[name="page[title]"]').val().toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-'));
        });

        //PAGE URL
        $('input[name="page[url]"]').on('input', function() {
            $('input[name="page[url]"]').val($('input[name="page[url]"]').val().toLowerCase().replace(/[^\w-]+/g,''));
        });

        $('input[name="page[title]"]').on('change', function() {
            $('input[name="page[url]"]').trigger('change');
        });

        $('input[name="page[url]"]').on('change', function() {
           
            $.ajax({
                url: '{{ url("admin/api/page/urlisexsist") }}/'+$('input[name="page[url]"]').val(),
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    if(data == '1') {
                        alert("URL Laman {{ url('/') }}/"+$('input[name="page[url]"]').val()+"/ SUDAH terpakai. Mohon gunakan URL Laman yang lain");
                        $('input[name="page[url]"]').val('');
                    }
                }
            });
        });

        //PAGE TYPE
        $('#page_type').dropdown('set selected', '1');
        $('#page_parent').dropdown('set selected', '0');
        $('#page_tags').dropdown({
            allowAdditions: true
        });

        //ACCORDION
        $('.accordion')
            .accordion({
                selector: {
                    trigger: '.trigger'
                },
                onOpening: function() {
                    $(this).parent().find('.title').find('.trigger').find('.icon').removeClass('down');
                    $(this).parent().find('.title').find('.trigger').find('.icon').addClass('up');
                },
                onClosing: function() {
                    $(this).parent().find('.title').find('.trigger').find('.icon').removeClass('up');
                    $(this).parent().find('.title').find('.trigger').find('.icon').addClass('down');
                }
            })
        ;

        //FORM VALIDATION
        $('#form_page_create')
            .form({
                fields : {
                    page_judul: {
                        identifier : 'page[title]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    page_type: {
                        identifier : 'page[type]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    // page_content: {
                    //     identifier : 'page[content]',
                    //     rules: [
                    //         {
                    //             type   : 'minLength[25]',
                    //             prompt : '{name} harus minimal {ruleValue}'
                    //         }
                    //     ]
                    // },
                    // page_url: {
                    //     identifier : 'page[url]',
                    //     rules: [
                    //         {
                    //             type   : 'empty',
                    //             prompt : 'Tolong masukkan url yang valid'
                    //         }
                    //     ]
                    // }
                },
                inline : true,
                on     : 'blur'
            })
        ;
    });
</script>
@endsection
