@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {!! $page_name = $page['name'] !!}
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {!! $page_name = Request::route()->getAction()['page_name'] !!}
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui clearing padded segment basic no-margin" style="background-color: #F9FAFB; padding-top: .5em; padding-bottom: .8em">
    <div class="ui breadcrumb right">
        <a class="section">Dashboard</a>
        <i class="right chevron icon divider"></i>
        <a class="section">Halaman</a>
        <i class="right arrow icon divider"></i>
        <div class="active section">Semua</div>
    </div>
</div>
<div class="ui segment padded main basic no-margin">
    <div class="ui basic clearing segment no-padding center aligned">
        <div class="ui small buttons right floated">
            <!--<button class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah pelanggan</button>-->
        </div>
        <div class="ui small buttons">
            <!--<button class="ui green button">Tambah</button>-->
        </div>
        <div class="ui small buttons left floated">
            <a href="{{ url('admin/page/create') }}" class="ui green left labeled icon button"><i class="add arrow icon"></i>Tambah</a>
        </div>
    </div>
    <div class="ui divider"></div>
    @if(session('alert') !=null)
    <div class="ui message closeable">
        <!-- <div class="header">
            Terjadi kesalahan!
        </div> -->
        <p>{!! session('alert')['pesan'] !!}</p>
    </div>
    @endif
    <table id="pages" class="ui celled table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="four wide">Title</th>
                <th class="four wide">URL</th>
                <th class="two wide">Label</th>
                <th class="two wide">Posisi</th>
                <th class="two wide">Terbuat</th>
                <th class="two wide">Tindakan</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@include('modal.confirm_delete')
@include('footers.footer2')
@endsection

@section('footerscript')
<script>
    $(document).ready(function() {
        $('#pages').DataTable({
            select: true,
            "oLanguage": {
                "sSearch": "Cari data {{ $page_name }}:",
                "sZeroRecords": "<i>Data {{ $page_name }} kosong</i>",
                "sInfoEmpty": "",
                "oPaginate": {
                    "sNext": "Selanjutnya",
                    "sPrevious": "Sebelumnya",
                    "sFirst": "Pertama",
                    "sLast": "Terakhir",
                },
                "sInfo": "Total _TOTAL_ data {{ $page_name }}",
                "sLengthMenu": '<select>'+
                                '<option value="10">10</option>'+
                                '<option value="50">50</option>'+
                                '<option value="-1">All</option>'+
                                '</select> data {{ $page_name }}'
            },
            processing: true,
            serverSide: true,
            buttons: [
                'csv', 'excel', 'pdf', 'print', 'reset', 'reload'
            ],
            ajax: "{{ url('/datatable/page/all?with=') }}",
            columns: [
                {data: 'title'},
                {data: 'url'},
                {data: 'menu-name'},
                {data: 'menu-position'},
                {data: 'created_at'},
                {data: 'action', orderable: false, searchable: false},
            ],
            columnDefs: [
                { className: "center aligned", "targets": [ 5 ] }
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
            }
        });
    } );
</script>
@endsection
