@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {{ $page['name'] }}
        @php $page_name = $page['name'] @endphp
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {{ Request::route()->getAction()['page_name'] }}
        @php $page_name = Request::route()->getAction()['page_name'] @endphp
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection

@section('extend_lib')
    
    <script src="{{ asset('libs/tinymce/tinymce.min.js') }}"></script>
    
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui clearing padded segment basic no-margin roboto" style="background-color: #F9FAFB; padding-top: .5em; padding-bottom: .8em">
    <div class="ui breadcrumb right">
        <a class="section">Dashboard</a>
        <i class="right chevron icon divider"></i>
        <a class="section">Artikel</a>
        <i class="right arrow icon divider"></i>
        <div class="active section">Edit</div>
    </div>
</div>
<div class="ui segment padded main basic no-margin" style="background: #f1f1f1">
    <form id="form_page_create" class="ui form" action="{{ route('post.update') }}" enctype="multipart/form-data" role="form" method="POST">
        {{ csrf_field() }}
        <div class="ui basic clearing segment no-padding no-margin center aligned">
            <div class="ui buttons left floated">
                <button type="submit" class="ui blue left labeled icon button"><i class="save icon"></i>Simpan</button>
            </div>
            <div class="ui small buttons">
                <!--<a class="ui green button">Tambah</a>-->
            </div>
            <div class="ui buttons right floated middle">
                <!--<button class="ui green right labeled icon button"><i class="add arrow icon"></i>Tambah pelanggan</button>-->
            </div>
        </div>
        <div class="ui divider"></div>
        @if(session('alert') !=null)
        <div class="ui message closeable">
            <p>{!! session('alert')['pesan'] !!}</p>
        </div>
        @endif

        <div class="ui top attached tabular menu">
            <div class="item active" data-tab="tab-id">Indonesia</div>
            <div class="item" data-tab="tab-en">English</div>
        </div>

        <div class="ui tab bottom attached segment active" data-tab="tab-id">
            <div class="equal width fields">
                <div class="field">
                    <div class="ui massive input">
                        <input name="page[id]" value="{{$page->id}}" type="hidden">
                        <input name="page[title]" value="{{$page->title}}" placeholder="Judul Artikel" type="text">
                    </div>
                </div>
            </div>
            <div class="equal width fields">
                <div class="field">
                    <div class="ui input">
                        <input name="page[subtitle]" value="{{$page->subtitle}}" placeholder="Deskripsi Singkat Artikel" type="text">
                    </div>
                </div>
            </div>
            <div class="fields">
                <div class="four wide field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Menu <span style="font-size: .8em">(optional)</span></i></label>
                    <div class="ui input">
                        <input name="page[menu_name]" placeholder="Label Menu" value="{{ $page->menu_name }}" type="text">
                    </div>
                </div>

                <div class="eight wide field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>URL Artikel <span style="font-size: .8em">(wajib)</span></i></label>
                    <div class="ui right labeled input">
                        <div class="ui label urllabel">
                            {{ url('/') }}/
                        </div>
                        <input name="page[url]" placeholder="Link..." value="{{ $page->url }}" type="text" id="urlPage">
                        <div class="ui label">
                            /
                        </div>
                    </div>
                </div>
            </div>

            <div class="equal width fields">
                <div class="field">
                    <textarea name="page[content]" placeholder="Content Artikel">{{ $page->content }}</textarea>
                </div>
            </div>

            <div class="ui grid">
                <div class="equal width row">
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tags</i></label>
                                <select name="page[tags][]" class="ui fluid search dropdown costum" multiple="" id="page_tags">
                                    <option value="">Masukkan Tags Artikel...</option>
                                    @foreach($master['page_tag'] as $key => $tag)
                                        <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="equal width row">
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[visible]" type="checkbox" @if(!$page->visible) checked @endif>
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Nonaktifkan Laman Artikel</i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[image_cover_landscape_bool]" type="checkbox" @if($page->image_cover_landscape_bool) checked @endif>
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tampilkan Cover</i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[siderbar_recent_journal_bool]" type="checkbox" @if($page->siderbar_recent_journal_bool) checked @endif>
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tampilkan "Recent Journal"</i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="field">
                                <div class="ui toggle checkbox">
                                    <input name="page[siderbar_recent_article_bool]" type="checkbox" @if($page->siderbar_recent_article_bool) checked @endif>
                                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Tampilkan "Recent Articles"</i></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Creative Commons License -->
                <div class="row no-padding">
                    <div class="sixteen wide column">
                        <div class="ui accordion field" style="background: white">
                            <div class="title no-padding">
                                <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                    <div  class="ui header no-margin">
                                        <i class="creative commons icon"></i>
                                        <div class="content">
                                            Creative Commons License
                                            <div class="sub header">Creative Commons Attribution-ShareAlike 4.0 International License</div>
                                        </div>
                                    </div >
                                    <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                                </a>
                            </div>
                            <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                                <div class="equal width fields">
                                    <div class="field">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Normal Icon*</i></label>
                                        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>
                                    </div> 

                                    <div class="field">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Compact Icon*</i></label>
                                        <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
                                    </div>
                                </div>
                                <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>*Copy ke text editor</i></label>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Cover dan Thumbnail -->
                <div class="row no-padding-b">
                    <div class="sixteen wide column">
                        <div class="ui accordion field" style="background: white">
                            <div class="title no-padding">
                                <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                    <div  class="ui header no-margin">
                                        <i class="image icon"></i>
                                        <div class="content">
                                            Cover dan Thumbnail Artikel
                                            <div class="sub header">Pengaturan untuk Cover dan Thumbnail Artikel</div>
                                        </div>
                                    </div >
                                    <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                                </a>
                            </div>
                            <div class="content field" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                                <div class="fields">
                                    <div class="ten wide field">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Cover Artikel Landscape</i></label>
                                        <div class="ui action actionfile input">
                                            <input type="text" placeholder="Cover Artikel Landscape" readonly>
                                            <input type="file" name="image_cover_landscape" style="display:none" accept="image/*">
                                            <div class="ui icon button">
                                                <i class="attach icon"></i>
                                            </div>
                                        </div>
                                        @if($page->image_cover_landscape != '' && $page->image_cover_landscape != NULL)
                                            <img width="100%" src="{{ url('/images/'.$page->image_cover_landscape) }}" alt="Default Cover" />
                                        @else 
                                            <img width="100%" src="{{ url('/images/coverjournal/imagenotfoundportrait.png?orientation=landscape') }}" alt="Default Cover" />
                                        @endif
                                    </div> 

                                    <div class="six wide field">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Cover Artikel Potrait</i></label>
                                        <div class="ui action actionfile input">
                                            <input type="text" placeholder="Cover Artikel Potrait" readonly>
                                            <input type="file" name="image_cover_potrait" style="display:none" accept="image/*">
                                            <div class="ui icon button">
                                                <i class="attach icon"></i>
                                            </div>
                                        </div>
                                        @if($page->image_cover_landscape != '' && $page->image_cover_landscape != NULL)
                                            <img width="100%" src="{{ url('/images/'.$page->image_cover_potrait) }}" alt="Default Cover" />
                                        @else 
                                            <img width="100%" src="{{ url('/images/coverjournal/imagenotfoundportrait.png?orientation=potrait') }}" alt="Default Cover" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 

                <!-- SIDEBAR -->
                <div class="row no-padding-b">
                    <div class="sixteen wide column">
                        <div class="ui accordion field" style="background: white">
                            <div class="title no-padding">
                                <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                    <div  class="ui header no-margin">
                                        <i class="wizard icon"></i>
                                        <div class="content">
                                            Sidebar
                                            <div class="sub header">Pengaturan pada Sidebar Laman Artikel</div>
                                        </div>
                                    </div >
                                    <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                                </a>
                            </div>
                            <div class="content field @if($page->sidebars()->count()) active @endif" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                                
                                <div class="field">
                                    <div class="ui toggle checkbox usesidebar">
                                        <input name="page[sidebar_visible]" type="checkbox">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Gunakan Sidebar</i></label>
                                    </div>
                                </div>
                                
                                <div class="allmenuitem-sidebar ui basic segment no-padding-b">
                                </div>

                                <div class="ui segment basic aligned no-padding-t">
                                    <a onclick="addItem('sidebar')" class="ui compact green button tambahitem-sidebar">
                                        Tambah Item
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- RUNNING TEXT -->
                <div class="row">
                    <div class="sixteen wide column">
                        <div class="ui accordion field" style="background: white">
                            <div class="title no-padding">
                                <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                    <div  class="ui header no-margin">
                                        <i class="text width icon"></i>
                                        <div class="content">
                                            Running Text
                                            <div class="sub header">Pengaturan untuk Running Text Laman Artikel</div>
                                        </div>
                                    </div >
                                    <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                                </a>
                            </div>
                            <div class="content field @if($page->textrunning_bool) active @endif" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                                <div class="field">
                                    <div class="ui toggle checkbox userunningtext">
                                        <input name="page[textrunning_visible]" type="checkbox">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Gunakan Running Text</i></label>
                                    </div>
                                    <div class="fields allmenuitem-runningtext" style="margin-top: 2em">
                                        <div class="eight wide field">
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Content Running Text</i></label>
                                            <div class="ui input">
                                                <input name="page[runningtext][content]" @if($page->textrunning_content) value="{{ $page->textrunning_content }}" @endif placeholder="Running text content..." type="text">
                                            </div>
                                        </div>
                                        <div class="four wide field">
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Kecepatan (detik)</i></label>
                                            <div class="ui input">
                                                <input name="page[runningtext][speed]" value="@if($page->textrunning_speed){{ $page->textrunning_speed }}@else 20 @endif" type="number">
                                            </div>
                                        </div>
                                        <div class="four wide field">
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Warna</i></label>
                                            <div class="ui selection dropdown">
                                                <input name="page[runningtext][color]" @if($page->textrunning_color) value="{{ $page->textrunning_color }}" @endif type="hidden">
                                                <div class="default text">Pilih warna...</div>
                                                <i class="dropdown icon"></i>
                                                <div class="menu">
                                                    <div class="item" data-value="normal">
                                                        Normal
                                                    </div>
                                                    <div class="item" data-value="red">
                                                        Merah
                                                    </div>
                                                    <div class="item" data-value="blue">
                                                        Biru
                                                    </div>
                                                    <div class="item" data-value="peach">
                                                        Peach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui tab bottom attached segment" data-tab="tab-en">
            <div class="equal width fields">
                <div class="field">
                    <div class="ui massive input">
                        <input name="page[title_en]" value="{{ $page->title_en }}" placeholder="Judul Artikel [english]" type="text">
                    </div>
                </div>
            </div>
            <div class="equal width fields">
                <div class="field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Deskripsi Singkat Artikel [english] <span style="font-size: .8em">(optional)</span></i></label>
                    <div class="ui input">
                        <input name="page[subtitle_en]" value="{{ $page->subtitle_en }}" placeholder="Deskripsi singkat Artikel [english]" type="text">
                    </div>
                </div>
                <div class="four wide field">
                    <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Menu [english] <span style="font-size: .8em">(optional)</span></i></label>
                    <div class="ui input">
                        <input name="page[menu_name_en]" value="{{ $page->menu_name_en }}" placeholder="Label menu [english]" type="text">
                    </div>
                </div>
            </div>
            <div class="equal width fields">
                <div class="field">
                    <textarea name="page[content_en]" placeholder="Content Artikel [english]">{{ $page->content_en }}</textarea>
                </div>
            </div>

            <div class="row">
                <div class="sixteen wide column">
                    <div class="ui accordion field" style="background: white">
                        <div class="title no-padding">
                            <a class="ui right labeled icon basic fluid button borderradiusless trigger" style="text-align: left">
                                <div  class="ui header no-margin">
                                    <i class="text width icon"></i>
                                    <div class="content">
                                        Running Text [english] <span style="font-size: .8em">(optional)</span>
                                        <div class="sub header">Pengaturan untuk Running Text Artikel</div>
                                    </div>
                                </div >
                                <i class="down chevron icon" style="background-color: rgba(0, 0, 0, 0);"></i>
                            </a>
                        </div>
                        <div class="content field @if($page->textrunning_bool) active @endif" style="box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.15) inset; padding: 1em 1.5em">
                            <div class="field">
                                <div class="fields allmenuitem-runningtext" style="margin-top: 2em">
                                    <div class="ten wide field">
                                        <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Content Running Text [english]</i></label>
                                        <div class="ui input">
                                            <input name="page[runningtext][content_en]" @if($page->textrunning_content_en) value="{{ $page->textrunning_content_en }}" @endif placeholder="Running text content [english]..." type="text">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
</div>

@include('footers.footer2')
@endsection

@section('footerscript')
<script>
    $itemSiderBar = '<div class="ui menuitem segment" style="padding: .5em; border: 1px dashed rgba(0, 0, 0, .2)">\
                                    <div class="ui fields no-margin">\
                                        <div class="five wide field">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Navigasi</i></label>\
                                            <div class="ui input">\
                                                <input name="page[sidebar][label][]" placeholder="Nama Menu" type="text">\
                                            </div>\
                                        </div>\
                                        <div class="ten wide field">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Pilih link atau input manual</i></label>\
                                            <div class="ui search selection dropdown siderbar costum">\
                                                <input name="page[sidebar][url][]" type="hidden">\
                                                <div class="default text">Link...</div>\
                                                <i class="dropdown icon"></i>\
                                                <div class="menu">'+
                                                    @if($master['page_all'])
                                                        <?php
                                                            $groupedPageByTypeID = $master['page_all']->where('status', '=', 'publish')->groupBy('page_type_id');
                                                        ?>
                                                        @foreach($groupedPageByTypeID as $key => $cur_page_col)
                                                            '<div class="header">\
                                                                <span style="font-size: 1.3em; color: rgba(0, 0, 0, .4)"> {!! App\PageType::find($cur_page_col[0]->page_type_id)->description !!}</span>\
                                                            </div>'+
                                                            @foreach($cur_page_col as $key2 => $cur_page)
                                                                '<div class="item" style="margin-left: 1em" data-value="'+'{{$cur_page->url}}'+'">'+
                                                                    '{{ $cur_page->title }}'+'<span style="color: rgba(0, 0, 0, .2)"> ('+'{{ url("/") }}/{{$cur_page->url}})</span>\
                                                                </div>'+
                                                            @endforeach
                                                        @endforeach
                                                    @endif
                                                '</div>\
                                            </div>\
                                        </div>\
                                        <div class="one wide field ki-right">\
                                            <label style="color: rgba(0, 0, 0, 0); font-weight: 400"><i>Aksi</i></label> \
                                            <div class="ui icon buttons">\
                                                <a onclick="deleteMenuItem(this)" class="ui button removeitem"><i class="trash icon"></i></a>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>';

    function addItem($whatitem) {
        if($whatitem == 'sidebar') {
            $('.allmenuitem-sidebar').append($itemSiderBar);
            if($('.checkbox.usesidebar').checkbox('is checked')) {
                enabledItemSidebar();
            } else {
                disabledItemSidebar();
            }

            $('.ui.dropdown.siderbar').dropdown({
                allowAdditions: true
            });
        }
    }

    function addItemSidebarWithValue(nameLabel, valueUrl) {
        $('.allmenuitem-sidebar').append('<div class="ui menuitem segment" style="padding: .5em; border: 1px dashed rgba(0, 0, 0, .2)">\
                                    <div class="ui fields no-margin">\
                                        <div class="five wide field">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Label Navigasi</i></label>\
                                            <div class="ui input">\
                                                <input name="page[sidebar][label][]" placeholder="Nama Menu" value="'+nameLabel+'" type="text">\
                                            </div>\
                                        </div>\
                                        <div class="ten wide field">\
                                            <label style="color: rgba(0, 0, 0, .5); font-weight: 400"><i>Pilih link atau input manual</i></label>\
                                            <div class="ui search selection dropdown siderbar costum">\
                                                <input name="page[sidebar][url][]" value="'+valueUrl+'" type="hidden">\
                                                <div class="default text">Link...</div>\
                                                <i class="dropdown icon"></i>\
                                                <div class="menu">'+
                                                    @if($master['page_all'])
                                                        <?php
                                                            $groupedPageByTypeID = $master['page_all']->where('status', '=', 'publish')->groupBy('page_type_id');
                                                        ?>
                                                        @foreach($groupedPageByTypeID as $key => $cur_page_col)
                                                            '<div class="header">\
                                                                <span style="font-size: 1.3em; color: rgba(0, 0, 0, .4)"> {!! App\PageType::find($cur_page_col[0]->page_type_id)->description !!}</span>\
                                                            </div>'+
                                                            @foreach($cur_page_col as $key2 => $cur_page)
                                                                '<div class="item" style="margin-left: 1em" data-value="'+'{{$cur_page->url}}'+'">'+
                                                                    '{{ $cur_page->title }}'+'<span style="color: rgba(0, 0, 0, .2)"> ('+'{{ url("/") }}/{{$cur_page->url}})</span>\
                                                                </div>'+
                                                            @endforeach
                                                        @endforeach
                                                    @endif
                                                '</div>\
                                            </div>\
                                        </div>\
                                        <div class="one wide field ki-right">\
                                            <label style="color: rgba(0, 0, 0, 0); font-weight: 400"><i>Aksi</i></label> \
                                            <div class="ui icon buttons">\
                                                <a onclick="deleteMenuItem(this)" class="ui button removeitem"><i class="trash icon"></i></a>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>');
        

        $('.ui.dropdown.siderbar').dropdown({
            allowAdditions: true
        });
    }

    function disabledItemSidebar() {
        $('.allmenuitem-sidebar').addClass('disabled');
        $('.allmenuitem-sidebar').find('.ui.input').addClass('disabled');
        $('.allmenuitem-sidebar').find('.dropdown').addClass('disabled');
        $('.allmenuitem-sidebar').find('.button.removeitem').addClass('disabled');
        $('.button.tambahitem-sidebar').addClass('disabled');
    }

    function enabledItemSidebar() {
        $('.allmenuitem-sidebar').removeClass('disabled');
        $('.allmenuitem-sidebar').find('.ui.input').removeClass('disabled');
        $('.allmenuitem-sidebar').find('.dropdown').removeClass('disabled');
        $('.allmenuitem-sidebar').find('.button.removeitem').removeClass('disabled');
        $('.button.tambahitem-sidebar').removeClass('disabled');
    }

    function disabledItemRunningtext() {
        $('.allmenuitem-runningtext').addClass('disabled');
        $('.allmenuitem-runningtext').find('.ui.input').addClass('disabled');
    }

    function enabledItemRunningtext() {
        $('.allmenuitem-runningtext').removeClass('disabled');
        $('.allmenuitem-runningtext').find('.ui.input').removeClass('disabled');
    }

    function deleteMenuItem($caller) {
        var $selectedMenuitem = $($caller).closest('.menuitem');
        $selectedMenuitem.remove();
    }

    $('input:text', '.ui.actionfile.input').click(function() {
        $(this).parent().find("input:file").click();
    });
    $('div.button', '.ui.actionfile.input').click(function() {
        $(this).parent().find("input:file").click();
    });

    $('input:file', '.ui.actionfile.input').on('change', function(e) {
        if(e.target.files[0]) {
            var name = e.target.files[0].name;
            $('input:text', $(e.target).parent()).val(name);
            readURL(this);
        }
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var tesPare = $(input).parent().parent();
                var img = $(input).parent().parent().find('img');
                $(img).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function() {
        $('.tabular.menu .item').tab();
        //INITIALIZE
        @if($page->sidebars()->count())
            $('.checkbox.usesidebar').checkbox('set checked');
            enabledItemSidebar();
            @foreach($page->sidebars as $key => $existingSidebars)    
                addItemSidebarWithValue('{{ $existingSidebars->label }}', '{{ $existingSidebars->url }}');
            @endforeach
        @else
            addItem('sidebar');
        @endif

        @if($page->textrunning_bool)
            $('.checkbox.userunningtext').checkbox('set checked');
            enabledItemRunningtext();
        @endif

        if($('.checkbox.userunningtext').checkbox('is checked')) enabledItemRunningtext();
        else disabledItemRunningtext();

        if($('.checkbox.usesidebar').checkbox('is checked')) enabledItemSidebar();
        else disabledItemSidebar();

        $('input:file', '.ui.actionfile.input').trigger("change");
        
        $('#form_page_create').addClass('loading');
        tinymce.init({
            selector     : "textarea",
            height: 500,
            theme: 'modern',
            plugins      : [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help',
                "insertdatetime media table contextmenu paste responsivefilemanager"
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab : true,
            setup: function(editor) {
                editor.on('init', function() {
                    $('#form_page_create').removeClass('loading');
                });
            },
            relative_urls: false,
            external_filemanager_path:"{!! str_finish(asset('libs/filemanager'),'/') !!}",
            filemanager_title        :"File Manager" ,
            external_plugins         : { "filemanager" : "{{ asset('libs/filemanager/plugin.min.js') }}"} 
        }); 

        //FORM
        $('.checkbox.usesidebar').checkbox({
            onChecked: function() {
                enabledItemSidebar();
            },
            onUnchecked: function() {
                disabledItemSidebar();
            }
        });

        $('.checkbox.userunningtext').checkbox({
            onChecked: function() {
                enabledItemRunningtext();
            },
            onUnchecked: function() {
                disabledItemRunningtext();
            }
        });

        //PAGE TITLE
        $('input[name="page[title]"]').on('input', function() {
            $('input[name="page[url]"]').val($('input[name="page[title]"]').val().toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-'));
        });

        //PAGE URL
        $('input[name="page[url]"]').on('input', function() {
            $('input[name="page[url]"]').val($('input[name="page[url]"]').val().toLowerCase().replace(/[^\w-]+/g,''));
        });

        $('input[name="page[title]"]').on('change', function() {
            $('input[name="page[url]"]').trigger('change');
        });

        $('input[name="page[url]"]').on('change', function() {
           
            $.ajax({
                url: '{{ url("admin/api/page/urlisexsist") }}/'+$('input[name="page[url]"]').val(),
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    if(data == '1') {
                        alert("URL Artikel {{ url('/') }}/"+$('input[name="page[url]"]').val()+"/ SUDAH terpakai. Mohon gunakan URL Laman yang lain");
                        $('input[name="page[url]"]').val('');
                    }
                }
            });
        });

        //PAGE TYPE
        $('#page_type').dropdown('set selected', '{{$page->page_type_id}}');
        $('#page_parent').dropdown('set selected', '{{$page->parent_id}}');
        $('#page_tags').dropdown({
            allowAdditions: true
        });

        var datatags = [];
        @foreach($page->tags as $key => $valueTag)
            datatags.push('{{ $valueTag->name }}');
        @endforeach

        $('#page_tags').dropdown('set selected', datatags);

        //ACCORDION
        $('.accordion')
            .accordion({
                selector: {
                    trigger: '.trigger'
                },
                onOpening: function() {
                    $(this).parent().find('.title').find('.trigger').find('.icon').removeClass('down');
                    $(this).parent().find('.title').find('.trigger').find('.icon').addClass('up');
                },
                onClosing: function() {
                    $(this).parent().find('.title').find('.trigger').find('.icon').removeClass('up');
                    $(this).parent().find('.title').find('.trigger').find('.icon').addClass('down');
                }
            })
        ;

        //FORM VALIDATION
        $('#form_page_create')
            .form({
                fields : {
                    page_judul: {
                        identifier : 'page[title]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    page_type: {
                        identifier : 'page[type]',
                        rules: [
                            {
                                type   : 'empty',
                                prompt : '{name} tidak boleh kosong'
                            }
                        ]
                    },
                    // page_content: {
                    //     identifier : 'page[content]',
                    //     rules: [
                    //         {
                    //             type   : 'minLength[25]',
                    //             prompt : '{name} harus minimal {ruleValue}'
                    //         }
                    //     ]
                    // },
                },
                inline : true,
                on     : 'blur'
            })
        ;
    });
</script>
@endsection
