@extends('layouts.app1')

@section('template_title')
    @if(isset($page['name']))
        {!! $page_name = $page['name'] !!}
    @elseif(Request::route()->getAction()['page_name'] !== null)
        {!! $page_name = Request::route()->getAction()['page_name'] !!}
    @endif
@endsection

@section('topbar')
    @include('headers.header4')
@endsection

@section('floatingbar')
    
@endsection

@section('sidebar')
    @include('admin.sidebar.mini')
@endsection
<div class="ui dimmer">
    <div class="ui loader"></div>
</div>
@section('content')
<!-- Page Contents -->
<div class="ui clearing padded segment basic no-margin" style="background-color: #F9FAFB; padding-top: .5em; padding-bottom: .8em">
    <div class="ui breadcrumb right">
        <a class="section">Dashboard</a>
        <i class="right chevron icon divider"></i>
        <a class="section">Jurnal</a>
        <i class="right arrow icon divider"></i>
        <div class="active section">Semua</div>
    </div>
</div>
<div class="ui segment padded main basic no-margin">
    <div class="ui basic clearing segment no-padding center aligned">
        <div class="ui small buttons right floated">
            <a href="{{ url('admin/api/journal/oai/update') }}" class="ui yellow right labeled icon button updateoai"><i class="retweet icon"></i>Update dari IPTEK</a>
        </div>
        <div class="ui small buttons">
            <!--<button class="ui green button">Tambah</button>-->
        </div>
        <div class="ui small buttons left floated">
            <a href="{{ url('admin/journal/create') }}" class="ui green left labeled icon button"><i class="add arrow icon"></i>Tambah</a>
        </div>
    </div>
    <div class="ui divider"></div>
    @if(session('alert') !=null)
    <div class="ui message @if(isset(session('alert')['color'])) {{session('alert')['color']}} @endif closeable">
        <!-- <div class="header">
            Terjadi kesalahan!
        </div> -->
        <p>{!! session('alert')['pesan'] !!}</p>
    </div>
    @endif
    <table id="pages" class="ui celled table" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th class="eight wide">Title</th>
                <th class="two wide">Status</th>
                <th class="three wide">Klasifikasi</th>
                <th class="two wide">Terupdate</th>
                <th class="two wide">Tindakan</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
@include('modal.confirm_delete')
@include('footers.footer2')
@endsection

@section('footerscript')
<script>
    $(document).ready(function() {
        
        $('.updateoai').on('click', function() {
            $('.dimmer').addClass('active');
            $('.footer').addClass('hidden');
        });
        $('#pages').DataTable({
            select: true,
            "oLanguage": {
                "sSearch": "Cari data {{ $page_name }}:",
                "sZeroRecords": "<i>Data {{ $page_name }} kosong</i>",
                "sInfoEmpty": "",
                "oPaginate": {
                    "sNext": "Selanjutnya",
                    "sPrevious": "Sebelumnya",
                    "sFirst": "Pertama",
                    "sLast": "Terakhir",
                },
                "sInfo": "Total _TOTAL_ data {{ $page_name }}",
                "sLengthMenu": '<select>'+
                                '<option value="10">10</option>'+
                                '<option value="50">50</option>'+
                                '<option value="-1">All</option>'+
                                '</select> data {{ $page_name }}'
            },
            processing: true,
            serverSide: true,
            buttons: [
                'csv', 'excel', 'pdf', 'print', 'reset', 'reload'
            ],
            ajax: "{{ url('/datatable/journal/all?with=') }}",
            columnDefs: [
                { className: "center aligned", "targets": [ 4 ] }
            ],
            order: [[ 3, "desc" ]],
            columns: [
                {data: 'title'},
                {data: 'status'},
                {data: 'klasifikasi'},
                {data: 'updated_at'},
                {data: 'action', orderable: false, searchable: false},
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
                });
            }
        });
    } );
</script>
@endsection
