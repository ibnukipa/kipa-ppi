<!--Floating Fixed Segment-->
<div class="fixed-segment bottom right">
    <!--<button class="ui compact icon button">
        <i class="pause icon"></i>
    </button>-->
    <div class="ui pointing refreshable dropdown link compact labeled search icon button">
        <i class="world icon"></i>
        <span class="text">
            <i class="@if( LaravelLocalization::getCurrentLocale() == 'en') us @else {{LaravelLocalization::getCurrentLocale()}} @endif flag"></i>
            {{LaravelLocalization::getCurrentLocaleName()}}
        </span>
        <div class="menu">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode  => $properties)
                @if($localeCode == LaravelLocalization::getCurrentLocale())
                    <a class="item active selected">
                        <i class="@if($localeCode == 'en') us @else {{ $localeCode }} @endif flag"></i> {{ $properties['name'] }}
                    </a>
                @else
                    <a class="item" rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                        <i class="@if($localeCode == 'en') us @else {{ $localeCode }} @endif flag"></i> {{ $properties['name'] }}
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</div>