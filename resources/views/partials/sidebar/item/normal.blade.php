<a class="item">
    <i class="{!! $pre_icon !!} large icon left"></i>
    <div class="middle aligned content inline">
        <div class="header">
            @if($header)
                <b>{!! $name !!}</b> 
            @endif
        </div>
    </div>
    <div class="middle aligned content inline right floated">
        <div class="ui red horizontal small label no-margin">51</div>
    </div>
</a>