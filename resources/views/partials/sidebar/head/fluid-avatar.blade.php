<div class="item fitted">
    <div class="ui basic card borderradiusless box-shadowless">
        <div class="image">
            <img class="borderradiusless" src="{{ asset('storage/logo-square-green-512.png') }}">
        </div>
        <div class="content center">
            <h2 class="ui header">
                Ibnu Prayogi
                <div class="sub header">SMP Negeri 1 Kota Besar Terbagi Lima</div>
            </h2>
        </div>
        <div class="extra content">
            <div class="ui tiny basic buttons">
                <button class="ui button center" data-inverted data-tooltip="100 pertanyaan" data-position="bottom left">
                    <i class="left help circle icon"></i>
                    100
                </button>
                <button class="ui right button" data-inverted data-tooltip="100 menjawab" data-position="bottom right">
                    100
                    <i class="right talk icon"></i>
                </button>
            </div>
            <div class="ui tiny basic icon buttons">
                <button class="ui button" data-inverted data-tooltip="Profile Edit" data-position="bottom left">
                    <i class="edit icon"></i>
                </button>
                <button class="ui button" data-inverted data-tooltip="Profile Settings" data-position="bottom right">
                    <i class="settings icon"></i>
                </button>
            </div>
        </div>
    </div>
</div>