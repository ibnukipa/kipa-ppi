<div class="item bordered">
    <div class="ui logo icon image" style="width: 85px; margin-right: 1em;">
        <img src="{{ asset('storage/logo-square-green-512.png') }}">
    </div>
    <div class="middle aligned content inline">
        <h3 class="ui green header">KiFramework
            <div class="sub header">Sharing to others</div>
        </h3>
        <div class="ui mini icon buttons">
            <a class="ui green basic button" data-inverted data-tooltip="Profile Edit" data-position="top left">
                <i class="edit icon"></i>
            </a>
            <a class="ui green basic button" data-inverted data-tooltip="Profile Settings" data-position="top right">
                <i class="settings icon"></i>
            </a>
        </div>
        <a class="ui mini red icon button" data-inverted data-tooltip="Keluar" data-position="top center">
            <i class="sign out icon"></i>
        </a>
    </div>
</div>
<div class="item horizontaly fitted m-bottom1" style="padding: .5rem 1rem">
    <div class="ui mini labeled icon basic blue button" data-inverted data-tooltip="100 pertanyaan" data-position="bottom left">
        <i class="help icon ki-blue"></i>
        1.000
    </div>
    <div class="ui mini right labeled icon basic blue button right floated" data-inverted data-tooltip="100 menjawab" data-position="bottom right">
        <i class="talk icon ki-blue"></i>
        1.000
    </div>
</div>