<!-- Sidebar Menu -->
<div class="ui left vertical sidebar accordion menu contentborderless borderless card">
    @if (Auth::guest())
        @include('partials.sidebar.head.small-avatar')
        <div class="ui divider no-margin-bottom no-border-bottom"></div>
        @include('partials.sidebar.item.normal', [
            'pre_icon'      => 'alarm',
            'header'        => true,
            'name'          => 'Notifikasi'
        ])
    @else

    @endif
</div>