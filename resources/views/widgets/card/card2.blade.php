<div class="ui fluid card">
    <div class="image">
        <img src="{{ $image_link }}">
    </div>
    <div class="content">
        <div class="header">Jurnal Aplikasi Teknik Sipil</div>
        <div class="description">
            Jurnal ini dikelola oleh Depatemen ...
        </div>            
    </div>
    <div class="extra content center">
        <div class="ui tiny buttons">
            <a href="{{ url('/') }}" class="ui icon button" data-tooltip="Detail journal">
                <i class="eye icon"></i>
            </a>
            <a href="{{ url('/sada') }}" target="_blank" class="ui green right labeled icon button">
                Visit
                <i class="external icon"></i>
            </a>
        </div>
    </div>
</div>