<div class="ui fluid card">
    <div class="image">
        <img src="{{ url($image_link) }}">
    </div>
    <div class="content">
        <div class="header">
            {{ $header }}
        </div>
        <div class="meta">
            <span class="date">{{ $label }}</span>
            @if($accreditation)
                <span class="date">Terakreditasi</span>
            @endif
        </div>
         <div class="description">
            {{ $description }}
        </div> 
    </div>
    <div class="extra content center">
        <div class="ui tiny buttons">
            <a href="{{ $detail_link }}" class="ui icon button" data-tooltip="Detail journal">
                <i class="eye icon"></i>
            </a>
            <a href="{{ $open_link }}" target="_blank" class="ui green right labeled icon button">
                View Journal
                <i class="sign in icon"></i>
            </a>
        </div>
    </div>
</div>