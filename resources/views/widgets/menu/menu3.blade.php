<div class="ui vertical menu">
    <a class="active teal item">
        Inbox
        <div class="ui teal left pointing label">1</div>
    </a>
    <a class="item">
        Spam
        <div class="ui label">51</div>
    </a>
    <div class="ui dropdown item">
        More
        <i class="dropdown icon"></i>
        <div class="menu">
            <a class="item"><i class="edit icon"></i> Edit Profile</a>
            <a class="item"><i class="globe icon"></i> Choose Language</a>
            <a class="item"><i class="settings icon"></i> Account Settings</a>
        </div>
    </div>
    <a class="item">
        Updates
        <div class="ui label">1</div>
    </a>
    <div class="item">
        <div class="ui transparent icon input">
            <input placeholder="Search mail..." type="text">
            <i class="search icon"></i>
        </div>
    </div>
</div>