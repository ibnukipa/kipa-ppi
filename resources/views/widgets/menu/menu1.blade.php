<div class="ui secondary vertical pointing menu left stickyable context">
    <a href="#" class="item active">
        Bio
    </a>
    <a href="#" class="item">
        Pics
    </a>
    <div class="ui dropdown left pointing item">
        More
        <i class="dropdown icon"></i>
        <div class="menu">
            <a class="item"><i class="edit icon"></i> Edit Profile</a>
            <a class="item"><i class="globe icon"></i> Choose Language</a>
            <a class="item"><i class="settings icon"></i> Account Settings</a>
        </div>
    </div>
</div>