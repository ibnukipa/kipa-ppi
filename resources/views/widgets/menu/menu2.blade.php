<div class="ui  vertical menu">
    <div class="item">
        <div class="header">Products</div>
        <div class="menu">
            <a class="item">Enterprise</a>
            <a class="item">Consumer</a>
        </div>
    </div>
    <div class="item">
        <div class="header">CMS Solutions</div>
        <div class="menu">
            <a class="active item">Rails</a>
            <a class="item">Python</a>
            <a class="item">PHP</a>
        </div>
    </div>
</div>