<div class="ui menu icon borderless grid borderradiusless no-margin" style="height: 61px; padding-right: 1em">
  <div class="item">
    <h4 class="ui header left aligned">
      <div class="content">
        @if(isset($page['name']))
            {{ $page['name'] }}
        @elseif(Request::route()->getAction()['page_name'] !== null)
            {{ Request::route()->getAction()['page_name'] }}
        @endif
        <div class="sub header">
          @if(isset($page['description']))
              {{ $page['description'] }}
          @elseif(Request::route()->getAction()['page_description'] !== null)
              {{ Request::route()->getAction()['page_description'] }}
          @endif
        </div>
      </div>
    </h4>
  </div>
  <div class="ui dropdown item right">
    <img class="ui mini circular image" src="{{ asset('storage/logo-square-green-512.png') }}">
    <div class="content ki-left" style="padding-left: 1em">
      <div class="ui sub header">{{ Auth::user()->email }}</div>
      Administrator
    </div>
    <div class="menu">
      <a href="{{ url('admin/dashboard') }}" class="item">
        <i class="grid layout icon"></i> Dashboard
      </a> 
      <a class="item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <i class="sign out layout icon"></i> Keluar
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </a>
    </div>
  </div> 
</div>