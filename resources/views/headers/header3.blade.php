<div class="header3 bar light ki-bg image{{ Color::getColor() }} @if(isset($no_topbar)) darken @endif">
    @if(!isset($no_topbar))
    <div class="ui large borderradiusless inverted top menu red no-margin ki-bg image{{ Color::getColor() }} darken shadow-2p" style="padding: 0em 0em 0em .5em">
        <a class="ui toc item button">
            <i class="large sidebar icon no-margin-lr"></i>
        </a>
        <div class="ui title item fitted">
            <a href="{{ url('/') }}" class="ui header">
                <img src="{{ asset('storage/ava/ava-flash.png') }}">
                <div class="content ki-c-white">
                    Sistem Pelayanan UPT
                    <div class="sub header ki-c-white" style="opacity: .5">Laboratorium Lingkungan</div>
                </div>
            </a>
        </div>
        <div class="right inverted menu">
            @if (Auth::guest())
                <a href="{{ route('login') }}" class="item">
                    Login
                </a>
            @else
                @if (Auth::user()->hasRole('Front_Office') )
                    <a class="item @if(isset($menu_active) && $menu_active == 'registrasi') active @endif" href="{{ url('/front/home') }}">Registrasi</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'job') active @endif" href="{{ url('/front/joblist') }}">Daftar Job</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'customer') active @endif" href="{{ url('/front/customerlist') }}">Daftar Pelanggan</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'sertif') active @endif" href="{{ url('/front/certificatelist') }}">Cetak Sertifikat</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'price') active @endif" href="{{ url('/front/pricelist') }}">Daftar Harga</a>
                @elseif (Auth::user()->hasRole('Analyst') && Auth::user()->hasRole('Supervisor'))
                    <div class="ui dropdown item">
                        Notifikasi
                        <a class="ui red circular label">{{$notif->count()}}</a>
                        <div class="menu borderradiusless">
                            @foreach($notif as $value)
                                <a href="{{ url('/analyst/samplelist/'.$value->link)}}" class="item">Revisi {{$value->parameter->name}}</a>
                            @endforeach
                        </div>
                    </div>

                    @if($menu_active != null)
                        @foreach($param as $value)
                            <a class="item @if(isset($menu_active) && $menu_active->name == $value->parameter->name) active @endif" href="{{ url('/analyst/'.$value->parameter_id) }}">{{$value->parameter->name}}</a>
                        @endforeach
                            <a class="item" href="{{ url('/supervisor/home')}}">Penyelia</a>
                    @else
                        @foreach($param as $value)
                            <a class="item" href="{{ url('/analyst/'.$value->parameter_id) }}">{{$value->parameter->name}}</a>
                        @endforeach
                            <a class="item active" href="{{ url('/supervisor/home')}}">Penyelia</a>
                    @endif                                    

                @elseif (Auth::user()->hasRole('Analyst'))
                    <div class="ui dropdown item">
                        Notifikasi
                        <a class="ui red circular label">{{$notif->count()}}</a>
                        <div class="menu borderradiusless">
                            @foreach($notif as $value)
                                <a href="{{ url('/analyst/samplelist/'.$value->link)}}" class="item">Revisi {{$value->parameter->name}}</a>
                            @endforeach
                        </div>
                    </div>

                    @foreach($param as $value)
                        <a class="item @if(isset($menu_active) && $menu_active->name == $value->parameter->name) active @endif" href="{{ url('/analyst/'.$value->parameter_id) }}">{{$value->parameter->name}}</a>
                    @endforeach  

                @elseif (Auth::user()->hasRole('Head_of_unit'))
                    <?php $year= \Carbon\Carbon::now()->year ?>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Harian') active @endif" href="{{ url('/head/home') }}">Laporan Harian</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Bulanan') active @endif" href="{{ url('/head/bulanan/'.$year) }}">Laporan Bulanan</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Tahunan') active @endif" href="{{ url('/head/tahunan') }}">Laporan Tahunan</a>
                @elseif (Auth::user()->hasRole('Viewer'))
                    <?php $year= \Carbon\Carbon::now()->year ?>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Harian') active @endif" href="{{ url('/viewer/home') }}">Laporan Harian</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Bulanan') active @endif" href="{{ url('/viewer/bulanan/'.$year) }}">Laporan Bulanan</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Tahunan') active @endif" href="{{ url('/viewer/tahunan') }}">Laporan Tahunan</a>
                @elseif (Auth::user()->hasRole('Section_head'))
                    <?php $year= \Carbon\Carbon::now()->year ?>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Harian') active @endif" href="{{ url('/kasi/home') }}">Laporan Harian</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Bulanan') active @endif" href="{{ url('/kasi/bulanan/'.$year) }}">Laporan Bulanan</a>
                    <a class="item @if(isset($menu_active) && $menu_active == 'Tahunan') active @endif" href="{{ url('/kasi/tahunan') }}">Laporan Tahunan</a> 
                    <a class="item @if(isset($menu_active) && $menu_active == 'latelist') active @endif" href="{{ url('/kasi/latelist') }}">Laporan Keterlambatan Analis</a>                                                        
                @endif

                    <a class="ki-c-{{ Color::getColor() }} item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">  
                            Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
            @endif
        </div>
    </div>
    @endif
    <div class="ui basic segment hiddenable no-margin">
        <h2 class="ui header left floated ki-c-white" style="opacity: .7">
            <div class="content">
                {{ $header_name }}
                @if(isset($header_description))
                    <div class="sub header">{{ $header_description }}</div>
                @endif
            </div>
        </h2>
        
        <h2 class="ui header right floated right aligned ki-c-white" style="opacity: .5">
            <div class="content">
                @if(!Auth::guest())
                    {{ Auth::user()->name }}
                    @if(isset($header_description))
                        <div class="sub header">{{ Auth::user()->email }}</div>  
                    @endif
                @endif
            </div>
        </h2>
    </div>
</div>