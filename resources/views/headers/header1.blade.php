<?php
    if( LaravelLocalization::getCurrentLocale() == 'en')
        $postFixLang = '_en';
    else
        $postFixLang = '';
    
    $menu_name              = 'menu_name'.$postFixLang;
?>
<div class="ui segment basic no-margin borderradiusless">
    <div class="ui container">
        <div class="ui fluid grid">
            <div class="left floated eight wide column">
                <h2 class="ui header inline no-margin">
                    <img src="{{ asset('storage/logo-teal-1024.png') }}">
                    <div class="content">
                        {{ App\Setting::where('name', '=', 'header')->first()->value }}
                         <div class="sub header">{{ App\Setting::where('name', '=', 'subheader')->first()->value }}</div> 
                    </div>
                </h2>
            </div> 
            <div class="right floated eight wide column middle aligned computer only" style="text-align: right">
                <form action="{{ url('/search') }}" class="ui form" method="GET">
                    <input value="@if(isset($search['pageCur'])) {{ $search['pageCur'] }} @endif" name="search[pageCur]" type="hidden">
                    <input value="@if(isset($search['pageSize'])) {{ $search['pageSize'] }} @endif" name="search[pageSize]" type="hidden">
                    <input value="@if(isset($search['sort'])) {{ $search['sort'] }} @endif" name="search[sort]" type="hidden">
                    <input value="@if(isset($search['classification'])) {{ $search['classification'] }} @endif" name="search[classification]" type="hidden">
                    <input value="@if(isset($search['type'])) {{ $search['type'] }} @endif" name="search[type]" type="hidden">
                    <input value="@if(isset($search['from'])) {{ $search['from'] }} @endif" name="search[from]" type="hidden">
                    <div class="ui action input">
                        <input value="@if(isset($search['key'])) {{ $search['key'] }} @endif" name="search[key]" placeholder="Search..." type="text">
                        <button type="submit" class="ui blue icon button"><i class="search icon"></i></button>
                    </div>
                </form> 
            </div> 
        </div>
    </div>
</div>

<div class="overlay">
    <div class="ui static inverted blue main menu borderless borderradiusless no-margin">
        <div class="ui container">
            <!-- <div class="item v-hide logo" style="padding-right: .5em">
                <img src="{{ asset('storage/logo-white-1024.png') }}">
            </div> -->
            <a href="{{ url('/') }}" class="item {{ isActiveURL(LaravelLocalization::getCurrentLocale().'/', 'active') }} {{ isActiveURL(LaravelLocalization::getCurrentLocale().'/home', 'active') }}">
                @if(App\Page::where('url', '=', 'home')->first()->$menu_name != '' || App\Page::where('url', '=', 'home')->first()->$menu_name != null) {{App\Page::where('url', '=', 'home')->first()->$menu_name}} @else {{App\Page::where('url', '=', 'home')->first()->menu_name}} @endif
            </a>
            <a href="{{ url('/journals') }}" class="item {{ isActiveURL(LaravelLocalization::getCurrentLocale().'/journals', 'active') }}">
                @if(App\Page::where('url', '=', 'journals')->first()->$menu_name != '' || App\Page::where('url', '=', 'journals')->first()->$menu_name != null) {{App\Page::where('url', '=', 'journals')->first()->$menu_name}} @else {{App\Page::where('url', '=', 'journals')->first()->menu_name}} @endif
            </a>
            <a href="{{ url('/articles') }}" class="item {{ isActiveURL(LaravelLocalization::getCurrentLocale().'/articles', 'active') }}">
                @if(App\Page::where('url', '=', 'articles')->first()->$menu_name != '' || App\Page::where('url', '=', 'articles')->first()->$menu_name != null) {{App\Page::where('url', '=', 'articles')->first()->$menu_name}} @else {{App\Page::where('url', '=', 'articles')->first()->menu_name}} @endif
            </a>
            <?php 
                $curPage = App\Page::where('id', '!=', 1)
                        ->where('id', '!=', 2)
                        ->where('id', '!=', 3)
                        ->where('visible', '=', 1)
                        ->where('page_type_id', '=', 1)
                        ->where('parent_id', '=', 0)
                        ->get();
            ?>
                @foreach ($curPage as $key => $value)
                    <a href="{{ url('/'.$value->url) }}" class="item {{ isActiveURL(LaravelLocalization::getCurrentLocale().'/'.$value->url, 'active') }}">{{ $value->$menu_name }}</a>
                @endforeach
            
            

            @if (!Auth::guest())
            |
            <a href="{{ url('/admin') }}" class="item">Dashboard</a>
            <a class="item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">  
                    Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            @endif
        </div>
    </div>
</div>