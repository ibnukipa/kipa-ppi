<div class="following bar">
    <div class="ui large secondary inverted top menu no-margin">
        <a class="ui toc item button">
            <i class="large sidebar icon no-margin-lr"></i>
        </a>
        <div class="item fitted">
            <a href="{{ url('/') }}">
                <img class="ui small image" src="{{ asset('storage/logo-rectangle-white-512.png') }}" inverted-src="{{ asset('storage/logo-rectangle-green-512.png') }}">
            </a>
        </div>
        <div class="right menu">
            @if (Auth::guest())
                <div class="item horizontally fitted">
                    <a href="{{ route('register') }}" class="ui green button capitalize">@lang('basic.register')</a>
                </div>
                <div class="item horizontally fitted">
                    <a href="{{ route('login') }}" class="ui green basic button capitalize">@lang('basic.login')</a>
                </div>
            @else
                <div class="item horizontally fitted">
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" 
                        class="ui green basic button">
                            @lang('basic.logout')
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            @endif
        </div>
    </div>
</div>