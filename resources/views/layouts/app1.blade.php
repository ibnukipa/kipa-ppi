<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <!-- Standard Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        @if (trim($__env->yieldContent('meta')))
            @yield('meta')
        @else
            <meta property="og:title" content="Pusat Publikasi Ilmiah - Better!">
            <meta property="og:url" content="{{ url('/') }}">
            <meta property="og:description" content="Journal">
            <meta property="og:image" content="{{ asset('storage/logo-square-green-512.png') }}">
            <meta property="og:type" content="website">
        @endif

        <link rel="icon" type="image/png" href="{{ asset('storage/logo-square-green-512.png') }}">

        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', 'KiFramework') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Site Properties -->
        <link rel="stylesheet" type="text/css" href="{{ asset('semantic/components/reset.css') }}">
        <link href="{{ asset('css/setting/position.css') }}" rel="stylesheet">
        <link href="{{ asset('css/setting/color.css') }}" rel="stylesheet">
        <link href="{{ asset('semantic/semantic.css') }}" rel="stylesheet">
        <link href="{{ asset('libs/datatables.semanticui.css') }}" rel="stylesheet">
        <link href="{{ asset('css/ki.css') }}" rel="stylesheet">
        
        <!-- Scripts -->
        <script>
        window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>

        <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script> 
        <script src="{{ asset('libs/datatables.js') }}"></script>
        <script src="{{ asset('libs/datatables.semanticui.js') }}"></script>
        <script src="{{ asset('semantic/semantic.min.js') }}"></script>
        <script src="{{ asset('js/ki.js') }}"></script>

        @yield('extend_lib')
    </head>
    <body>
        <!--LAYOUT-->
        @yield('topbar')
        @yield('floatingbar')
        @yield('sidebar')
        @yield('content')
        @yield('footerscript')
    </body>
</html>
