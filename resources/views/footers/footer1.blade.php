<?php
    if( LaravelLocalization::getCurrentLocale() == 'en')
        $postFixLang = '_en';
    else
        $postFixLang = '';
    
    $menu_name              = 'menu_name'.$postFixLang;
?>

<div class="ui inverted blue segment borderradiusless no-margin" style="padding: 0em 4em; margin-top: 2em !important">
    <div class="ui four column grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted vertical text menu">
                    <div class="header item ki-c-white capitalize" style="font-size: 1.5em">Information for</div>
                    @foreach(App\Page::where('page_type_id', '=', 2)->get() as $key => $childPage)
                        <a href="{{ url('/'.$childPage->url) }}" class="item no-margin">
                            @if($childPage->$menu_name == '')
                                {{$childPage->menu_name}}
                            @else
                                {{$childPage->$menu_name}}
                            @endif
                        </a>
                    @endforeach
                </div>
            </div> 
            <div class="column">
                <div class="ui inverted vertical text menu">
                    <div class="header item ki-c-white capitalize" style="font-size: 1.5em">Open access</div>
                        <a href="{{ url('/journals') }}" class="item no-margin">
                            @if(App\Page::where('url', '=', 'journals')->first()->$menu_name != '' || App\Page::where('url', '=', 'journals')->first()->$menu_name != null) {{App\Page::where('url', '=', 'journals')->first()->$menu_name}} @else {{App\Page::where('url', '=', 'journals')->first()->menu_name}} @endif
                        </a>
                        <a href="{{ url('/articles') }}" class="item no-margin">
                            @if(App\Page::where('url', '=', 'articles')->first()->$menu_name != '' || App\Page::where('url', '=', 'articles')->first()->$menu_name != null) {{App\Page::where('url', '=', 'articles')->first()->$menu_name}} @else {{App\Page::where('url', '=', 'articles')->first()->menu_name}} @endif
                        </a>
                    @foreach(App\Page::where('page_type_id', '=', 3)->get() as $key => $childPage)
                        <a href="{{ url('/'.$childPage->url) }}" class="item no-margin">
                            @if($childPage->$menu_name == '')
                                {{$childPage->menu_name}}
                            @else
                                {{$childPage->$menu_name}}
                            @endif
                        </a>
                    @endforeach
                </div>
            </div> 
            <div class="column">
                <div class="ui inverted vertical text menu">
                    <div class="header item ki-c-white capitalize" style="font-size: 1.5em">Help and info</div>
                    @foreach(App\Page::where('page_type_id', '=', 4)->get() as $key => $childPage)
                        <a href="{{ url('/'.$childPage->url) }}" class="item no-margin">
                            @if($childPage->$menu_name == '')
                                {{$childPage->menu_name}}
                            @else
                                {{$childPage->$menu_name}}
                            @endif
                        </a>
                    @endforeach
                </div>
            </div> 
            <div class="column">
                <div class="ui inverted vertical text menu">
                    <div class="header item ki-c-white capitalize" style="font-size: 1.5em">Connect with us</div>
                    <div class="active item">
                        <div class="ui icon basic inverted  buttons">
                            @foreach(App\Setting::where('name', 'like', 'sosmed_%')->get() as $key => $sosmed)
                                @if($sosmed->value != '' && $sosmed->value != NULL)
                                <a data-tooltip="{{ $sosmed->value }}" data-position="top center" target="_blank" href="{{ $sosmed->value }}" class="ui button">
                                    <i class="{{ explode('_', $sosmed->name)[1] }} icon"></i>
                                </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="two column row">
            <div class="column padded">
                Copyright © Institut Teknologi Sepuluh Nopember
            </div>
            <div class="column right aligned">
                Version 1.0.1
            </div>
        </div> 
    </div> 
</div>