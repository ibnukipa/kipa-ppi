
<div class="ui inverted teal segment borderradiusless no-margin footer" style="padding: 1em 2em">
    <div class="ui four column grid">
        <div class="two column row">
            <div class="column padded">
                Copyright © Institut Teknologi Sepuluh Nopember
            </div>
            <div class="column right aligned">
                Version 1.0.1
            </div>
        </div> 
    </div> 
</div>
