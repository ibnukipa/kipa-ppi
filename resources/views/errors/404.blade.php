@extends('layouts.app1')

@section('template_title')
    Page Not Found
@endsection

@section('sidebar')
    
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui segment basic center middle aligned">
    <div class="ui container equal width stackable internally celled grid" style="height: 70vh;">
        <div class="center aligned row">
            <div class="column middle aligned content">
                <h2 class="ui icon header">
                    <i class="large icons">
                        <i class="black search icon"></i>
                    </i>
                    <div class="content">
                        Page Not Found
                        <div class="sub header">
                            @php
                                $tes = "ERROR CODE 404";
                                dump($tes);
                            @endphp
                            @if(!Auth::user())
                                Please visit another page or back to <i><a href="{{ url('/') }}">Home {{ env('APP_NAME') }}</a></i>
                            @else
                                Please visit another page or back to <i><a href="{{ url('/admin/dashboard') }}">Dashboard {{ env('APP_NAME') }}</a></i>
                            @endif
                        </div>
                    </div>
                </h2>
            </div>
        </div>
    </div>
</div>

<!--@include('footers.footer1')-->
@endsection
