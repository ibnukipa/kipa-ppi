@extends('layouts.app1')

@section('template_title')
    Not Found
@endsection

@section('topbar')
    @include('headers.header1')
@endsection

@section('floatingbar')
    @include('partials.floatingbar.bottom-right')
@endsection

@section('sidebar')
    
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui segment basic center middle aligned">
    <div class="ui container equal width stackable internally celled grid" style="height: 70vh;">
        <div class="center aligned row">
            <div class="column middle aligned content">
                <h2 class="ui icon header">
                    <i class="search icon"></i>
                    <!--<div class="content">
                        Page not found...
                        <div class="sub header">Halaman yang Anda cari tidak ada.</div>
                    </div>-->
                    <div class="content">
                        Page Not Found
                        <div class="sub header">
                            <span style="font-size: .8em">ERROR CODE 404</span>
                            <br>
                            Please visit another page or back to <i><a href="{{ url('/') }}">Home {{ env('APP_NAME') }}</a></i>
                        </div>
                    </div>
                </h2>
            </div>
        </div>
    </div>
</div>

@include('footers.footer1')
@endsection
