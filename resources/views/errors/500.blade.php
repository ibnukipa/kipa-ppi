@extends('layouts.app1')

@section('template_title')
    Database Offline
@endsection

@section('sidebar')
    
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui segment basic center middle aligned">
    <div class="ui container equal width stackable internally celled grid" style="height: 70vh;">
        <div class="center aligned row">
            <div class="column middle aligned content">
                <h2 class="ui icon header">
                    <i class="large icons">
                        <i class="black database icon"></i>
                        <i class="small red dont icon"></i>
                    </i>
                    <div class="content">
                        Database Offline
                        <div class="sub header">
                            <span style="font-size: .8em">ERROR CODE 500</span>
                            <br>
                            Please come back later or report to <i>csadmin@kipa.com</i>
                        </div>
                    </div>
                </h2>
            </div>
        </div>
    </div>
</div>

<!--@include('footers.footer1')-->
@endsection
