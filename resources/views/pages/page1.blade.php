@extends('layouts.app1')
<?php
    if( LaravelLocalization::getCurrentLocale() == 'en')
        $postFixLang = '_en';
    else
        $postFixLang = '';
    
    $title                  = 'title'.$postFixLang;
    $subtitle               = 'subtitle'.$postFixLang;
    $content                = 'content'.$postFixLang;
    $menu_name              = 'menu_name'.$postFixLang;
    $textrunning_content    = 'textrunning_content'.$postFixLang;
?>

@section('template_title')
    @if(isset($page['name']))
        {{ $page['name'] }}
    @elseif($page->$title)
        {!! $page->$title !!}
    @else
    @endif
@endsection

@section('extend_lib')
<link href="{{ asset('css/jsor.css') }}" rel="stylesheet">
<style>
    .scroll-left {
        font-size: 1.1em;
        /* overflow: hidden; */
        position: relative;
        padding: .3em 0;
        white-space: nowrap;
    }
    .scroll-left p {
        width: auto;
        text-align: center;

        /* Starting position */
        -moz-transform: translateX(100%);
        -webkit-transform: translateX(100%);
        transform: translateX(100%);
        -moz-animation: scroll-left 20s linear infinite;
        -webkit-animation: scroll-left 20s linear infinite;
        animation: scroll-left 20s linear infinite;
    }
</style> 
<script src="{{ asset('js/jssor.slider-25.2.0.min.js') }}"></script>
<script src="{{ asset('js/jsor.js') }}"></script>
@endsection

@section('topbar')
    @include('headers.header1', ['header_name' => $page['name'], 'header_description' => $page['description']])
@endsection

@section('floatingbar')
    @include('partials.floatingbar.bottom-right')
@endsection

@section('sidebar')
    
@endsection

@section('content')
<!-- Page Contents -->
@if($page->textrunning_bool)
<div class="scroll-left" style="background-color: rgba(0, 0, 0, .02)">
    <p class="ki-c-{{$page->textrunning_color}}">{{ $page->$textrunning_content }}</p>
</div>
<div class="ui divider no-margin"></div>
@endif

@if($page->image_slider_bool && $page->imagesliders()->count() > 0)
<!--SLIDER START-->
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" style="position:absolute;top:0px;left:0px;background:url('img/loading.gif') no-repeat 50% 50%;background-color:rgba(0, 0, 0, 0.7);"></div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
        @foreach($page->imagesliders as $key => $imageslide)
            <div>
                <img data-u="image" src="{{ url('/images/'.$imageslide->image_url) }}" />
            </div>
        @endforeach
        
    </div>
    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora061" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75"   data-scale-left="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <path class="a" d="M11949,1919L5964.9,7771.7c-127.9,125.5-127.9,329.1,0,454.9L11949,14079"></path>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora061" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75"             data-scale-right="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <path class="a" d="M5869,1919l5984.1,5852.7c127.9,125.5,127.9,329.1,0,454.9L5869,14079"></path>
        </svg>
    </div>
</div>
<script type="text/javascript">jssor_1_slider_init();</script>
<!--SLIDER END-->
@else

@endif
<div class="ui container" style="min-height: 50vh">
    <div class="ui segment basic">
        <div class="ui grid">
            <div class="row">
                <div class="@if(App\Page::where('parent_id', '=', $page->id)->count() < 1 && !$page->siderbar_bool && !$page->siderbar_recent_journal_bool && !$page->siderbar_recent_article_bool && !$page->siderbar_with_tag_bool) sixteen wide column @else twelve wide column @endif" style="font-size: 1.2em">
                    @if($page->image_cover_landscape && $page->image_cover_landscape_bool)
                        <img class="ui fluid image" src="{{ url('/images/'.$page->image_cover_landscape) }}">
                    @endif
                    <h1 class="ui header no-margin" style="margin-top: 1.5em">
                        <span class="uppercase"> @isset($page_name){{ $page_name }} @elseif($page->$title) {{ $page->$title }} @endif </span>
                        @if($page->$subtitle) <div class="sub header">  {{ $page->$subtitle }} </div> @endif
                    </h1>
                    <?php if($page->journal_clasification_id > 0) $curClassName = $page->clasification->name; else $curClassName = ""; ?>
                    @if($page->journal_clasification_id > 0 || $page->accreditation)
                    <div class="ui horizontal list">
                        @if($page->accreditation)
                        <div class="item">
                            <h5 class="ui header" style="color: rgba(0,0,0,.5)">
                                <i class="certificate icon no-padding"></i>
                                <div class="content">
                                    Terekareditasi
                                </div>
                            </h5>
                        </div>
                        @endif
                        @if($page->journal_clasification_id > 0)
                        <div class="item">
                            <h5 class="ui header" style="color: rgba(0,0,0,.5)">
                                <i class="bookmark remove icon no-padding"></i>
                                <div class="content">
                                    {{$curClassName}}
                                </div>
                            </h5>
                        </div>
                        @endif
                    </div>
                    @endif
                    @if($page->page_type_id == 5)
                        <div class="ui basic segment no-padding">
                            <a target="_blank" href="{{ $page->journal_source_page }}" class="ui tiny right labeled icon button">
                                <i class="sign in icon"></i>
                                View Journal
                            </a>
                            @if(!Auth::guest())
                                <a href="{{ url('/admin/edit/'.$page->id) }}" class="ui tiny right labeled icon button">
                                    <i class="pencil icon"></i>
                                    Edit
                                </a>
                            @endif
                        </div>
                    @elseif (!Auth::guest())
                        <div class="ui basic segment no-padding">
                            <a href="{{ url('/admin/edit/'.$page->id) }}" class="ui tiny right labeled icon button">
                                <i class="pencil icon"></i>
                                Edit
                            </a>
                        </div>
                    @endif
                    <!-- <h5 class="ui header tiny">
                        <i class="settings icon"></i>
                        <div class="content">
                            Author
                            <div class="sub header">Manage your preferences</div>
                        </div>
                    </h5> -->
                    <div class="ui divider" style="border-top: solid 1px rgba(0, 0, 0, .04)"></div>
                    {!! html_entity_decode($page->$content) !!}

                    @if($page->id == 2)
                        <div class="ui segment no-padding basic">
                            @if(App\Page::where('parent_id', '=', $page->id)->count() > 0 || $page->siderbar_bool || $page->siderbar_recent_journal_bool || $page->siderbar_recent_article_bool)
                                <div class="ui three column grid">
                            @else
                                <div class="ui five column grid">
                            @endif 
                                @foreach(App\Page::where('status', '=', 'publish')->where('page_type_id', '=', 5)->orderBy('journal_clasification_id', 'ASC')->get() as $key => $journal)
                                    <div class="column">
                                        <?php 
                                            if($journal->image_cover_potrait) $curImageJournalCover = $journal->image_cover_potrait; else $curImageJournalCover = 'coverjournal/notfound'; 
                                            if($journal->accreditation) $akreditasi = true; else $akreditasi = false; 
                                            if($journal->journal_clasification_id > 0) $curClassName = $journal->clasification->name; else $curClassName = "";
                                        ?>
                                        @include('widgets.card.card1', [
                                            'image_link'    => '/images/'.$curImageJournalCover.'?orientation=potrait',
                                            'header'        => $journal->$title,
                                            'label'         => $curClassName,
                                            'accreditation' => $akreditasi,
                                            'description'   => substr($journal->subtitle, 0, 30).'...',
                                            'detail_link'   => url('/'.$journal->url) ,
                                            'open_link'     => $journal->journal_source_page,
                                        ])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if($page->id == 3)
                        <div class="ui segment no-padding basic">
                            <div class="ui divided items">
                                @foreach(App\Page::where('status', '=', 'publish')->where('page_type_id', '=', 6)->orderBy('updated_at', 'DESC')->get() as $key => $article)
                                <div class="item">
                                    <!-- <div class="image">
                                        <img src="/images/wireframe/image.png">
                                    </div> -->
                                    <div class="content">
                                        <a class="header">{{$article->$title}}</a>
                                        <div class="meta">
                                            <span class="cinema">{{substr($article->$subtitle, 0, 25)}}</span>
                                        </div>
                                        <!-- <div class="description">
                                            <p></p>
                                        </div> -->
                                        <div class="extra">
                                            <a href="{{ url('/'.$article->url) }}" class="ui right floated primary button">
                                                Read More
                                                <i class="right chevron icon"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                
                @if(App\Page::where('parent_id', '=', $page->id)->count() > 0 || $page->siderbar_bool || $page->siderbar_recent_journal_bool || $page->siderbar_recent_article_bool || $page->siderbar_with_tag_bool)
                <div class="four wide column">
                    <div class="ui segments borderradiusless" style="box-shadow: none">
                        @if(($page->siderbar_bool && $page->sidebars()->count() > 0 ) || App\Page::where('parent_id', '=', $page->id)->count() > 0)
                        <div class="ui segment borderradiusless" style="border-bottom: 2px solid #2185D0">
                            <div class="ui relaxed divided list">
                                @foreach(App\Page::where('parent_id', '=', $page->id)->get() as $key => $childPage)
                                    <div class="item">
                                        <i class="chevron right middle aligned icon"></i>
                                        <div class="content">
                                            <a href="{{ url('/'.$childPage->url) }}" class="header">{{ $childPage->$menu_name }}</a>
                                        </div>
                                    </div>
                                @endforeach
                                @foreach($page->sidebars as $key => $pageSidebar)
                                    <div class="item">
                                        <i class="chevron right middle aligned icon"></i>
                                        <div class="content">
                                            <a href="{{ url('/'.$pageSidebar->url) }}" class="header">{{ $pageSidebar->label }}</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div> 
                        @endif
                        @if($page->siderbar_recent_journal_bool)
                        <div class="ui segment borderradiusless">
                            <h1 class="ui sub header">
                                Recent Journal
                            </h1>
                            @if(App\Page::where('page_type_id', '=', 5)->get()->count() > 0)
                                @foreach(App\Page::take(5)->where('status', '=', 'publish')->where('visible', '=', true)->where('page_type_id', '=', 5)->orderBy('updated_at', 'DESC')->get() as $key => $recjournal)
                                    <div class="ui feed">
                                        <div class="event">
                                            <div class="content">
                                                <div class="summary" style="font-weight: 400">
                                                    @if($page->id == $recjournal->id)
                                                        <a style="margin-right: .5em; color: rgba(0, 0 , 0, .8)">{{$recjournal->$title}}</a>
                                                    @else
                                                        <a href="{{ url('/'.$recjournal->url) }}" style="margin-right: .5em;">{{$recjournal->$title}}</a>
                                                    @endif
                                                    <div class="date no-margin">Last Updated at {{ $recjournal->updated_at }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="ui feed">
                                    <div class="event">
                                        <div class="content">
                                            <div class="summary" style="font-weight: 400">
                                                <div class="date no-margin"><i>Empty Journals</i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div> 
                        @endif
                        @if($page->siderbar_recent_article_bool)
                        <div class="ui segment borderradiusless">
                            <h1 class="ui sub header">
                                Recent Articles
                            </h1>
                            @if(App\Page::where('page_type_id', '=', 6)->get()->count() > 0)
                                @foreach(App\Page::take(5)->where('status', '=', 'publish')->where('visible', '=', true)->where('page_type_id', '=', 6)->orderBy('updated_at', 'DESC')->get() as $key => $recarticle)
                                    <div class="ui feed">
                                        <div class="event">
                                            <div class="content">
                                                <div class="summary" style="font-weight: 400">
                                                    @if($page->id == $recarticle->id)
                                                        <a style="margin-right: .5em; color: rgba(0, 0 , 0, .8)">{{$recarticle->$title}}</a>
                                                    @else
                                                        <a href="{{ url('/'.$recarticle->url) }}" style="margin-right: .5em;">{{$recarticle->$title}}</a>
                                                    @endif
                                                    <div class="date no-margin">Last Updated at {{ $recarticle->updated_at }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="ui feed">
                                    <div class="event">
                                        <div class="content">
                                            <div class="summary" style="font-weight: 400">
                                                <div class="date no-margin"><i>Empty Articles</i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div> 
                        @endif
                    </div> 
                </div>
                @endif
            </div>
            
        </div>
    </div>
</div>

@include('footers.footer1')
@endsection

@section('footerscript')
<script>
    //AKSI
    
    $(document).ready(function() {
        //SLIDER
        
    });
</script>
@endsection