@extends('layouts.app1')
<?php
    if( LaravelLocalization::getCurrentLocale() == 'en')
        $postFixLang = '_en';
    else
        $postFixLang = '';
    
    $title                  = 'title'.$postFixLang;
    $subtitle               = 'subtitle'.$postFixLang;
    $content                = 'content'.$postFixLang;
    $menu_name              = 'menu_name'.$postFixLang;
    $textrunning_content    = 'textrunning_content'.$postFixLang;
?>
@section('template_title')
    Search "{{ $search['_key'] }}"
@endsection

@section('extend_lib')
<link href="{{ asset('css/jsor.css') }}" rel="stylesheet">
<style>
    .scroll-left {
        font-size: 1.1em;
        /* overflow: hidden; */
        position: relative;
        padding: .3em 0;
        white-space: nowrap;
    }
    .scroll-left p {
        width: auto;
        text-align: center;

        /* Starting position */
        -moz-transform: translateX(100%);
        -webkit-transform: translateX(100%);
        transform: translateX(100%);
        -moz-animation: scroll-left 20s linear infinite;
        -webkit-animation: scroll-left 20s linear infinite;
        animation: scroll-left 20s linear infinite;
    }
</style> 
<script src="{{ asset('js/jssor.slider-25.2.0.min.js') }}"></script>
<script src="{{ asset('js/jsor.js') }}"></script>
@endsection

@section('topbar')
    @include('headers.header1', ['header_name' => 'Search', 'header_description' => 'Search for '.$search['_key'] ])
@endsection

@section('floatingbar')
    @include('partials.floatingbar.bottom-right')
@endsection

@section('sidebar')
    
@endsection

@section('content')
<!-- Page Contents -->
<div class="ui container" style="min-height: 77vh">
    <div class="ui segment basic">
        <div class="ui grid">
            <div class="row">
                <div class="twelve wide column" style="font-size: 1.2em">
                    @if(count($resultPpi) < 1 && count($resultDoaj) < 1)
                    <h1 class="ui header no-margin" style="margin-top: 1.5em">
                        <span class="">Sorry, There is no result for "{{ $search['_key'] }}" </span>
                        <div class="sub header"> Search by Title Page, Title Journal, Title Post, etc... </div>
                    </h1>
                    @else
                    <h1 class="ui header no-margin" style="margin-top: 1.5em">
                        <span class=""> Result for "{{ $search['_key'] }}" </span>
                        <div class="sub header"> Search by Title Page, Title Journal, Title Post, etc... </div>
                    </h1>
                    @endif
                    <div class="ui divider" style="border-top: solid 1px rgba(0, 0, 0, .04)"></div>

                    <!-- Journal -->
                    @if($resultPpi->whereIn('page_type_id', [5])->count() > 0)
                    <div class="ui segment" style="border: 1px dashed rgba(0, 0, 0, .2)">
                        <h5 class="ui header">
                            <i class="folder open icon"></i>
                            <div class="content">
                                Hasil untuk Journal
                                <div class="sub header">Semua hasil untuk Journal</div>
                            </div>
                        </h5>
                        <div class="ui divided items">
                            @foreach($resultPpi->whereIn('page_type_id', [5])->sortBy('journal_clasification_id') as $key => $journal)
                            <div class="item">
                                <div class="image">
                                    @if($journal->image_cover_potrait)
                                        <img src="{{ url('/images/'.$journal->image_cover_potrait.'?orientation=potrait') }}">
                                    @else
                                        <img src="{{ url('/images/coverjournal/notfound?orientation=landscape') }}">
                                    @endif
                                </div>
                                <div class="content">
                                    <a href="{{ url('/'.$journal->url) }}" class="header">{{$journal->$title}}</a>
                                    <div class="meta">
                                        @if($journal->accreditation)
                                            <span class="cinema">Terakreditasi, </span>
                                        @endif
                                        <span class="cinema">{{ $journal->type->description }}</span>
                                        <span class="cinema">{{ $journal->clasification->description }}</span>
                                    </div>
                                    <div class="description">
                                        <p>{{substr($journal->$subtitle, 0, 150)}}</p>
                                    </div>
                                    <div class="extra">
                                        <a href="{{ url('/'.$journal->url) }}" class="ui right floated basic button">
                                            Read More
                                            <i class="right chevron icon"></i>
                                        </a>
                                        @foreach($journal->tags as $key => $valueTag)
                                            <div class="ui label">{{ $valueTag->name }}</div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    <!-- DOAJ -->
                    <div class="ui segment" style="border: 1px dashed rgba(0, 0, 0, .2)">
                        <h5 class="ui header">
                            <i class="folder open icon"></i>
                            <div class="content">
                                Hasil untuk <a target="_blank" href="https://doaj.org">Journal DOAJ</a> 
                                <div class="sub header">Semua hasil untuk Journal DOAJ</div>
                            </div>
                        </h5>
                        <div class="ui divided items">
                            @foreach($resultDoaj as $key => $journalDoaj)
                            <div class="item">
                                <div class="image">
                                    <img src="{{ url('/images/default.png') }}">
                                </div>
                                <div class="content">
                                    <a target="_blank" href="{{ $journalDoaj->bibjson->link[0]->url }}" class="header">{{ $journalDoaj->bibjson->title }}</a>
                                    <div class="meta">
                                        <span class="cinema">{{ $journalDoaj->bibjson->publisher }}</span> -
                                        @foreach($journalDoaj->bibjson->subject as $key => $subject)
                                            <span class="cinema">{{ $subject->term }}</span>
                                        @endforeach
                                    </div>
                                    <div class="description">
                                        <p>{{ $journalDoaj->bibjson->subject[0]->term }} </p>
                                    </div>
                                    <div class="extra">
                                        <a target="_blank" href="{{ $journalDoaj->bibjson->link[0]->url }}" class="ui right floated basic button">
                                            Read More
                                            <i class="right chevron icon"></i>
                                        </a>
                                        @foreach($journalDoaj->bibjson->keywords as $key => $valueTag)
                                            <div class="ui label">{{ $valueTag }}</div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <!-- Laman -->
                    @if($resultPpi->whereNotIn('page_type_id', [5,6])->count() > 0)
                    <div class="ui segment" style="border: 1px dashed rgba(0, 0, 0, .2)">
                        <h5 class="ui header">
                            <i class="file icon"></i>
                            <div class="content">
                                Hasil untuk Laman
                                <div class="sub header">Semua hasil untuk Laman</div>
                            </div>
                        </h5>
                        <div class="ui divided items">
                            @foreach($resultPpi->whereNotIn('page_type_id', [5,6]) as $key => $laman)
                            <div class="item">
                                <div class="image">
                                    <img src="{{ url('/images/default.png') }}">
                                </div>
                                <div class="content">
                                    <a href="{{ url('/'.$laman->url) }}" class="header">{{$laman->$title}}</a>
                                    <div class="meta">
                                        @if($laman->page_type_id != 0)
                                            <span class="cinema">{{ $laman->type->description }}</span>
                                        @else
                                            <span class="cinema">Page</span>
                                        @endif
                                    </div>
                                    <div class="description">
                                        <p>{{substr($laman->$subtitle, 0, 150)}}</p>
                                    </div>
                                    <div class="extra">
                                        <a href="{{ url('/'.$laman->url) }}" class="ui right floated basic button">
                                            Read More
                                            <i class="right chevron icon"></i>
                                        </a>
                                        @foreach($laman->tags as $key => $valueTag)
                                            <div class="ui label">{{ $valueTag->name }}</div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                    <!-- Artikel -->
                    @if($resultPpi->whereIn('page_type_id', [6])->count() > 0)
                    <div class="ui segment" style="border: 1px dashed rgba(0, 0, 0, .2)">
                        <h5 class="ui header">
                            <i class="eyedropper icon"></i>
                            <div class="content">
                                Hasil untuk Artikel
                                <div class="sub header">Semua hasil untuk Artikel</div>
                            </div>
                        </h5>
                        <div class="ui divided items">
                            @foreach($resultPpi->whereIn('page_type_id', [6])->sortBy('updated_at') as $key => $article)
                            <div class="item">
                                <div class="image">
                                    @if($article->image_cover_potrait)
                                        <img src="{{ url('/images/'.$article->image_cover_potrait.'?orientation=potrait') }}">
                                    @else
                                        <img src="{{ url('/images/coverjournal/notfound?orientation=landscape') }}">
                                    @endif
                                </div>
                                <div class="content">
                                    <a href="{{ url('/'.$article->url) }}" class="header">{{$article->$title}}</a>
                                    <div class="meta">
                                        <span class="cinema">{{ $article->type->description }}</span>
                                    </div>
                                    <div class="description">
                                        <p>{{substr($article->$subtitle, 0, 150)}}</p>
                                    </div>
                                    <div class="extra">
                                        <a href="{{ url('/'.$article->url) }}" class="ui right floated basic button">
                                            Read More
                                            <i class="right chevron icon"></i>
                                        </a>
                                        @foreach($article->tags as $key => $valueTag)
                                            <div class="ui label">{{ $valueTag->name }}</div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>

                <div class="four wide column">
                    <div class="ui segments borderradiusless" style="box-shadow: none">
                        <div class="ui segment borderradiusless">
                            <h1 class="ui sub header">
                                Recent Journal
                            </h1>
                            @if(App\Page::where('status', '=', 'publish')->where('visible', '=', true)->where('page_type_id', '=', 5)->get()->count() > 0)
                                @foreach(App\Page::take(5)->where('page_type_id', '=', 5)->orderBy('updated_at', 'DESC')->get() as $key => $recjournal)
                                    <div class="ui feed">
                                        <div class="event">
                                            <div class="content">
                                                <div class="summary" style="font-weight: 400">
                                                    <a href="{{ url('/'.$recjournal->url) }}" style="margin-right: .5em;">{{$recjournal->$title}}</a>
                                                    <div class="date no-margin">Last Updated at {{ $recjournal->updated_at }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="ui feed">
                                    <div class="event">
                                        <div class="content">
                                            <div class="summary" style="font-weight: 400">
                                                <div class="date no-margin"><i>Empty Journals</i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="ui segment borderradiusless">
                            <h1 class="ui sub header">
                                Recent Articles
                            </h1>
                            @if(App\Page::where('status', '=', 'publish')->where('visible', '=', true)->where('page_type_id', '=', 6)->get()->count() > 0)
                                @foreach(App\Page::take(5)->where('page_type_id', '=', 6)->orderBy('updated_at', 'DESC')->get() as $key => $recarticle)
                                    <div class="ui feed">
                                        <div class="event">
                                            <div class="content">
                                                <div class="summary" style="font-weight: 400">
                                                    <a href="{{ url('/'.$recarticle->url) }}" style="margin-right: .5em;">{{$recarticle->$title}}</a>
                                                    <div class="date no-margin">Last Updated at {{ $recarticle->updated_at }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="ui feed">
                                    <div class="event">
                                        <div class="content">
                                            <div class="summary" style="font-weight: 400">
                                                <div class="date no-margin"><i>Empty Articles</i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div> 
                    </div> 
                </div>

            </div>
        </div>
    </div>
</div>

@include('footers.footer1')
@endsection

@section('footerscript')
<script>
    //AKSI
    
    $(document).ready(function() {
        //SLIDER
        
    });
</script>
@endsection