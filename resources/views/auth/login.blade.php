@extends('layouts.app1')

@section('topbar')
    
@endsection

@section('floatingbar')
    @include('partials.floatingbar.bottom-right')
@endsection

@section('sidebar')
@endsection

@section('content')
<!--<div class="column middle aligned content">
    <p>
        <?php 
            // $uuid = Uuid::generate(1);
            // dd($uuid);
            // dd(date("Y/m/d h:i:sa", $uuid->time));
            // dd($_SERVER['REMOTE_ADDR']);
        ?>
    </p>
</div>-->
<div class="ui inverted segment no-padding borderradiusless">
    <div class="ui equal width fluid grid" style="height: 100vh">
        <div class="middle aligned row">
            <div class="column"></div>
            <div class="four wide column">
                @if(!$errors->isEmpty())
                <div class="ui error mini message closeable">
                    <i class="close icon"></i>
                    <div class="header">
                         @lang('hello.form-has-error')
                    </div>
                    <ul class="list">
                        @foreach($errors->getMessages() as $error)
                            <li>
                                {!! $error[0] !!}
                            </li>    
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="ui attached message borderradiusless">
                    <h4 class="ui header">
                        <img class="ui image" src="{{ asset('storage/logo-square-green-512.png') }}">
                        <div class="content">
                            {{ config('app.name', 'KiFramework') }}
                            <!-- <div class="sub header">@lang('basic.app-slogan')</div> -->
                        </div>
                    </h4>
                </div>
                <form class="ui form attached padded segment" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <!--<a href="{{ route('register') }}" class="ui green right ribbon large label absoluted capitalize">
                        <i class="add user icon "></i> @lang('basic.register')
                    </a>-->
                    <div class="{{ $errors->has('email') ? 'error' : '' }} field">
                        <label class="capitalize">@lang('basic.email-address') / Username</label>
                        <input placeholder="@lang('basic.email-address')" id="email" type="email" name="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="{{ $errors->has('password') ? 'error' : '' }} field">
                        <label class="capitalize">@lang('basic.password')</label>
                        <input placeholder="xxxxxxx" id="@lang('basic.password')" type="password" name="password" required>
                    </div>
                    <div class="field">
                        <div class="ui {{ old('remember') ? 'checked' : '' }} checkbox">
                            <input id="remember" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                            <label class="capitalize" for="remember">@lang('basic.password-remember')</label>
                        </div>
                    </div>
                    <div class="ui center aligned">
                        <button class="ui primary button fluid uppercase ki-text">@lang('basic.login')</button>
                    </div>
                </form>
                <!-- <div class="ui bottom attached message borderradiusless">
                    <i class="warning sign icon"></i>
                    @lang('auth.lost-password-1') <a href="{{ route('password.request') }}">@lang('auth.lost-password-2')</a>
                </div> -->
            </div>
            <div class="column"></div>
        </div>
    </div>
</div>
@endsection
